export const data = {
    "parametres": [
        {
            "paa_id": 1,
            "paa_nom": "Nombre de passages de la pirouette",
            "paa_code": "NbPasPirou",
            "paa_description": "Nombre de passages de la pirouette après coupe lorsque le type d'intervention Fanage n'est pas renseigné",
            "paa_tyi_id": "Rec"
        },
        {
            "paa_id": 2,
            "paa_nom": "Nombre de bottes récoltées",
            "paa_code": "NbBottes",
            "paa_description": "Nombre de bottes récoltées sur la surface concernée",
            "paa_tyi_id": "Rec"
        }
    ],
    "unites": [
        {
            "uni_id": 1,
            "uni_symbole": "m",
            "uni_nom": "mètre"
        },
        {
            "uni_id": 2,
            "uni_symbole": "cm",
            "uni_nom": "centimètre"
        }
    ],
    "statuts": [
        {
            "st_id": 1,
            "st_nom": "Créée"
        },
        {
            "st_id": 2,
            "st_nom": "Validée"
        },
        {
            "st_id": 3,
            "st_nom": "Verrouillée"
        },
        {
            "st_id": 4,
            "st_nom": "Archivée"
        }
    ],
    "statutsaisie": [
        {
            "sts_id": 1,
            "sts_nom": "En cours"
        },
        {
            "sts_id": 2,
            "sts_nom": "Clôturée"
        }
    ],
    "typesintervention": [
        {
            "tyi_id": 3,
            "tyi_nom": "Coupe exportée",
            "tyi_code": "CoEx",
            "tyi_description": "Coupe de la végétation avec pour objectif de la récolter et donc de l'exporter",
            "tyi_document": "oui",
            "tyi_produit": null
        },
        {
            "tyi_id": 4,
            "tyi_nom": "Coupe non exportée",
            "tyi_code": "CoNEx",
            "tyi_description": "Coupe de la végétation sans objectif de récolte mais plus de nettoyage non exporté",
            "tyi_document": "oui",
            "tyi_produit": null
        }
    ],
    "lotsanimaux": [
        {
            "lax_id": 1,
            "lax_code": "2005658",
            "lax_description": "24 VL Bota 2015-2016 saison 5",
            "lax_debut": "2014-12-10T23:00:00.000Z",
            "lax_fin": "2016-03-30T22:00:00.000Z"
        },
        {
            "lax_id": 2,
            "lax_code": "2007606",
            "lax_description": "24 VL Bota 2015-2016 saison 5",
            "lax_debut": "2014-12-10T23:00:00.000Z",
            "lax_fin": "2016-03-30T22:00:00.000Z"
        }
    ],
    "ressources": [
        {
            "rsc_id": 1,
            "rsc_nom": "Foin",
            "rsc_type": "Produits récoltes"
        },
        {
            "rsc_id": 2,
            "rsc_nom": "Regain",
            "rsc_type": "Produits récoltes"
        }
    ],
    "echantillons": [
        {
            "ech_id": 1,
            "ech_nom": "echantillon 1"
        },
        {
            "ech_id": 2,
            "ech_nom": "échantillon 2"
        }
    ],
    "elements": [
        {
            "elt_id": 10,
            "elt_code": "QB1",
            "elt_nom": "Biomasse 1",
            "elt_debut": "2015-04-30T22:00:00.000Z",
            "elt_fin": null,
            "elt_1": null,
            "elt_2": null,
            "elt_3": null,
            "elt_4": null,
            "elt_5": null,
            "elt_6": null,
            "elt_7": "F",
            "elt_lieu": "Quadrats-Marcenat"
        },
        {
            "elt_id": 11,
            "elt_code": "QB2",
            "elt_nom": "Biomasse 2",
            "elt_debut": "2015-04-30T22:00:00.000Z",
            "elt_fin": null,
            "elt_1": null,
            "elt_2": null,
            "elt_3": null,
            "elt_4": null,
            "elt_5": null,
            "elt_6": null,
            "elt_7": "F",
            "elt_lieu": "Quadrats-Marcenat"
        }
    ],
    "animaux": [
        {
            "anm_id": 1,
            "anm_code1": "2005658",
            "anm_code2": "5658",
            "anm_nom": "FARINE",
            "anm_sex": "Femelle",
            "anm_debut": "2005-04-10T22:00:00.000Z",
            "anm_fin": "2016-08-08T22:00:00.000Z",
            "anm_site": "Theix"
        },
        {
            "anm_id": 2,
            "anm_code1": "2007606",
            "anm_code2": "7606",
            "anm_nom": "HABI",
            "anm_sex": "Femelle",
            "anm_debut": "2007-03-06T23:00:00.000Z",
            "anm_fin": null,
            "anm_site": "Theix"
        }
    ],
    "users": [
        {
            "uti_id": 7,
            "uti_nom": "Villeneuve",
            "uti_prenom": "Eric",
            "dn": "evilleneuve",
            "uti_login": "eric.villeneuve@inra.fr",
            "uti_mobile_mdp": "1234"
        },
        {
            "uti_id": 24,
            "uti_nom": "Note",
            "uti_prenom": "Priscilla",
            "dn": "pnote",
            "uti_login": "priscilla.note@inra.fr",
            "uti_mobile_mdp": "1234"
        }
    ],
    "dispositifs": [
        {
            "dis_id": 5,
            "dis_nom": "OasYsT",
            "dis_debut": "2013-05-31T22:00:00.000Z",
            "dis_fin": null,
            "dis_logo": null,
            "dis_reference": null,
            "dis_description": "Experimentation-systeme pour tester un systeme bovin laitier innovant, diversifie et durable",
            "dis_protocole": null,
            "dis_site_id": "Theix"
        }
    ],
    "modop": [
        {
            "mod_id": 24,
            "mod_nom": "Brûlage d'andain",
            "mod_code": "BrAnd",
            "mod_description": "Réalisation d'andains sur la parcelle puis les andains sont brûlés",
            "mod_document": null,
            "mod_debut": "1999-12-31T23:00:00.000Z",
            "mod_fin": null,
            "mod_tyi_id": "CoNEx",
            "mod_sit_id": "Theix",
            "mod_reference": "Aqy1"
        },
        {
            "mod_id": 25,
            "mod_nom": "Broyage",
            "mod_code": "Broy",
            "mod_description": "Broyage mécanique sur la parcelle",
            "mod_document": null,
            "mod_debut": "1999-12-31T23:00:00.000Z",
            "mod_fin": null,
            "mod_tyi_id": "CoNEx",
            "mod_sit_id": "Theix",
            "mod_reference": "Aqy2"
        }
    ],
    "methodes": [
        {
            "met_id": 1,
            "met_nom": "pH eau du sol",
            "met_code": "pH_eau",
            "met_description": "pH eau du sol",
            "met_seuil_inf": -15,
            "met_seuil_sup": 15,
            "met_precision": null,
            "met_uni_id": null,
            "met_site_id": "Theix"
        },
        {
            "met_id": 3,
            "met_nom": "Poids sec de l'échantillon 72h 60°C",
            "met_code": "PdsSec_7260",
            "met_description": "Poids sec de l'échantillon après séchage 72h 60°C",
            "met_seuil_inf": 2000,
            "met_seuil_sup": 3000,
            "met_precision": null,
            "met_uni_id": "g",
            "met_site_id": "Theix"
        }
    ],
    "parcelles": [
        {
            "prc_id": 1,
            "prc_idb": 1001,
            "prc_nom": "BH3BPC + BH4 2011",
            "prc_debut": "2011-08-13T22:00:00.000Z",
            "prc_fin": null,
            "prc_commentaire": null,
            "prc_point": "5.42",
            "prc_site": "Theix"
        },
        {
            "prc_id": 2,
            "prc_idb": 1002,
            "prc_nom": "BH3BPC + Communal",
            "prc_debut": "2011-08-07T22:00:00.000Z",
            "prc_fin": null,
            "prc_commentaire": null,
            "prc_point": "5.69",
            "prc_site": "Theix"
        }
    ],
    "equipements": [
        {
            "eqp_id": 131,
            "eqp_idb": 132,
            "eqp_code": null,
            "eqp_site": "Theix",
            "eqp_debut": "1999-12-31T23:00:00.000Z",
            "eqp_fin": null,
            "eqp_commentaire": null,
            "eqp_type": "Tracteurs / Porte-outils",
            "eqp_nom": "ex tracteur 1"
        },
        {
            "eqp_id": 132,
            "eqp_idb": 133,
            "eqp_code": null,
            "eqp_site": "Theix",
            "eqp_debut": "1999-12-31T23:00:00.000Z",
            "eqp_fin": null,
            "eqp_commentaire": null,
            "eqp_type": "Tracteurs / Porte-outils",
            "eqp_nom": "ex tracteur 2"
        }
    ],
    "fiches": [
        {
            "fic_id": 4,
            "fic_nom": "Fiche En cours mobile",
            "fic_date": "2020-04-01T22:00:00.000Z",
            "fic_idb": null,
            "fic_stf_id": 3,
            "fic_uti_id": 7,
            "fic_sit_id": 5,
            "fic_dis_id": null
        },
        {
            "fic_id": 1,
            "fic_nom": "Fiche Theix 1",
            "fic_date": "2020-04-01T22:00:00.000Z",
            "fic_idb": null,
            "fic_stf_id": 3,
            "fic_uti_id": 7,
            "fic_sit_id": 5,
            "fic_dis_id": 5
        }
    ],
    "linkficheintervention": [ ],
    "interventions": [],
    "mesures": []
}