const strings = {
    name : "GAEL mobile",
    site: "Site",
    action : {
        ok : "Valider",
        logout : "Déconnexion", 
        download : "Charger des données",
        upload : "Décharger des données",
        manageFiches : "Gérer les fiches",
        seeInformations: "Voir les informations du site",
        choiceSite: "Changer de site", 
        cancel: "Annuler", 
        back: "Retour"
    },
    login : {
        login : "Identifiant",
        password: "Mot de passe (code à 4 chiffres)",
        connection : "Se connecter",
        error : {
            emptyLogin : "L'identifiant est nécessaire",
            emptyPassword : "Le mot de passe est nécessaire",
            error : "Une erreur s'est produite.", 
            isNotLoginOrPassword : 'Veuillez saisir une adresse mail et un code à 4 chiffres',
            badLoginOrPassword: 'Le login et/ou le mot de passe ne sont pas corrects.', 
            tryAgain : 'Veuillez réessayer.'

        }
    },
    snackbar : {
        needSite: "Veuillez choisir un site",
        downloadOk: "Les données sont chargées",
        uploadOk: "Les données sont déchargées", 
        saveDataOk : "Les données sont sauvegardées", 
        error : "Une erreur s'est produite.", 
        errorUploadApp: "Une erreur s'est produite au niveau de l'application, impossible d'envoyer les données",
        errorUploadServer:"Une erreur s'est produite au niveau du serveur. Impossible d'envoyer les données",
        errorUploadDelete: "Les données ont été envoyées avec succès mais une erreur s'est produite lors de leur suppresion de l'application.",
        successUpload : "Les données ont été envoyées avec succès",
    },
    choicesite : {
        title : "Sélection du site ",
        information : "Veuillez choisir le site que vous voulez associer à la tablette. Les données du site seront téléchargées."
    }, 
    home : {
        title : "Que voulez-vous faire ?"
    }, 
    download: {
        title : "Charger des données", 
        information : "Charger les nouvelles données vous permettra automatiquement de :", 
        action1 : "Supprimer les fiches archivées", 
        action2: "Ajouter les nouveaux utilisateurs",
    },
    upload: {
        title : "Décharger des données", 
        information : "Décharger les données vous permettra automatiquement de :", 
        action1 : "Envoyer les fiches clôturées au serveur", 
        action2: "Supprimer les fiches clôturées de l'application"
    }, 
    animaux: {
        name: "Animaux",
        singular: "Animal",
        nom: "Nom", 
        code1: "Code 1",
        code2: "Code 2",
        sexe: "Sexe",
        dateStart: "Date de naissance",
        dateEnd: "Date de mort"
    },
    dispositifs: {
        name: "Dispositifs", 
        singular: "Dispositif",
        nom: "Nom", 
        logo: "Logo",
        reference: "Référence",
        description: "Description",
        protocole: "Protocole",
        dateStart: "Date de début",
        dateEnd: "Date de fin"
    },
    echantillons: {
        name: "Echantillons", 
        singular: "Echantillon",
        nom: "Nom"
    },
    elements: {
        name: "Eléments du paysage",
        singular: "Elément du paysage",
        nom: "Nom",
        code: "Code",
        dateStart: "Date de début",
        dateEnd: "Date de fin",
        place: "Lieu",
        unknow1 : "Inconnu 1",
        unknow2 : "Inconnu 2",
        unknow3 : "Inconnu 3",
        unknow4 : "Inconnu 4",
        unknow5 : "Inconnu 5",
        unknow6 : "Inconnu 6",
        unknow7 : "Inconnu 7"
    },
    equipements: {
        name: "Equipements",
        singular: "Equipement",
        nom: "Nom",
        code: "Code",
        dateStart: "Date de début",
        dateEnd: "Date de fin",
        type: "Type",
        commentaire : "Commentaire",
    },
    lots: {
        name: "Lots d'animaux",
        singular: "Lot d'animaux",
        nom: "Nom",
        code: "Code",
        dateStart: "Date de début",
        dateEnd: "Date de fin",
    },
    methodes: {
        name: "Méthodes de mesure",
        singular: "Méthode de mesure",
        nom: "Nom",
        code: "Code",
        description: "Description",
        inf: "Borne inférieur",
        sup: "Borne supérieur",
        precision: "Précision",
        unite: "Unité",
    },
    modeop: {
        name: "Modes opératoires",
        singular: "Mode opératoire",
        nom: "Nom",
        code: "Code",
        description: "Description",
        document: "Document",
        dateStart: "Date de début",
        dateEnd: "Date de fin",
        typeintervention: "Type d'intervention",
        reference: "Référence",
    },
    parametres: {
        name: "Paramètres",
        singular: "Paramètre",
        nom: "Nom",
        code: "Code",
        description: "Description",
        typeintervention: "Type d'intervention",
    },
    parcelles: {
        name: "Parcelles",
        singular: "Parcelle",
        nom: "Nom",
        code: "Code",
        dateStart: "Date de début",
        dateEnd: "Date de fin",
        idb: "ID b",
        commentaire: "Commentaire",
        point: "Point",
    },
    ressources: {
        name: "Ressources",
        singular: "Ressource",
        nom: "Nom",
        type: "Type",
    },
    typesintervention: {
        name: "Types d'intervention",
        singular: "Type d'intervention",
        nom: "Nom",
        code: "Code",
        description: "Description",
        document: "Document",
        produit: "Produit"
    },
    unites: {
        name: "Unités",
        singular: "Unité",
        nom: "Nom",
        symbole: "Symbole",
    },
    fiche: {
        state : {
            inProgress: "En cours",
            finished: "Clôturée"
        },
        name : "Nom",
        date : "Date",
        stateSaisie : "Statut de la saisie",
        dispositif: "Dispositif",
        typeintervention: "Type d'intervention",
        modeop: "Mode opératoire",
        object :"Objets",
        measure: "Mesures",
        method: "Méthode",
    }
}

export default strings