import React, { Component } from 'react';
import { Appbar, Menu, Snackbar } from 'react-native-paper';
import { Text, View, StyleSheet} from 'react-native';

import DeviceData from '../sessionData/DeviceData'
import strings from '../res/Strings'
import {isSite} from '../Utils/UtilsAsyncStorage'

/**
 * Component App bar in the top of the screen
 */
export default class AppBar extends Component {

  state = {visibleMenu: false, visibleSnackbar: false}

  /**
   * check if a site is selected before move to another screen
   */
  goScreen = async (screen) => {
    if ( await isSite() ){
      this.props.navigation.push(screen)
    } else {
      this.setState({visibleSnackbar: true})
    }
  }

  /**
   * logout a connected user
   */
  logout = () => {
    this.setState({visibleMenu: false})
    DeviceData.logout()
    this.props.navigation.push('Login')
  }

  render() {
    return (
      <View>
        <Appbar style={styles.top}>
            <Appbar.Action icon="home" onPress={() => this.goScreen('Home')} />
            <Text onPress={() => this.goScreen('Site')}> {strings.site} : {DeviceData.currentSite} </Text>
            <View>
              <Menu 
                visible={this.state.visibleMenu}
                anchor={<Text onPress={() => this.setState({visibleMenu: true})}>{DeviceData.userName}</Text> }
                onDismiss={() => this.setState({visibleMenu: false})}>
                <Menu.Item icon="download" title={strings.action.download} onPress={() => this.goScreen('Download')}></Menu.Item>
                <Menu.Item icon="upload" title={strings.action.upload} onPress={() => this.goScreen('Upload')}></Menu.Item>
                <Menu.Item icon="logout" title={strings.action.logout} onPress={() => this.logout()}></Menu.Item>
              </Menu>
            </View>
        </Appbar>
        <Snackbar
          visible={this.state.visibleSnackbar}
          onDismiss={() => this.setState({ visibleSnackbar: false })}
          action={{
            label: "Fermer",
            onPress: () => { this.setState({ visibleSnackbar: false })}
          }}
        >
          {strings.snackbar.needSite}
        </Snackbar>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  top: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    flexDirection: 'row',
    justifyContent: 'space-between',
  }
});