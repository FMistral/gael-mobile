import React, { Component } from 'react'
import { View, ScrollView, StyleSheet } from 'react-native'
import {Card, Avatar} from 'react-native-paper'

import AppBar from '../component/AppBar'
import strings from '../res/Strings'


const parcelleLeft = props => <Avatar.Icon {...props} icon="map-legend" /> 
const equipementLeft = props => <Avatar.Icon {...props} icon="hammer" /> 
const elementLeft = props => <Avatar.Icon {...props} icon="tree" /> 
const modeOpLeft = props => <Avatar.Icon {...props} icon="clipboard" /> 
const ressourcesLeft = props => <Avatar.Icon {...props} icon="water-pump" /> 
const animauxLeft = props => <Avatar.Icon {...props} icon="paw" /> 
const lotLeft = props => <Avatar.Icon {...props} icon="sheep" /> 
const echantillonsLeft = props => <Avatar.Icon {...props} icon="test-tube" /> 
const methodeLeft = props => <Avatar.Icon {...props} icon="ruler-square" /> 
const typeLeft = props => <Avatar.Icon {...props} icon="note-text" /> 
const uniteLeft = props => <Avatar.Icon {...props} icon="ruler" /> 
const parametresLeft = props => <Avatar.Icon {...props} icon="cogs" /> 
const dispositifLeft= props => <Avatar.Icon {...props} icon="flask" />

/**
 * Screen with list of type of information
 */
export default class InformationsScreen extends Component {
  render() {
    return (
      <View style={styles.container}>
        <AppBar navigation={this.props.navigation}/>
        <ScrollView style={{marginTop: 55}}>
          <View style={styles.list}>
            <Card style={styles.card} onPress={() => this.props.navigation.push('List', {categorie: 'Animaux', icon: "paw" })}>
              <Card.Title title={strings.animaux.name}  left={animauxLeft} />
            </Card>
            <Card style={styles.card} onPress={() => this.props.navigation.push('List', {categorie: 'Dispositifs', icon: "flask" })}>
              <Card.Title title={strings.dispositifs.name}  left={dispositifLeft} />
            </Card>
            <Card style={styles.card} onPress={() => this.props.navigation.push('List', {categorie: 'Echantillons', icon: "test-tube" })}>
              <Card.Title title={strings.echantillons.name}  left={echantillonsLeft} />
            </Card>
            <Card style={styles.card} onPress={() => this.props.navigation.push('List', {categorie: 'Elément du paysage', icon: "tree" })}>
              <Card.Title title={strings.elements.name}  left={elementLeft} />
            </Card>
            <Card style={styles.card} onPress={() => this.props.navigation.push('List', {categorie: 'Equipements', icon: "hammer" })}>
              <Card.Title title={strings.equipements.name}  left={equipementLeft} />
            </Card>
            <Card style={styles.card} onPress={() => this.props.navigation.push('List', {categorie: "Lots d'animaux", icon: "sheep" })}>
              <Card.Title title={strings.lots.name}  left={lotLeft} />
            </Card>
            <Card style={styles.card} onPress={() => this.props.navigation.push('List', {categorie: "Méthodes de mesure", icon: "ruler-square" })}>
              <Card.Title title={strings.methodes.name}  left={methodeLeft} />
            </Card> 
            <Card style={styles.card} onPress={() => this.props.navigation.push('List', {categorie: "Modes opératoires", icon: "clipboard" })}>
              <Card.Title title={strings.modeop.name}  left={modeOpLeft} />
            </Card> 
            <Card style={styles.card} onPress={() => this.props.navigation.push('List', {categorie: "Paramètres", icon: "cogs" })}>
              <Card.Title title={strings.parametres.name}  left={parametresLeft} />
            </Card>
            <Card style={styles.card} onPress={() => this.props.navigation.push('List', {categorie: "Parcelles", icon: "map-legend" })}>
              <Card.Title title={strings.parcelles.name}  left={parcelleLeft} />
            </Card>
            <Card style={styles.card} onPress={() => this.props.navigation.push('List', {categorie: "Ressources", icon: "water-pump" })}>
              <Card.Title title={strings.ressources.name}  left={ressourcesLeft} />
            </Card>
            <Card style={styles.card} onPress={() => this.props.navigation.push('List', {categorie: "Types d'intervention", icon: "note-text" })}>
              <Card.Title title={strings.typesintervention.name}  left={typeLeft} />
            </Card>    
            <Card style={styles.card} onPress={() => this.props.navigation.push('List', {categorie: "Unités", icon: "ruler" })}>
              <Card.Title title={strings.unites.name}  left={uniteLeft} />
            </Card>        
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  card: {
    width: "90%", 
    margin: '2%'
  }, 
  list: {
    backgroundColor: '#dae8db', 
    alignItems: 'center'
  },
  container: {
    flexDirection: "column", 
    backgroundColor: '#dae8db', 
    height: '100%'
  }
})