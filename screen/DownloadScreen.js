import React, { Component } from 'react';
import { Text, View, StyleSheet } from 'react-native';
import { Headline, Button, IconButton, withTheme, ProgressBar, Snackbar } from 'react-native-paper';
import axios from 'axios'

import AppBar from '../component/AppBar'
import strings from '../res/Strings'
import {requestGetFichesAndDependecy} from '../Utils/Utils'
import {updateData} from '../businessLayer/Download'
import DeviceData from '../sessionData/DeviceData'


/**
 * Screen for download (and update) data
 */
class DownloadScreen extends Component {

  state = {isLoad: false, snackbar: false, messageSnackbar: ''}

  /** download new sata and delete old data */
  downloadData = async () => {
    try{
      this.setState({isLoad: true})
      const request = requestGetFichesAndDependecy + DeviceData.currentIdSite
      axios.get(request,  {headers: {'token ': DeviceData.userToken}})
      .then(async (res) => {
        try {
          updateData(res.data.data)
          this.setState({isLoad: false, snackbar: true, messageSnackbar: strings.snackbar.downloadOk})
        }
        catch(error){
          this.setState({isLoad: false, snackbar: true, messageSnackbar: strings.snackbar.error})
        }
      })
      .catch(error => {
        const code = error.response ? error.response.status : 500
        switch(code) {
          case 400:
            this.setState({isLoad : false})
            this.props.navigation.push('Login')
            break;
          case 401:
            this.setState({isLoad : false})
            this.props.navigation.push('Login')
            break;
          default:
            this.setState({isLoad : false, snackbar : true, messageSnackbar: strings.snackbar.error})
        }
      })
    }
    catch(error){
      this.setState({isLoad: false, snackbar: true, messageSnackbar: strings.snackbar.error})
    }
  }

  render() {
    const {colors} = this.props.theme
    return (
      <>
        <AppBar navigation={this.props.navigation}/>
        <View style={styles.container}>
          <View >
            <View style={styles.containerTitle}>
              <IconButton icon="download" style={{marginLeft: 0, backgroundColor: colors.primary}}/>
              <Headline>{strings.download.title}</Headline>
            </View>
            <Text>{strings.download.information}</Text>
            <View style={{marginLeft: '3%'}}>
              <Text>{strings.download.action1}</Text>
              <Text>{strings.download.action2}</Text>
            </View>
          </View>
          <View style={styles.containerButton}>
            <Button mode="outlined" disabled={this.state.isLoad} onPress={() => this.props.navigation.pop()} style={styles.button}>{strings.action.back}</Button>
            <Button mode="contained" disabled={this.state.isLoad} onPress={() => this.downloadData()} style={styles.button}>{strings.action.ok}</Button>
          </View>
          <ProgressBar indeterminate={true} visible={this.state.isLoad} style={styles.progressbar}/>
        </View>
        <Snackbar
          visible={this.state.snackbar}
          onDismiss={() => this.setState({ snackbar: false })}
          action={{
            label: "Fermer",
            onPress: () => { this.setState({ snackbar: false })}
          }}
        >
          {this.state.messageSnackbar}
        </Snackbar>
      </>
    );
  }
}

export default withTheme(DownloadScreen);

const styles = StyleSheet.create({
  container:{
    justifyContent: "center",
    alignItems: "flex-start", 
    marginLeft: "5%", 
    marginRight: "5%", 
    height: '80%', 
    marginTop: 55
  },
  containerButton: {
    flexDirection: 'row',
    justifyContent: "flex-end", 
    width: '100%', 
  },
  containerTitle: {
    flexDirection: 'row', 
    alignItems: 'center',
    marginBottom: '2%'
  },
  button: {
    marginLeft: '5%', 
    marginTop: '5%'
  }, 
  progressbar: {
    minWidth: '100%', 
    height: 10, 
    marginTop: '3%'
  }
});
