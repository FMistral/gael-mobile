import React, { Component } from 'react';
import { Text, View, StyleSheet } from 'react-native';
import { Headline, IconButton, withTheme } from 'react-native-paper';

import AppBar from '../../component/AppBar'
import {getElement} from '../../businessLayer/GeneralInformation'
import strings from '../../res/Strings'

/**
 * Screen with alls details about one operating mode
 */
class ModeOperatoireDetailsScreen extends Component {

    state = {element: undefined}

    componentDidMount() {
        getElement("Modes opératoires",this.props.route.params.id).then((res) => {this.setState({element : res})})
    }

    render() {
        const {colors} = this.props.theme
        const  element = this.state.element
        var date_start = undefined
        var date_end = undefined
        if (element){
            date_start = new Date(element.mod_debut)
            date_end = new Date(element.mod_fin)
        }
        return (
        <>
            <AppBar navigation={this.props.navigation}/>
            <View style={styles.containerTitle}>
                <IconButton icon={this.props.route.params.icon} style={{marginLeft: 0, backgroundColor: colors.primary, marginLeft: '2%'}}/>
                <Headline>{strings.modeop.singular}</Headline>
            </View>
            <View style={styles.containerInfo}>
                {element !== undefined &&
                    <View >
                        <View style={styles.containerName}>
                            <Headline>{strings.modeop.nom}: {element.mod_nom}</Headline>
                        </View>
                        <View style={{marginLeft: '3%'}}>
                            <Text style={styles.text}>{strings.modeop.code}: {element.mod_code}</Text>
                            <Text style={styles.text}>{strings.modeop.description}: {element.mod_description}</Text>
                            <Text style={styles.text}>{strings.modeop.document}: {element.mod_document}</Text>
                            <Text style={styles.text}>{strings.modeop.dateStart}: {date_start.getDate()}/{date_start.getMonth()+1}/{date_start.getFullYear()}</Text>
                            <Text style={styles.text}>{strings.modeop.dateEnd}: {date_end.getDate()}/{date_end.getMonth()+1}/{date_end.getFullYear()}</Text>
                            <Text style={styles.text}>{strings.modeop.typeintervention}: {element.mod_tyi_id}</Text>
                            <Text style={styles.text}>{strings.modeop.reference}: {element.mod_reference}</Text>
                        </View>
                    </View>
                }
            </View>
        </>
        );
    }
}

export default withTheme(ModeOperatoireDetailsScreen);

const styles = StyleSheet.create({
    containerInfo:{
    justifyContent: "center",
    alignItems: "flex-start", 
    marginLeft: "5%", 
    marginRight: "5%", 
  },
  containerName: {
    flexDirection: 'row', 
    alignItems: 'center',
    marginBottom: '2%'
  },
  containerTitle: {
    flexDirection: 'row', 
    alignItems: 'center', 
    width: '100%', 
    height: '15%', 
    marginTop: '5%', 
  },
  text : {
      marginBottom: '2%'
  }
});
