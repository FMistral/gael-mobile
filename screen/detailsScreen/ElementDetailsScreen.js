import React, { Component } from 'react';
import { Text, View, StyleSheet } from 'react-native';
import { Headline, IconButton, withTheme } from 'react-native-paper';

import AppBar from '../../component/AppBar'
import {getElement} from '../../businessLayer/GeneralInformation'
import strings from '../../res/Strings'

/**
 * Screen with alls details about one landscape element
 */
class ElementDetailsScreen extends Component {

    state = {element: undefined}

    componentDidMount() {
        getElement("Elément du paysage",this.props.route.params.id).then((res) => {this.setState({element : res})})
    }

    render() {
        const {colors} = this.props.theme
        const  element = this.state.element
        var date_start = undefined
        var date_end = undefined
        if (element){
            date_start = new Date(element.elt_debut)
            date_end = new Date(element.elt_fin)
        }
        return (
        <>
            <AppBar navigation={this.props.navigation}/>
            <View style={styles.containerTitle}>
                <IconButton icon={this.props.route.params.icon} style={{marginLeft: 0, backgroundColor: colors.primary, marginLeft: '2%'}}/>
                <Headline>{strings.elements.singular}</Headline>
            </View>
            <View style={styles.containerInfo}>
                {element !== undefined &&
                    <View >
                        <View style={styles.containerName}>
                            <Headline>{strings.elements.nom}: {element.elt_nom}</Headline>
                        </View>
                        <View style={{marginLeft: '3%'}}>
                            <Text style={styles.text}>{strings.elements.code}: {element.elt_code}</Text>
                            <Text style={styles.text}>{strings.elements.dateStart}: {date_start.getDate()}/{date_start.getMonth()+1}/{date_start.getFullYear()}</Text>
                            <Text style={styles.text}>{strings.elements.dateEnd}: {date_end.getDate()}/{date_end.getMonth()+1}/{date_end.getFullYear()}</Text>
                            <Text style={styles.text}>{strings.elements.place}: {element.elt_lieu}</Text>
                            <Text style={styles.text}>{strings.elements.unknow1}: {element.elt_1}</Text>
                            <Text style={styles.text}>{strings.elements.unknow2}: {element.elt_2}</Text>
                            <Text style={styles.text}>{strings.elements.unknow3}: {element.elt_3}</Text>
                            <Text style={styles.text}>{strings.elements.unknow4}: {element.elt_4}</Text>
                            <Text style={styles.text}>{strings.elements.unknow5}: {element.elt_5}</Text>
                            <Text style={styles.text}>{strings.elements.unknow6}: {element.elt_6}</Text>
                            <Text style={styles.text}>{strings.elements.unknow7}: {element.elt_7}</Text>
                        </View>
                    </View>
                }
            </View>
        </>
        );
    }
}

export default withTheme(ElementDetailsScreen);

const styles = StyleSheet.create({
    containerInfo:{
    justifyContent: "center",
    alignItems: "flex-start", 
    marginLeft: "5%", 
    marginRight: "5%", 
  },
  containerName: {
    flexDirection: 'row', 
    alignItems: 'center',
    marginBottom: '2%'
  },
  containerTitle: {
    flexDirection: 'row', 
    alignItems: 'center', 
    width: '100%', 
    height: '15%', 
    marginTop: '5%', 
  },
  text : {
      marginBottom: '2%'
  }
});
