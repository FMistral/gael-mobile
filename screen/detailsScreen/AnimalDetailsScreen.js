import React, { Component } from 'react';
import { Text, View, StyleSheet } from 'react-native';
import { Headline, IconButton, withTheme } from 'react-native-paper';

import AppBar from '../../component/AppBar'
import {getElement} from '../../businessLayer/GeneralInformation' 
import strings from '../../res/Strings'

/**
 * Screen with alls details about one animal
 */
class AnimalDetailsScreen extends Component {

    state = {element: undefined}

    componentDidMount() {
        getElement("Animaux",this.props.route.params.id).then((res) => {this.setState({element : res})})
    }

    render() {
        const {colors} = this.props.theme
        const  element = this.state.element
        var date_start = undefined
        var date_end = undefined
        if (element){
            date_start = new Date(element.anm_debut)
            date_end = new Date(element.anm_fin)
        }
        return (
        <>
            <AppBar navigation={this.props.navigation}/>
            <View style={styles.containerTitle}>
                <IconButton icon={this.props.route.params.icon} style={{marginLeft: 0, backgroundColor: colors.primary, marginLeft: '2%'}}/>
                <Headline>{strings.animaux.singular}</Headline>
            </View>
            <View style={styles.containerInfo}>
                {element !== undefined &&
                    <View >
                        <View style={styles.containerName}>
                            <Headline>{strings.animaux.nom}: {element.anm_nom}</Headline>
                        </View>
                        <View style={{marginLeft: '3%'}}>
                            <Text style={styles.text}>{strings.animaux.code1}: {element.anm_code1}</Text>
                            <Text style={styles.text}>{strings.animaux.code2}: {element.anm_code2}</Text>
                            <Text style={styles.text}>{strings.animaux.sexe}: {element.anm_sex}</Text>
                            <Text style={styles.text}>{strings.animaux.dateStart}: {date_start.getDate()}/{date_start.getMonth()+1}/{date_start.getFullYear()}</Text>
                            <Text style={styles.text}>{strings.animaux.dateEnd}: {date_end.getDate()}/{date_end.getMonth()+1}/{date_end.getFullYear()}</Text>
                        </View>
                    </View>
                }
            </View>
        </>
        );
    }
}

export default withTheme(AnimalDetailsScreen);

const styles = StyleSheet.create({
    containerInfo:{
    justifyContent: "center",
    alignItems: "flex-start", 
    marginLeft: "5%", 
    marginRight: "5%", 
  },
  containerName: {
    flexDirection: 'row', 
    alignItems: 'center',
    marginBottom: '2%'
  },
  containerTitle: {
    flexDirection: 'row', 
    alignItems: 'center', 
    width: '100%', 
    height: '15%', 
    marginTop: '5%', 
  },
  text : {
      marginBottom: '2%'
  }
});
