import React, { Component } from 'react';
import { View, StyleSheet, ScrollView } from 'react-native';
import { List, IconButton, Headline, withTheme } from 'react-native-paper';

import AppBar from '../component/AppBar'
import {getList} from '../businessLayer/GeneralInformation'

/**
 * Generic screen for show a list of element
 */
class ListScreen extends Component {

  constructor(props){
    super(props)
    this.state = {list: []}
  }

  componentDidMount() {
    getList(this.props.route.params.categorie).then(res => this.setState({list : res}))
  }

  /**
   * Move to details screen of one element
   */
  goToDetails = (id) =>{
    switch (this.props.route.params.categorie){
      case 'Animaux': 
        this.props.navigation.push('AnimalDetails', {id : id, icon: this.props.route.params.icon})
        break
      case 'Dispositifs': 
        this.props.navigation.push('DispositifDetails', {id : id, icon: this.props.route.params.icon})
        break
      case 'Echantillons':
        this.props.navigation.push('EchantillonDetails', {id : id, icon: this.props.route.params.icon})
        break
      case 'Elément du paysage':
        this.props.navigation.push('ElementDetails', {id : id, icon: this.props.route.params.icon})
        break
      case 'Equipements':
        this.props.navigation.push('EquipementDetails', {id : id, icon: this.props.route.params.icon})
        break
      case "Lots d'animaux":
        this.props.navigation.push('LotDetails', {id : id, icon: this.props.route.params.icon})
        break
      case "Méthodes de mesure": 
        this.props.navigation.push('MethodeDetails', {id : id, icon: this.props.route.params.icon})
        break
      case "Modes opératoires": 
        this.props.navigation.push('ModeOpDetails', {id : id, icon: this.props.route.params.icon})
        break
      case "Paramètres": 
        this.props.navigation.push('ParametreDetails', {id : id, icon: this.props.route.params.icon})
        break
      case "Parcelles": 
        this.props.navigation.push('ParcelleDetails', {id : id, icon: this.props.route.params.icon})
        break
      case "Ressources": 
        this.props.navigation.push('RessourceDetails', {id : id, icon: this.props.route.params.icon})
        break
      case "Types d'intervention": 
        this.props.navigation.push('TypeInterventionDetails', {id : id, icon: this.props.route.params.icon})
        break
      case "Unités": 
        this.props.navigation.push('UniteDetails', {id : id, icon: this.props.route.params.icon})
        break
      default : 
    }
  }

  render() {
    const {colors} = this.props.theme
    return (
      <>
        <AppBar navigation={this.props.navigation}/>
        <View style={styles.containerTitle}>
            <IconButton icon={this.props.route.params.icon} style={{marginLeft: 0, backgroundColor: colors.primary, marginLeft: '2%'}}/>
            <Headline>{this.props.route.params.categorie}</Headline>
        </View>
        <ScrollView>
            <List.Section style={styles.containerList}>
              <View style={styles.line} />
              { this.state.list.map((element) => (
                <View key={element.id} onPress={()=> this.goToDetails(element.id)  }>
                  <List.Item onPress={()=> this.goToDetails(element.id)  }
                    title={element.name}
                  />
                  <View style={styles.line} />
                </View>
              ))
              }
            </List.Section>
        </ScrollView>
      </>
    );
  }
}

export default withTheme(ListScreen);

const styles = StyleSheet.create({
  line:{
    backgroundColor: '#A2A2A2',
    height: 2,
    width: '100%'
  },
  containerTitle: {
    flexDirection: 'row', 
    alignItems: 'center', 
    marginBottom: '2%',
    width: '100%', 
    height: 75, 
    marginTop: 55
  },
  containerCenter: {
    justifyContent:'center'
  },
  containerList:{
    width: '100%', 
    marginTop: 0, 
    backgroundColor: 'white'
  },
  circle: {
    width : 24,
    height: 24,
    borderWidth: 1,
    borderColor: 'transparent',
    borderRadius : 12,
  }, 
  smallCircle: {
    width : 12,
    height: 12,
    borderWidth: 1,
    borderColor: 'transparent',
    borderRadius : 6,
  }, 
});
