import React, { Component } from 'react'
import { Text, View, Picker, StyleSheet } from 'react-native'
import { Headline, Button, IconButton, withTheme, ProgressBar } from 'react-native-paper'
import axios from 'axios'

import DeviceData from '../sessionData/DeviceData'
import {requestGetSites, requestGetFichesAndDependecy} from '../Utils/Utils'
import {storeData} from '../Utils/UtilsAsyncStorage'
import AppBar from '../component/AppBar'
import strings from '../res/Strings'
import Database from '../Database'

/**
 * Screen for slected a site and load data about this site.
 */
class SiteScreen extends Component {

  state = {site: 'null', disable: true, sites: [], isLoad: false, isError: false}

  componentDidMount() {
    this.loadSites()
  }

  /**
   * Load list of site
   */
  loadSites = () => {
    this.setState({isLoad : true})
    axios.get(requestGetSites, {headers: {'token ': DeviceData.userToken }})
    .then(res => {
       this.setState({isLoad : false, isError : false, sites : res.data.data})
    })
    .catch(error => {
        const code = error.response ? error.response.status : 500
        switch(code) {
          case 400:
            this.setState({isLoad : false, isError : true})
            this.props.navigation.push('Login')
            break;
          case 401:
            this.setState({isLoad : false, isError : true})
            this.props.navigation.push('Login')
            break;
          default:
            this.setState({isLoad : false, isError : true})
        }
    })

  }

  /**
   * Change selected site in state.
   */
  selectedSite = (itemValue) => {
    var disable = itemValue === 'null' ? true : false
    this.setState({disable: disable, site: itemValue})
  }

  /**
   * Save data of site in database and move to home
   */
  saveData = async (nameSite, idSite,data) =>{
    try {
      await Database.deleteInformationAboutEntry()
      Database.saveData(data)
      await storeData("@site",nameSite)
      await storeData("@id_site",idSite)
      await DeviceData.reloadSite()
      this.setState({isLoad : false, isError : false})
      this.props.navigation.push('Home')
    }
    catch(error) {
      this.setState({isLoad : false, isError : true})
    }
  }

  /**
   * Action when user click button "validate", check site selected and load data of this site
   */
  clickButton = () => {
    if (this.state.site !== 'null'){
      const split = this.state.site.split(':')
      const nameSite = split[1]
      const idSite = split[0]
      if(nameSite !== DeviceData.currentSite){
        this.setState({isLoad : true})
        const request = requestGetFichesAndDependecy + idSite.toString()
        axios.get(request, {headers: {'token ': DeviceData.userToken }})
        .then(res => {
          this.saveData(nameSite, idSite, res.data.data)
        })
        .catch(error => {
            const code = error.response ? error.response.status : 500
            switch(code) {
              case 400:
                this.setState({isLoad : false, isError : true})
                this.props.navigation.push('Login')
                break;
              case 401:
                this.setState({isLoad : false, isError : true})
                this.props.navigation.push('Login')
                break;
              default:
                this.setState({isLoad : false, isError : true})
            }
        })
      }
      else{
        this.props.navigation.push('Home')
      }
    }
  }

  render() {
    const {colors} = this.props.theme
    const messageError = this.state.isError ? "Une erreur s'est produite. Veuillez réessayer" : ''
    return (
      <>
        <AppBar navigation={this.props.navigation}/>
        <View style={styles.container}>
          <View >
            <View style={styles.containerTitle}>
              <IconButton icon="map-marker" style={{marginLeft: 0, backgroundColor: colors.primary}}/>
              <Headline>{strings.choicesite.title}</Headline>
            </View>
            <Text>{strings.choicesite.information}</Text>
          </View>
          <Text style={styles.errorText}>{messageError}</Text>
          <Picker
            selectedValue={this.state.site}
            style={styles.picker}
            onValueChange={(itemValue, itemIndex) => this.selectedSite(itemValue)}>
            <Picker.Item label="Veuillez choisir un site" value="null" />
            { this.state.sites.map(site => <Picker.Item label={site.sit_nom} value={site.sit_id+":"+site.sit_nom} key={site.sit_id}/>) }
          </Picker>
          <View style={styles.containerButton}>
            { this.state.isError &&
                <Button mode="contained" onPress={() => this.loadSites()} style={{marginRight: '1%'}}>Reessayer</Button>
            }
            <Button mode="contained" disabled={this.state.disable} onPress={() => this.clickButton()}>{strings.action.ok}</Button>
          </View>
          <ProgressBar indeterminate={true} visible={this.state.isLoad} style={styles.progressbar}/>
        </View>
      </>
    );
  }
}

export default withTheme(SiteScreen)

const styles = StyleSheet.create({
  container:{
    justifyContent: "center",
    alignItems: "flex-start", 
    marginLeft: "5%", 
    marginRight: "5%", 
    height: '80%'
  },
  containerButton: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    width: '100%', 
  },
  containerTitle: {
    flexDirection: 'row', 
    alignItems: 'center', 
    marginBottom: '2%'
  },
  picker: {
    height: 50, 
    width: '100%',
    marginTop : '3%',
    marginBottom: '3%'
  }, 
  errorText: {
    fontSize: 18, 
    color: 'red'
  },
  progressbar: {
      minWidth: '100%', 
      height: 10, 
      marginTop: '3%', 
      marginBottom: '3%'
  }
});
