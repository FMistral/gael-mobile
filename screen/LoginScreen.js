import React, { Component } from 'react';
import { Text, View, StyleSheet, Image  } from 'react-native';
import { Button, TextInput, ProgressBar  } from 'react-native-paper';
import axios from 'axios';
import base64 from 'react-native-base64'

import {isEmpty, isBlank, requestLogin} from '../Utils/Utils'
import {getData} from '../Utils/UtilsAsyncStorage'
import strings from '../res/Strings'
import DeviceData from '../sessionData/DeviceData'
import {checkLogin} from '../businessLayer/Login'

/**
 * First screen, user can login
 */
export default class LoginScreen extends Component {

    state = {
        login: '', 
        password: '', 
        isEmptyLogin: false,
        isEmptyPassword: false, 
        isLoad : false, 
        isError : false, 
        codeError : -1
      };

    /**
     * Login the user thanks back's or database's data
     */
    login = async () => {
        const login = this.state.login
        const password = this.state.password
        var isEmptyLogin = (isEmpty(login) || isBlank(login))
        var isEmptyPassword = (isEmpty(password) || isBlank(password))
        this.setState({isEmptyPassword, isEmptyLogin, isError :false, codeError: -1})
        if( !isEmptyLogin && !isEmptyPassword){
            this.setState({login: '', password: '', isLoad: true})
            const site = await getData('@site')
            DeviceData.setUserName(login)

            if (site === null){
                const auth = base64.encode(login + ":" + password)
                axios.get(requestLogin, {headers: {'Authorization': `Basic ${auth}` }})
                    .then(res => {
                        DeviceData.setUserToken(res.data.data.token)
                        this.setState({isLoad: false})
                        this.props.navigation.push('Site')
                    })
                    .catch(error => {
                        var code = error.response ? error.response.status : 500
                        this.setState({isLoad: false, isError: true, codeError: code})
                    })
            }
            else {
                checkLogin(login, password)
                .then(user => {
                    this.setState({isLoad: false, isError: false})
                    this.props.navigation.push('Home')
                })
                .catch(err => {
                    if (err === "Error : bad user or/and password"){
                        this.setState({isLoad: false, isError: true, codeError: 404})
                    }
                    else {
                        this.setState({isLoad: false, isError: true, codeError: 500})
                    }
                })
            }
        }
    }


    render() {
        var labelLogin = this.state.isEmptyLogin ? strings.login.error.emptyLogin : strings.login.login
        var labelPassword = this.state.isEmptyPassword ? strings.login.error.emptyPassword : strings.login.password
        var messageError = ''
        var detailMessageError = ''
        if (this.state.isError){
            messageError = strings.login.error.error
            switch(this.state.codeError) {
                case 400:
                  detailMessageError = strings.login.error.isNotLoginOrPassword
                  break;
                case 404:
                    detailMessageError = strings.login.error.badLoginOrPassword
                  break;
                default:
                    detailMessageError = strings.login.error.tryAgain
              }
        }
        return (
            <View style={{flex:1, justifyContent: 'center', alignItems: "center"}}>
                <View style={styles.container}>
                    <View style={{justifyContent: 'center', alignItems: "center"}} >
                        <Image source={require('../res/img/Logo-INRAE.jpg')} />
                        <Text style={styles.title}>{strings.name}</Text>
                    </View>
                    <TextInput
                        mode="outlined"
                        label={labelLogin}
                        value={this.state.login}
                        onChangeText={login => this.setState({ login })}
                        style={styles.input}
                        error={this.state.isEmptyLogin}
                    />
                    <TextInput
                        mode="outlined"
                        secureTextEntry={true}
                        keyboardType="number-pad"
                        label={labelPassword}
                        value={this.state.password}
                        onChangeText={password => this.setState({ password })}
                        style={styles.input}
                        error={this.state.isEmptyPassword}
                    />
                    <Button mode="contained" onPress={this.login}>
                        {strings.login.connection}
                    </Button>
                    <Text style={styles.errorText}>{messageError}</Text>
                    <Text style={styles.errorText}>{detailMessageError}</Text>
                    <ProgressBar indeterminate={true} visible={this.state.isLoad} style={styles.progressbar}/>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    input: {
      height: 50, 
      marginBottom: '5%'
    }, 
    container:{
        height: '100%',
        width: '80%', 
        backgroundColor:'#ffffff',
        padding: '1%',
        justifyContent: 'center',
    },
    containerRow:{
        flexDirection: 'row',
        justifyContent: 'flex-start',
        marginBottom: '3%'

    }, 
    title: {
        marginLeft: '5%',
        fontSize: 50, 
        color: '#4caf50'
    }, 
    errorText: {
        fontSize: 18, 
        color: 'red'
    },
    progressbar: {
        minWidth: '100%', 
        height: 10, 
        marginTop: '3%', 
        marginBottom: '3%'
    }
  });
