import React, { Component } from 'react'
import { Text, View, StyleSheet, ScrollView } from 'react-native'
import { List, Button, Chip, IconButton } from 'react-native-paper'

import AppBar from '../component/AppBar'
import Fiches from '../sessionData/Fiches'
import strings from '../res/Strings'

/**
 * Screen with a list of sheets
 */
export default class FichesScreen extends Component {

  constructor(props){
    super(props)
    this.state = {progress: true, finished: true, fiches: []}
  }

  componentDidMount() {
    Fiches.initFiches().then(
      res =>{
        this.setState({progress: true, finished: true, fiches: Fiches.getFichesFilter(true, true)})
      } 
    )
  }

  /**
   * Return a circle with color depends of state
   */
  getCircle = (state) => {
    var color = state === 1 ? "#4caf50" : "#cf4848"
    return (
      <View style={styles.containerCenter}>
        <View style={[styles.circle, {backgroundColor: color}]}/>
      </View>
    )
  }

  /**
   * Add or remove progress sheets of list
   */
  clickProgress = () => {
    const newProgress = ! this.state.progress
    const showFiches = Fiches.getFichesFilter(newProgress, this.state.finished)
    this.setState({progress: newProgress,fiches: showFiches})
  }

  /**
   * Add or remove finished sheets of list
   */
  clickFinished = () => {
    const newFinished = ! this.state.finished
    const showFiches = Fiches.getFichesFilter(this.state.progress, newFinished)
    this.setState({finished: newFinished,fiches: showFiches})
  }

  render() {
    return (
      <>
        <AppBar navigation={this.props.navigation}/>
        <View style={styles.containerHeader}>
          <View style={styles.containerFilter}>
            <IconButton icon="filter" style={{marginLeft: 0}}/>
            <Chip mode="outlined" selected={this.state.progress} style={styles.chip} onPress={() => this.clickProgress()}>
                <View style={[styles.smallCircle, {backgroundColor: '#4caf50'}]}/>
                <Text>{strings.fiche.state.inProgress}</Text>
            </Chip>
            <Chip mode="outlined" selected={this.state.finished} style={styles.chip}onPress={() => this.clickFinished()}>
                <View style={[styles.smallCircle, {backgroundColor: '#cf4848'}]}/>
                <Text>{strings.fiche.state.finished}</Text>
            </Chip>
          </View>
        </View>
        <ScrollView>
            <List.Section style={styles.containerList}>
              <View style={styles.line} />
              { this.state.fiches.map((fiche) => (
                <View key={fiche.id}>
                  <List.Item
                    onPress={() =>  this.props.navigation.push('FicheAndSaisie', {id: fiche.id})}
                    left ={() => this.getCircle(fiche.state)}
                    title={fiche.name}
                    right= {() => <Text style={{textAlignVertical: 'center'}}>{fiche.date.getDate()}/{fiche.date.getMonth()+1}/{fiche.date.getFullYear()}</Text>}
                  />
                  <View style={styles.line} />
                </View>
              ))
              }
            </List.Section>
        </ScrollView>
        <View style={styles.containeButton}>
          <Button mode="contained" icon="upload" onPress={() => this.props.navigation.push("Upload")} style={styles.button}>{strings.action.upload}</Button>
          <Button mode="contained" onPress={() => this.props.navigation.pop()} style={styles.button}>{strings.action.back}</Button>
        </View>
      </>
    );
  }
}

const styles = StyleSheet.create({
  line:{
    backgroundColor: '#A2A2A2',
    height: 2,
    width: '100%'
  },
  containerCenter: {
    justifyContent:'center'
  },
  containerList:{
    width: '100%', 
    marginTop: 0, 
    backgroundColor: 'white'
  },
  containeButton: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: "flex-end", 
    width: '100%',
    width: '100%', 
    height: 75
  },
  containerFilter: {
    flexDirection: 'row', 
    width: '50%', 
    alignItems: 'center', 
    height: 75
  },
  containerHeader:{
    width: '100%', 
    height: 75, 
    marginTop: 55
  },
  circle: {
    width : 24,
    height: 24,
    borderWidth: 1,
    borderColor: 'transparent',
    borderRadius : 12,
  }, 
  smallCircle: {
    width : 12,
    height: 12,
    borderWidth: 1,
    borderColor: 'transparent',
    borderRadius : 6,
  }, 
  button: {
    marginLeft: '5%', 
    marginRight: '5%'
  },
  chip: {
    backgroundColor:'white', 
    margin: 10
  }
});
