import React, { Component } from 'react'
import {StyleSheet, View} from 'react-native'
import { TextInput, Text} from 'react-native-paper'
import MultiSelect from 'react-native-multiple-select'

import strings from '../../res/Strings'

/**
 * Component for show measurement entry for an intervention
 */
export default class MesureSaisie extends Component {

    /** update value  */
    updateValue = (newValue) => {
        var mesure = this.props.mesure
        mesure.value = newValue
        this.props.updateMesure(this.props.ligneInterventionId, this.props.mesure.ligneMesureId, mesure)
    }

    /** update selected plots */
    onSelectedParcelleChange = (newValue) => {
        var mesure = this.props.mesure
        if(newValue.length > 0){
            mesure.selectedParcelle = newValue[0]
        } 
        else {
            mesure.selectedParcelle = undefined
        }
        
        this.props.updateMesure(this.props.ligneInterventionId, this.props.mesure.ligneMesureId, mesure)
    }

    /** update selected sample */
    onSelectedEchantillonChange = (newValue) => {
        var mesure = this.props.mesure
        if(newValue.length > 0){
            mesure.selectedEchantillon = newValue[0]
        } 
        else {
            mesure.selectedEchantillon = undefined
        }
        this.props.updateMesure(this.props.ligneInterventionId, this.props.mesure.ligneMesureId, mesure)
    }

    /** update selected equipements */
    onSelectedEquipementChange = (newValue) => {
        var mesure = this.props.mesure
        mesure.selectedEquipements = newValue
        this.props.updateMesure(this.props.ligneInterventionId, this.props.mesure.ligneMesureId, mesure)
    }

    render() {
        const {name, bEchantillon, bEquipement,selectedEquipements, unite, methode, value, disabled} = this.props.mesure
        const {parcelles, echantillons, equipements} = this.props
        const selectedParcelle = [this.props.mesure.selectedParcelle]
        const selectedEchantillon =  this.props.mesure.selectedEchantillon !== undefined ? [this.props.mesure.selectedEchantillon] : []
        const color = "#4caf50"
        return (
            <View style={styles.container}>
                    <Text style={styles.text}>{name} ({methode})</Text>
                    <View style={styles.valueContainer}>
                        <Text style={styles.text}>Valeur ({unite}): </Text>
                        <TextInput
                            mode="outlined"
                            value={value}
                            onChangeText={newValue => this.updateValue(newValue)}
                            style={styles.input}
                            disabled={disabled}
                        />
                    </View>
                    <MultiSelect
                        styleMainWrapper={styles.picker}
                        hideSubmitButton
                        items={parcelles}
                        uniqueKey="id"
                        ref={(component) => { this.multiSelect = component }}
                        onSelectedItemsChange={this.onSelectedParcelleChange}
                        selectedItems={selectedParcelle}
                        selectText={strings.parcelles.name}
                        displayKey="name"
                        tagRemoveIconColor={color}
                        tagBorderColor={color}
                        tagTextColor={color}
                        selectedItemTextColor={color}
                        selectedItemIconColor={color}
                        single={true}/>
                    {
                        bEchantillon &&
                        <MultiSelect
                        styleMainWrapper={styles.picker}
                        hideSubmitButton
                        items={echantillons}
                        uniqueKey="id"
                        ref={(component) => { this.multiSelect = component }}
                        onSelectedItemsChange={this.onSelectedEchantillonChange}
                        selectedItems={selectedEchantillon}
                        selectText={strings.echantillons.name}
                        displayKey="name"
                        tagRemoveIconColor={color}
                        tagBorderColor={color}
                        tagTextColor={color}
                        selectedItemTextColor={color}
                        selectedItemIconColor={color}
                        single={true}/>                        
                    }
                    {
                        bEquipement &&
                        <MultiSelect
                        styleMainWrapper={styles.picker}
                        hideSubmitButton
                        items={equipements}
                        uniqueKey="id"
                        ref={(component) => { this.multiSelect = component }}
                        onSelectedItemsChange={this.onSelectedEquipementChange}
                        selectedItems={selectedEquipements}
                        selectText={strings.equipements.name}
                        displayKey="name"
                        tagRemoveIconColor={color}
                        tagBorderColor={color}
                        tagTextColor={color}
                        selectedItemTextColor={color}
                        selectedItemIconColor={color}/>                        
                    }
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container : {
        marginLeft: '3%', 
        width: '90%',
        borderStyle: 'solid',
        borderColor : 'black',
        borderWidth: 1,
        padding: '1%', 
        marginTop: '1%',
        marginBottom: '1%'
    },
    text: {
        marginTop: '1%',
        marginBottom: '1%',
    },
    valueContainer : {
        flexDirection: 'row',
        alignItems: 'center',
        width: '80%',
        marginLeft: '3%',
    },
    input: {
        width: '40%',
        height: 35
    }, 
    picker: {
        marginTop: '3%',
        marginBottom: '1%',
        marginLeft: '3%',
        width: '90%'
    }
})