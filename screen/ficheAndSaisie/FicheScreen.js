import React, { Component } from 'react'
import { ScrollView, Text, StyleSheet, View } from 'react-native'
import {ProgressBar, Headline} from 'react-native-paper';

import AppBar from '../../component/AppBar'
import {getFiche} from '../../businessLayer/FicheAndSaisie'
import {sortByPosition} from '../../Utils/Utils' 
import strings from '../../res/Strings'

/**
 * Screen to show information about a sheet
 */
export default class FicheScreen extends Component {

    state= {fiche: undefined}

    componentDidMount() {
        getFiche(this.props.id).then((res) => {
            var interventionsSorted = res.interventions
            if (interventionsSorted.length > 0){
                interventionsSorted = sortByPosition(interventionsSorted)
            }

            for(var i = 0; i < interventionsSorted.length; i ++){
                var mesuresSorted = interventionsSorted[i].mesures
                if (mesuresSorted.length > 0){
                    mesuresSorted = sortByPosition(mesuresSorted)
                    interventionsSorted[i].mesures = mesuresSorted
                }
            }
            this.setState({fiche : res.fiche, dispositif: res.dispositif, statut: res.statut, interventions : interventionsSorted})})
    }

    render() {
        const {fiche, dispositif, statut, interventions} = this.state
        var date = undefined
        if (fiche){
            date = new Date(fiche.fic_date)
        }
        return (
            <View>
                <AppBar navigation={this.props.nav}/>
                { fiche === undefined ?
                    <View style={styles.containerProgress}>
                        <ProgressBar indeterminate={true} style={styles.progressbar}/>
                    </View>
                :
                    <ScrollView style={{marginTop: 55}}>
                        <View style={styles.containerName}>
                            <Headline>{strings.fiche.name} : {fiche.fic_nom}</Headline>
                        </View>
                        <View style={{marginLeft: '3%'}}>
                            <Text style={styles.text}>{strings.fiche.date} : {date.getDate()}/{date.getMonth()+1}/{date.getFullYear()}</Text>
                            <Text style={styles.text}>{strings.fiche.stateSaisie} : {statut}</Text>
                            <Text style={styles.text}>{strings.fiche.dispositif} : {dispositif}</Text>                        
                        </View>
                        <View style={styles.containerName}>
                            <Headline>Interventions</Headline>
                        </View>
                        {interventions.map((intervention) => {
                            return (
                            <View style={styles.intervention} key={intervention.id}>
                                <Text style={styles.text}>{strings.fiche.name} : {intervention.nom}</Text>
                                <Text style={styles.text}>{strings.fiche.typeintervention} : {intervention.type}</Text>
                                <Text style={styles.text}>{strings.fiche.modeop} : {intervention.modeop}</Text>
                                <Text style={styles.text}>{strings.fiche.object} : {intervention.objets}</Text> 
                                <Text style={styles.text}>{strings.fiche.measure}: </Text>     
                                {intervention.mesures.map((mesure) => {
                                    return (
                                    <View style={styles.mesure} key={mesure.id}>
                                        <Text style={styles.textMesure}>{strings.fiche.name} : {mesure.nom}</Text>
                                        <Text style={styles.textMesure}>{strings.fiche.method} : {mesure.methode}</Text>
                                        <Text>{strings.fiche.object} : {mesure.objets}</Text>
                                    </View>)
                                })}                   
                            </View>)
                        })}
                    </ScrollView>
                }
            </View>
        );
    }
}

const styles = StyleSheet.create({
    containerProgress :{
        justifyContent: 'center',
        alignContent: 'center',
        height: '100%',
        width: '100%',
    },
    progressbar: {
        minWidth: '100%', 
        height: 10, 
        marginTop: '3%'
    },
    containerName: {
        flexDirection: 'row', 
        alignItems: 'center',
        marginBottom: '2%',
        marginLeft: '3%'
    },
    text : {
        marginBottom: '2%'
    }, 
    intervention: {
        marginRight: '3%',
        marginLeft: '3%',
        marginBottom: '3%',
        backgroundColor : '#dae8db',
        padding: '1%'  
    },
    mesure: {
        marginRight: '3%',
        marginLeft: '3%',
        marginBottom: '3%',  
        borderStyle: 'solid',
        borderColor : 'black',
        borderWidth: 1,
        padding: '1%'
    },
    textMesure : {
        marginBottom: '1%'
    }, 
})
