
import React, { Component } from 'react'
import {StyleSheet, View} from 'react-native'
import MultiSelect from 'react-native-multiple-select'

import strings from '../../res/Strings'
import ParametreSaisie from './ParametreSaisie'
import MesureSaisie from './MesureSaisie'

/**
 * Component for show intervention entry
 */
export default class InterventionSaisie extends Component {

    /** update selected plots */
    onSelectedParcellesChange = selectedParcelles => {
        var tmpState = this.props.selection
        tmpState.selectedParcelles = selectedParcelles
        this.props.updateSelectedObjectByIntervention(this.props.ligneintervention.idLigne, tmpState)
    }

    /** update selected users */
    onSelectedUsersChange = selectedUsers => {
        var tmpState = this.props.selection
        tmpState.selectedUsers = selectedUsers
        this.props.updateSelectedObjectByIntervention(this.props.ligneintervention.idLigne, tmpState)
    }

    /** update selected samples */
    onSelectedEchantillonsChange = selectedEchantillons => {
        var tmpState = this.props.selection
        tmpState.selectedEchantillons = selectedEchantillons
        this.props.updateSelectedObjectByIntervention(this.props.ligneintervention.idLigne, tmpState)
    }

    /** update selected equipement */
    onSelectedEquipementsChange = selectedEquipements => {
        var tmpState = this.props.selection
        tmpState.selectedEquipements = selectedEquipements
        this.props.updateSelectedObjectByIntervention(this.props.ligneintervention.idLigne, tmpState)
    }

    /** update selected animals */
    onSelectedAnimauxChange = selectedAnimaux => {
        var tmpState = this.props.selection
        tmpState.selectedAnimaux = selectedAnimaux
        this.props.updateSelectedObjectByIntervention(this.props.ligneintervention.idLigne, tmpState)
    }

    /** update selected lots of animals */
    onSelectedLotsChange = selectedLots => {
        var tmpState = this.props.selection
        tmpState.selectedLots = selectedLots
        this.props.updateSelectedObjectByIntervention(this.props.ligneintervention.idLigne, tmpState)
    }

    /** update selected landscape element */
    onSelectedElementsChange = selectedElements => {
        var tmpState = this.props.selection
        tmpState.selectedElements = selectedElements
        this.props.updateSelectedObjectByIntervention(this.props.ligneintervention.idLigne, tmpState)
    }

    /** update selected resources*/
    onSelectedRessourcesChange = selectedRessources => {
        var tmpState = this.props.selection
        tmpState.selectedRessources = selectedRessources
        this.props.updateSelectedObjectByIntervention(this.props.ligneintervention.idLigne, tmpState)
    }

    render() {
        const color = "#4caf50"
        const {parcelles, participants, echantillons, equipements, animaux, lots, elements, ressources, ligneintervention, selection, disabled} = this.props
        const parametres = ligneintervention.parametres
        const mesures = ligneintervention.mesures
        return (
            <View style={{backgroundColor : '#dae8db', marginLeft: '3%', width: '70%'}}>
                <MultiSelect
                    styleMainWrapper={styles.picker}
                    hideSubmitButton
                    items={participants}
                    uniqueKey="id"
                    ref={(component) => { this.multiSelect = component }}
                    onSelectedItemsChange={this.onSelectedUsersChange}
                    selectedItems={selection.selectedUsers}
                    selectText="Participants"
                    displayKey="name"
                    tagRemoveIconColor={color}
                    tagBorderColor={color}
                    tagTextColor={color}
                    selectedItemTextColor={color}
                    selectedItemIconColor={color}/>
                    {
                        ligneintervention.parcelles === 1 &&
                    <MultiSelect
                        styleMainWrapper={styles.picker}
                        hideSubmitButton
                        items={parcelles}
                        uniqueKey="id"
                        ref={(component) => { this.multiSelect = component }}
                        onSelectedItemsChange={this.onSelectedParcellesChange}
                        selectedItems={selection.selectedParcelles}
                        selectText={strings.parcelles.name}
                        displayKey="name"
                        tagRemoveIconColor={color}
                        tagBorderColor={color}
                        tagTextColor={color}
                        selectedItemTextColor={color}
                        selectedItemIconColor={color}/>
                        
                    }
                    {
                        ligneintervention.echantillons === 1 &&
                    <MultiSelect
                        styleMainWrapper={styles.picker}
                        hideSubmitButton
                        items={echantillons}
                        uniqueKey="id"
                        ref={(component) => { this.multiSelect = component }}
                        onSelectedItemsChange={this.onSelectedEchantillonsChange}
                        selectedItems={selection.selectedEchantillons}
                        selectText={strings.echantillons.name}
                        displayKey="name"
                        tagRemoveIconColor={color}
                        tagBorderColor={color}
                        tagTextColor={color}
                        selectedItemTextColor={color}
                        selectedItemIconColor={color}/>
                    }
                    {
                        ligneintervention.equipements === 1 && 
                    <MultiSelect
                        styleMainWrapper={styles.picker}
                        hideSubmitButton
                        items={equipements}
                        uniqueKey="id"
                        ref={(component) => { this.multiSelect = component }}
                        onSelectedItemsChange={this.onSelectedEquipementsChange}
                        selectedItems={selection.selectedEquipements}
                        selectText={strings.equipements.name}
                        displayKey="name"
                        tagRemoveIconColor={color}
                        tagBorderColor={color}
                        tagTextColor={color}
                        selectedItemTextColor={color}
                        selectedItemIconColor={color}/>
                    }
                    {
                        ligneintervention.animaux === 1 && 
                    <MultiSelect
                        styleMainWrapper={styles.picker}
                        hideSubmitButton
                        items={animaux}
                        uniqueKey="id"
                        ref={(component) => { this.multiSelect = component }}
                        onSelectedItemsChange={this.onSelectedAnimauxChange}
                        selectedItems={selection.selectedAnimaux}
                        selectText={strings.animaux.name}
                        displayKey="name"
                        tagRemoveIconColor={color}
                        tagBorderColor={color}
                        tagTextColor={color}
                        selectedItemTextColor={color}
                        selectedItemIconColor={color}/>
                    }
                    {
                        ligneintervention.lots === 1 &&
                    <MultiSelect
                        styleMainWrapper={styles.picker}
                        hideSubmitButton
                        items={lots}
                        uniqueKey="id"
                        ref={(component) => { this.multiSelect = component }}
                        onSelectedItemsChange={this.onSelectedLotsChange}
                        selectedItems={selection.selectedLots}
                        selectText={strings.lots.name}
                        displayKey="name"
                        tagRemoveIconColor={color}
                        tagBorderColor={color}
                        tagTextColor={color}
                        selectedItemTextColor={color}
                        selectedItemIconColor={color}/>
                    }
                    {
                        ligneintervention.elements === 1 &&
                    <MultiSelect
                        styleMainWrapper={styles.picker}
                        hideSubmitButton
                        items={elements}
                        uniqueKey="id"
                        ref={(component) => { this.multiSelect = component }}
                        onSelectedItemsChange={this.onSelectedElementsChange}
                        selectedItems={selection.selectedElements}
                        selectText={strings.elements.name}
                        displayKey="name"
                        tagRemoveIconColor={color}
                        tagBorderColor={color}
                        tagTextColor={color}
                        selectedItemTextColor={color}
                        selectedItemIconColor={color}/>
                    }                            
                    {
                        ligneintervention.ressources === 1 &&
                    <MultiSelect
                        styleMainWrapper={styles.picker}
                        hideSubmitButton
                        items={ressources}
                        uniqueKey="id"
                        ref={(component) => { this.multiSelect = component }}
                        onSelectedItemsChange={this.onSelectedRessourcesChange}
                        selectedItems={selection.selectedRessources}
                        selectText={strings.ressources.name}
                        displayKey="name"
                        tagRemoveIconColor={color}
                        tagBorderColor={color}
                        tagTextColor={color}
                        selectedItemTextColor={color}
                        selectedItemIconColor={color}/>
                    }
                    {
                        parametres.map((parametre) => {
                            return (
                                <ParametreSaisie name={parametre.name} key={parametre.id} value={parametre.value} 
                                updateParametre={this.props.updateParametre} idLigne={ligneintervention.idLigne} id={parametre.id}
                                disabled={disabled}/>
                            )
                        })
                        
                    }
                    {
                        mesures.map((mesure) => {
                            return (
                                <MesureSaisie mesure={mesure} key={mesure.ligneMesureId} updateMesure={this.props.updateMesure} ligneInterventionId={ligneintervention.idLigne}
                                parcelles={parcelles} echantillons={echantillons} equipements={equipements}
                                disabled={disabled}/>
                            )
                        })
                        
                    }
                        
            </View>
        );
    }
}


const styles = StyleSheet.create({
    picker: {
        marginTop: '1%',
        marginBottom: '1%',
        marginLeft: '3%',
        width: '90%'
    }
})