import React, { Component } from 'react'
import { BottomNavigation, Text } from 'react-native-paper'

import AppBar from '../../component/AppBar'
import FicheScreen from './FicheScreen'
import SaisieScreen from './SaisieScreen'

/**
 * Screen with bottom navigation between sheet and entry
 */
export default class BottomNavigationScreen extends Component {
  state = {
    index: 0,
    routes: [
      { key: 'fiche', title: 'Fiche', icon: 'file-document' },
      { key: 'saisie', title: 'Saisie', icon: 'file-document-edit' },
    ],
  }

  /** Return sheet's screen */
  ficheRoute = () => <FicheScreen nav={this.props.navigation} id={this.props.route.params.id}/>

  /**Retrun entry's screen */
  saisieRoute = () => <SaisieScreen nav={this.props.navigation} id={this.props.route.params.id}/>

  /** upadate index of screen to show*/
  handleIndexChange = index => this.setState({ index });

  renderScene = BottomNavigation.SceneMap({
    fiche: this.ficheRoute,
    saisie: this.saisieRoute,
  });

  render() {
    this.ficheRoute = () => <Text>Fiche</Text>
    this.saisieRoute = () => <Text>Saisie</Text>
    return (
        <>
            <AppBar navigation={this.props.navigation}/>
            <BottomNavigation
                navigationState={this.state}
                onIndexChange={this.handleIndexChange}
                renderScene={this.renderScene}
            />
        </>
    );
  }
}