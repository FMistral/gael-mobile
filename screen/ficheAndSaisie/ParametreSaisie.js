import React, { Component } from 'react'
import {StyleSheet, View} from 'react-native'
import { TextInput, Text  } from 'react-native-paper';


/**
 * Component for show setting entry for an intervention
 */
export default class ParametreSaisie extends Component {

  render() {
    const {name, value, id, idLigne, disabled} = this.props
    return (
        <View style={styles.container}>
            <Text style={styles.text}>{name} : </Text>
            <TextInput
                mode="outlined"
                value={value}
                onChangeText={newValue => this.props.updateParametre(id, idLigne, newValue)}
                style={styles.input}
                disabled={disabled}
            />
        </View>
    );
  }
}

const styles = StyleSheet.create({
    container : {
        flexDirection: 'row',
        alignItems: 'center',
        width: '70%',
        marginLeft: '3%',
    },
    text: {
        marginTop: '1%',
        marginBottom: '1%',
    },
    input: {
        width: '40%',
        height: 35
    }
})