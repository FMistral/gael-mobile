import React, { Component } from 'react'
import { ScrollView, Text, StyleSheet, View } from 'react-native'
import {ProgressBar, Headline, FAB, List, Snackbar} from 'react-native-paper'

import AppBar from '../../component/AppBar'
import {getSaisie, saveSaisie, closeSaisie} from '../../businessLayer/FicheAndSaisie'
import {dateToString} from '../../Utils/Utils' 
import strings from '../../res/Strings'
import InterventionSaisie from './InterventionSaisie'

/**
 * Screen for entry
 */
export default class SaisieScreen extends Component {

    state= {nameFiche: undefined, snackbar: false, snackbarMessage: ''}

    componentDidMount() {
        getSaisie(this.props.id)
        .then((res)=> {
            this.setState({idSaisie: res.idSaisie,
                fiche: res.fiche, 
                dateStart: res.dateStart, 
                dateEdit : res.dateEdit, 
                idStatut : res.idStatut,
                statut : res.statut,
                participants: res.participants,
                parcelles: res.parcelles,
                echantillons: res.echantillons, 
                equipements: res.equipements, 
                animaux: res.animaux, 
                lots: res.lots, 
                elements: res.elements, 
                ressources: res.ressources,
                lignesintervention: res.lignesintervention,
                snackbar: false})
        })
        .catch((err) => this.setState({snackbar: true, snackbarMessage: strings.snackbar.error}))

    }

    /**
     * update selected object for an intervention in state
     */
    updateSelectedObjectByIntervention = (idLigneIntervention, newSelectedObjet) => {
        var newListLignes = this.state.lignesintervention
        const indexLigne = newListLignes.findIndex(object => object.idLigne === idLigneIntervention)
        if (indexLigne >= 0){
            var newLigne = newListLignes[indexLigne]
            newLigne.selectedObjects = newSelectedObjet
            newListLignes[indexLigne] = newLigne
            this.setState({lignesintervention: newListLignes})
        }
    } 

    /**
     * update setting for an intervention in state
     */
    updateParametre = (idParametre, idLigne, newValue) => {
        var newListLignes = this.state.lignesintervention
        const indexLigne = newListLignes.findIndex(object => object.idLigne === idLigne)
        if (indexLigne >= 0){
            var newLigne = newListLignes[indexLigne]
            const indexParametre = newLigne.parametres.findIndex(object => object.id === idParametre)
            if(indexParametre >= 0){
                newLigne.parametres[indexParametre].value = newValue
                newListLignes[indexLigne] = newLigne
                this.setState({lignesintervention: newListLignes})
            }
        }
    }

    /**
     * update measurement for an intervention in state
     */
    updateMesure = (idLigneIntervention, idLigneMesure, newMesure) => {
        var updatedLignesintervention = this.state.lignesintervention
        const indexIntervention = updatedLignesintervention.findIndex(object => object.idLigne === idLigneIntervention)
        if (indexIntervention >= 0){
            var updatedIntervention = updatedLignesintervention[indexIntervention]
            const indexMesure = updatedIntervention.mesures.findIndex(object => object.ligneMesureId === idLigneMesure)
            if(indexMesure >= 0){
                updatedIntervention.mesures[indexMesure] = newMesure
            }
            updatedLignesintervention[indexIntervention] = updatedIntervention
            this.setState({lignesintervention: updatedLignesintervention})
        }
    }

    /**
     * save an entry in database
     */
    saveSaisie = async () => {
        try {
            const updatedData = await saveSaisie(this.state.idSaisie, this.state.fiche.fic_id, this.state.lignesintervention)
            this.setState({idSaisie: updatedData.idSaisie, lignesintervention: updatedData.lignesIntervention, snackbar: true, snackbarMessage: strings.snackbar.saveDataOk})
        }
        catch(error) {
            this.setState({snackbar: true, snackbarMessage: strings.snackbar.error})
        }
        
    }

    /**
     * check if object is selected (if necessary) for one type of object
     */
    isDataSelectedObjectsEntered = (isObject, list) => {
        var isEntered = true 
        if (isObject && list.length <= 0){
            isEntered = false
        }
        return isEntered
    }

    /**
     * check if objects are selected (if necessary) for all type of object for an intervention
     */
    isAllDataSelectedObjectsEntered = (intervention) => {
        var allDataSelected = true
        allDataSelected = this.isDataSelectedObjectsEntered(intervention.parcelles, intervention.selectedObjects.selectedParcelles)
        allDataSelected = this.isDataSelectedObjectsEntered(true, intervention.selectedObjects.selectedUsers)   
        allDataSelected = this.isDataSelectedObjectsEntered(intervention.echantillons ,intervention.selectedObjects.selectedEchantillons)  
        allDataSelected = this.isDataSelectedObjectsEntered(intervention.equipements ,intervention.selectedObjects.selectedEquipements)
        allDataSelected = this.isDataSelectedObjectsEntered(intervention.animaux, intervention.selectedObjects.selectedAnimaux)   
        allDataSelected = this.isDataSelectedObjectsEntered(intervention.lots, intervention.selectedObjects.selectedLots) 
        allDataSelected = this.isDataSelectedObjectsEntered(intervention.elements ,intervention.selectedObjects.selectedElements)  
        allDataSelected = this.isDataSelectedObjectsEntered(intervention.ressources ,intervention.selectedObjects.selectedRessources)  
        return  allDataSelected
    }

    /**
     * check if value of setting is entered for an intervention
     */
    isDataParametresEntered = (intervention) => {
        var dataParametresEntered = true
        var indexParametre = 0
        while(dataParametresEntered && indexParametre < intervention.parametres.length ){
            const currParametre = intervention.parametres[indexParametre]
            dataParametresEntered = currParametre.value != undefined && currParametre.value != null
            indexParametre ++
        }
        return dataParametresEntered
    } 

    /**
     * check if all data for measurement are entered for an intervention
     */
    isDataMesuresEntered = (intervention) => {
        var dataMesuresEntered = true
        var indexMesure = 0
        while(dataMesuresEntered && indexMesure < intervention.mesures.length ){
            const currMesure = intervention.mesures[indexMesure]
            dataMesuresEntered = this.isDataSelectedObjectsEntered(currMesure.bEquipement, currMesure.selectedEquipements)
            dataMesuresEntered = dataMesuresEntered && (currMesure.parcelle !== undefined || currMesure.parcelle != null)
            dataMesuresEntered = dataMesuresEntered && ((currMesure.bEchantillon && currMesure.selectedEchantillon !== undefined && currMesure.selectedEchantillon !== null) || (! currMesure.bEchantillon))
            indexMesure ++
        }
        return dataMesuresEntered
    }

    /**
     * check if all data for all intervention are entered
     */
    isAllDataEntered = () => {
        var allDataEntered = true 
        var indexIntervention = 0
        while(allDataEntered && indexIntervention < this.state.lignesintervention.length ){
            const currIntervention = this.state.lignesintervention[indexIntervention]
            allDataEntered = this.isDataMesuresEntered(currIntervention) && this.isDataParametresEntered(currIntervention) && this.isAllDataSelectedObjectsEntered(currIntervention)
            indexIntervention ++
        }
        return allDataEntered
    }

    /**
     * close an entry if all data are entered
     */
    closeSaisie = async () => {
        try {
            if ( !this.isAllDataEntered()){
                this.setState({snackbar: true, snackbarMessage: "Certaines données ne sont pas saisies, impossible de clôturer."})
            }
            else {
                const updatedData = await closeSaisie(this.state.idSaisie, this.state.fiche.fic_id, this.state.lignesintervention)
                this.setState({idSaisie: updatedData.idSaisie, statut : "Clôturée", lignesintervention: updatedData.lignesIntervention, snackbar: true, snackbarMessage: strings.snackbar.saveDataOk})
            }
        }
        catch (error){
            this.setState({snackbar: true, snackbarMessage: strings.snackbar.error})
        }
    }



    render() {
        const {fiche, statut, idStatut, parcelles, participants, echantillons, equipements, animaux, lots, elements, ressources,
            lignesintervention} = this.state
        var dateStartString = undefined
        var dateEditString = undefined
        if (this.state.dateStart !== undefined && this.state.dateStart !== null){
            const dateStart = new Date(this.state.dateStart)
            dateStartString = dateToString(dateStart)
        }
        if (this.state.dateEdit !== undefined && this.state.dateEdit !== null){
            const dateEdit = new Date(this.state.dateEdit)
            dateEditString = dateToString(dateEdit)
        }
        const disabled = idStatut === 2 ? true : false
        return (
            <View style={{height: "100%"}}>
                <AppBar navigation={this.props.nav} style={{diplay: 'flex', flex: 1}}/>
                {fiche === undefined ?
                    <View style={styles.containerProgress}>
                        <ProgressBar indeterminate={true} style={styles.progressbar}/>
                    </View>
                :
                    <ScrollView style={{marginTop: 55}}>
                        <View style={styles.containerName}>
                            <Headline>{strings.fiche.name} : {fiche.fic_nom}</Headline>
                        </View>
                        <View style={{marginLeft: '3%'}}>
                            <Text style={styles.text}>{strings.fiche.stateSaisie} : {statut}</Text>
                            <Text style={styles.text}>Date de début : {dateStartString}</Text>
                            <Text style={styles.text}>Dernière modification : {dateEditString}</Text>                        
                        </View>
                        {lignesintervention.map((ligneintervention) => {
                            const title = "Intervention : " + ligneintervention.nom
                            const selection = ligneintervention.selectedObjects
                            return (
                            <View key={ligneintervention.idLigne}>
                                <List.Accordion style={{marginLeft: '3%', width: '70%', backgroundColor : '#dae8db', marginTop: '2%'}}  title={title} >
                                    <InterventionSaisie parcelles={parcelles} participants={participants} echantillons={echantillons} equipements={equipements} 
                                        animaux={animaux} lots={lots} elements={elements} ressources={ressources} ligneintervention={ligneintervention}
                                        selection={selection} updateSelectedObjectByIntervention={this.updateSelectedObjectByIntervention}
                                        updateParametre={this.updateParametre} updateMesure={this.updateMesure} disabled={disabled}/>
                                </List.Accordion>
                            </View> )
                        })
                        }
                    </ScrollView>

                }
                {
                    !disabled && 
                    <>
                        <FAB style={styles.fab} label="Enregistrer" icon="content-save"  onPress={() => this.saveSaisie()}/>
                        <FAB style={styles.fabTwo} label="Clôturer" icon="file-lock"  onPress={() => this.closeSaisie()}/>
                    </>
                }
                <Snackbar
                visible={this.state.snackbar}
                onDismiss={() => this.setState({ snackbar: false })}
                action={{
                    label: "Fermer",
                    onPress: () => { this.setState({ snackbar: false })}
                }}
                >
                    {this.state.snackbarMessage}
                </Snackbar>
                </View>
        );
    }
}

const styles = StyleSheet.create({
    containerProgress :{
        justifyContent: 'center',
        alignContent: 'center',
        height: '100%',
        width: '100%',
    },
    progressbar: {
        minWidth: '100%', 
        height: 10, 
        marginTop: '3%'
    },
    containerName: {
        flexDirection: 'row', 
        alignItems: 'center',
        marginBottom: '2%',
        marginLeft: '3%'
    },
    text : {
        marginBottom: '2%'
    }, 
    fab: {
        position: 'absolute',
        margin: 16,
        right: 0,
        bottom: 0,
    },
    fabTwo: {
        position: 'absolute',
        margin: 16,
        right: 0,
        bottom: 65,
    },
    picker: {
        marginTop: '1%',
        marginBottom: '1%',
        marginLeft: '3%',
        width: '70%'
    }
})
