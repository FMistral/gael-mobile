import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import { Headline, Button} from 'react-native-paper';

import AppBar from '../component/AppBar'
import strings from '../res/Strings'

/**
 * Main screen, home with choice action for user
 */
export default class HomeScreen extends Component {
  render() {
    return (
      <>
        <AppBar navigation={this.props.navigation}/>
        <View style={{ justifyContent: "center", alignItems: "center", marginTop: 55 }}>
          <Headline>{strings.home.title}</Headline>
          <View >
            <View style={styles.containerButtons}>
              <View style={styles.containerButton}>
                <Button mode="contained" icon="file"  contentStyle={styles.button} onPress={() => this.props.navigation.push('Fiches')}>{strings.action.manageFiches}</Button>
              </View>
              <View style={styles.containerButton}>
            <Button mode="contained" icon="information" contentStyle={styles.button} onPress={() => this.props.navigation.push('Informations')}>{strings.action.seeInformations}</Button>
              </View>
            </View>
            <View style={styles.containerButtons}>
              <View style={styles.containerButton}>
                <Button mode="contained" icon="download" contentStyle={styles.button} onPress={() => this.props.navigation.push('Download')}>{strings.action.download}</Button>
              </View>
              <View style={styles.containerButton}>
                <Button mode="contained" icon="map-marker" contentStyle={styles.button} onPress={() => this.props.navigation.push('Site')}>{strings.action.choiceSite}</Button>
              </View>
            </View>
          </View>
        </View>
      </>
    );
  }
}

const styles = StyleSheet.create({
  button: {
    width:'100%', 
    height: '100%'
  },
  containerButton: {
    width: '40%',
    height: '100%',
  }, 
  containerButtons: {
    flexDirection: "row",
    justifyContent: 'space-around',
    alignItems: 'flex-start',
    height: '35%',
    width: '80%',
    marginTop: '2%'
  }
});