import Database from '../Database'
import DeviceData from '../sessionData/DeviceData'
import {generateUserToken} from '../Utils/Utils'

/**
 * @description check if email and password are corrects
 * @param {String} login 
 * @param {String} password 
 */
export async function checkLogin(login, password){
    try {
        const user = await Database.getElement('ts_utilisateur_uti', 'uti_login', login)
        if(user.length > 0 && user[0].uti_mobile_mdp === password){
            const token = await generateUserToken(user.login, user.uti_id, user.uti_prenom, user.uti_nom)
            DeviceData.setUserToken(token)
        }
        else {
            throw "Error : bad user or/and password"
        }
    }catch (error){
        throw error
    }
}