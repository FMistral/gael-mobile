
import Database from '../Database'
import {dateToString} from '../Utils/Utils'

/**
 * @description return an array with id of selected object for an intervention
 * @param {int} interventionId id of intervention
 * @param {string} table name of table where search selected objects
 * @param {string} nameIdIntervention name of column where search id of intervention
 * @param {string} nameSelectedId name of column where search id of selected object
 * @return {Array} an array with id of selected object for an intervention
 */
export async function getSelectedObject(interventionId, table, nameIdIntervention, nameSelectedId ){
    try {
      var list = []
      if(interventionId !== undefined){
        const resSelectedObject = await Database.getElement(table, nameIdIntervention, interventionId)
        for(var k =0; k < resSelectedObject.length; k++){
          list.push(resSelectedObject[k][nameSelectedId])
        }
      }
    }
    catch (error){
      throw "Error"
    }
    return list
}

/**
 * @description return informations about mesure for upload data 
 * @param {*} currMesure measurement 
 * @return {*} {mes_parcelle: int, mes_methode: int, mes_echantillon : int, equipements : [int], valeur: string}
 */
export async function getMesure(currMesure){
    try {
        var mesure = {mes_parcelle: currMesure.mes_parcelle, mes_methode: currMesure.mes_methode, mes_echantillon : currMesure.mes_echantillon,
                    equipements : [], valeur: ''}

        const resValue = await Database.getElement('t_mesurevariable_mvr', 'mvr_mesure', currMesure.mes_id)
        if(resValue.length > 0){
            mesure.valeur = resValue[0].mvr_value
        }

        const resEquipements = await Database.getElement('t_mesureequipement_meq', 'meq_mesure', currMesure.mes_id)
        for(var j = 0; j < resEquipements.length; j++){
            mesure.equipements.push(resEquipements[j].meq_equipement)
        }
        return mesure
    }
    catch(error){
        throw error
    }
}

/**
 * @description return informations about intervention for upload data
 * @param {*} currIntervention intervention
 * @return {*} {int_type: int, int_modeop: int, animaux: [int], lots: [int], equipements: [int], elements: [int], ressources:[int],
            echantillons:[int], parcelles: [int], utilisateurs:[int], mesures: [], parametres: [{vpa_parametre: int, vpa_value: string}]}
 */
async function getIntervention(currIntervention){
    try {
        var intervention = {int_type: currIntervention.int_type , int_modeop: currIntervention.int_modeop, 
            animaux: [], lots: [], equipements: [], elements: [], ressources:[],
            echantillons:[], parcelles: [], utilisateurs:[], mesures: [], parametres: [] }
        const interventionId = currIntervention.int_id

        intervention.parcelles = await getSelectedObject(interventionId, 't_interventionparcelle_ipa','ipa_intervention' ,'ipa_parcelle')
        intervention.utilisateurs = await getSelectedObject(interventionId, 't_interventionutilisateurs_iut', 'iut_intervention', 'iut_utilisateur')
        intervention.echantillons = await getSelectedObject(interventionId, 't_interventionechantillon_iec', 'iec_intervention', 'iec_echantillon')
        intervention.equipements = await getSelectedObject(interventionId, 't_interventionequipement_ieq', 'ieq_intervention', 'ieq_equipement')
        intervention.animaux = await getSelectedObject(interventionId, 't_interventionanimaux_ian', 'ian_intervention', 'ian_animal')
        intervention.lots = await getSelectedObject(interventionId, 't_interventionlotanimaux_ila', 'ila_intervention', 'ila_lotanimaux')
        intervention.elements = await getSelectedObject(interventionId, 't_interventionelement_iel', 'iel_intervention', 'iel_element')
        intervention.ressources = await getSelectedObject(interventionId, 't_interventionressource_irc', 'irc_intervention', 'irc_ressource')

        const resMesures = await Database.getElement('t_mesure_mes','mes_intervention', interventionId)
        for(var i = 0; i < resMesures.length; i++){
            const currMesure = resMesures[i]
            const mesure = getMesure(currMesure)
            intervention.mesures.push(mesure)
        }

        const resParametres = await Database.getElement('t_valeurparametres_vpa','vpa_intervention', interventionId)
        for(var i = 0; i < resParametres.length; i++){
            const currParametre = resParametres[i]
            intervention.parametres.push({vpa_parametre: currParametre.vpa_parametre, vpa_value: currParametre.vpa_value })
        }
        return intervention
    }
    catch(error){
        throw "Error"
    }

}

/**
 * @description return all data for upload (ie close entry and dependency)
 * @return {*} [{sas_creation: string, sas_modification: string, sas_utilisateur: int, sas_fiche: int, interventions:[]}]
 */
export async function getDataForUpload(){
    var saisies = []
    try {
        const resSaisie = await Database.getElement('t_saisie_sas','sas_statut',2)
        for(var i = 0; i < resSaisie.length; i++){
            const currSaisie = resSaisie[0]
            var saisie = {sas_creation: null, sas_modification: null, sas_utilisateur: null, sas_fiche: null, interventions:[] }
            const dateCreation = null
            const dateModification = null
            if(currSaisie.sas_creation !== undefined && currSaisie.sas_creation !==null){
                dateCreation = dateToString(new Date(currSaisie.sas_creation))
            }
            if(currSaisie.sas_modification !== undefined && currSaisie.sas_modification !==null){
                dateModification = dateToString(new Date(currSaisie.sas_modification))
            }
            saisie.sas_creation = dateCreation
            saisie.sas_modification = dateModification
            saisie.sas_utilisateur = currSaisie.sas_utilisateur
            saisie.sas_fiche = currSaisie.sas_fiche
            const resIntervention = await Database.getElement('t_intervention_int','int_saisie', currSaisie.sas_id)
            for(var j= 0; j < resIntervention.length; j++){
                const currIntervention = resIntervention[0]
                const intervention = await getIntervention(currIntervention)
                saisie.interventions.push(intervention)
            }
            saisies.push(saisie)
        }
        return saisies
    }
    catch (error){
        throw "Error"
    }
   
}

/**
 * @description delete all informations about a sheet and dependency
 * @param {int} idFiche id of sheet
 */
async function deleteFiche(idFiche){
    try{
        const resLigneIntervention = await Database.getElement('t_ligneintervention_lii','lii_fich_id',idFiche)
        for(var i = 0; i < resLigneIntervention.length; i++){
            await Database.delete('t_lignesmesure_lgm', 'lgm_lii_id', resLigneIntervention[i].lii_id)
        }
        Database.delete('t_ligneintervention_lii','lii_fich_id',idFiche)
        Database.delete('t_fiche_fic','fic_id',idFiche)
    }
    catch(error){
        throw "Error"
    }

}

/**
 * @description delete all infromation about close entry and dependency and sheets
 */
export async function deleteCloseEntry(){
    try{
        const resSaisie = await Database.getElement('t_saisie_sas','sas_statut',2)
        for(var i = 0; i < resSaisie.length; i++){
            const currSaisieId = resSaisie[0].sas_id

            const resIntervention = await Database.getElement('t_intervention_int','int_saisie', currSaisieId)
            for(var j = 0; j < resIntervention.length; j++){
                const currInterventionId = resIntervention[j].int_id
                await Database.delete('t_interventionparcelle_ipa','ipa_intervention', currInterventionId)
                await Database.delete('t_interventionutilisateurs_iut', 'iut_intervention', currInterventionId)
                await Database.delete('t_interventionechantillon_iec', 'iec_intervention', currInterventionId)
                await Database.delete('t_interventionequipement_ieq', 'ieq_intervention', currInterventionId)
                await Database.delete('t_interventionanimaux_ian', 'ian_intervention', currInterventionId)
                await Database.delete('t_interventionlotanimaux_ila', 'ila_intervention', currInterventionId)
                await Database.delete('t_interventionelement_iel', 'iel_intervention', currInterventionId)
                await Database.delete('t_interventionressource_irc', 'irc_intervention', currInterventionId)

                const resMesures = await Database.getElement('t_mesure_mes','mes_intervention', currInterventionId)
                for(var i = 0; i < resMesures.length; i++){
                    const currMesureId = resMesures[i].mes_id
                    await Database.delete('t_mesurevariable_mvr', 'mvr_mesure', currMesureId)
                    await Database.delete('t_mesureequipement_meq', 'meq_mesure', currMesureId)
                }
                await Database.delete('t_mesure_mes','mes_intervention', currInterventionId)

                await Database.delete('t_valeurparametres_vpa','vpa_intervention', currInterventionId)
            }
            await Database.delete('t_intervention_int','int_saisie', currSaisieId)
            await deleteFiche(resSaisie[0].sas_fiche)
        }
        await Database.delete('t_saisie_sas','sas_statut',2)
    }
    catch (error){
        throw "Error"
    }
}