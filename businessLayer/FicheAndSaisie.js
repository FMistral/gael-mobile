import Database from '../Database'
import DeviceData from '../sessionData/DeviceData'
import {sortByPosition} from '../Utils/Utils'

/**
 * Business layer for sheet and entry informations
 */



/** GET FICHES */

/**
 * Create object with all information about a intervention
 * @param {*} currIntervention object with information about intervention (result of query)
 * @return {*} object with information intervention 
 * return format {id:int, nom: string, type: string, modeop: string, objets: string, position: int, mesures : [{id: int, nom: string, objets: string, position: int, methode: string}]}
 */
async function getInterventionForFiche(currIntervention) {
  try {
    var intervention = {id: undefined, nom: undefined, type: undefined, modeop: undefined, objets: "", position: 0, mesures : []}
    intervention.nom = currIntervention.lii_libelle
    intervention.id = currIntervention.lii_id

    if (currIntervention.lii_tyi_id != null){
      const resType = await Database.getElement('tr_typeintervention_tyi', 'tyi_id', currIntervention.lii_tyi_id)
      if(resType.length >0){
        intervention.type = resType[0].tyi_nom
      }
    }

    if (currIntervention.lii_mod_id != null){
      const resModeOp = await Database.getElement('tr_modeoperatoire_mod', 'mod_id', currIntervention.lii_mod_id)
      if(resModeOp.length >0){
        intervention.modeop = resModeOp[0].mod_nom
      }
    }

    var objets = currIntervention.lii_bparcelle ? "Parcelles, " : ""
    objets = currIntervention.lii_bechantillon ? objets +"Echantillons, " : objets
    objets = currIntervention.lii_banimaux ? objets +"Animaux, " : objets
    objets = currIntervention.lii_belementpaysage ? objets +"Elements du paysage, " : objets
    objets = currIntervention.lii_bequipement ? objets +"Equipements, " : objets
    objets = currIntervention.lii_bressource ? objets +"Ressources, " : objets
    objets = currIntervention.lii_bblotsanimaux ? objets +"Lots d'animaux, " : objets
    intervention.objets = objets
    intervention.position = currIntervention.position

    // Get mesures
    const resMesure = await Database.getElement('t_lignesmesure_lgm', 'lgm_lii_id', currIntervention.lii_id)
    for(var j = 0; j < resMesure.length; j++){
      const currMesure = resMesure[j]
      var mesure = {id: undefined, nom: undefined, objets: undefined, position: 0, methode: undefined}
      var objetsMesure = currMesure.lgm_bEchantillon ? "Echantillons, " : ""
      objetsMesure = currMesure.lgm_bEquipement ? objetsMesure +"Equipements, " : objetsMesure
      mesure.id = currMesure.lgm_id
      mesure.nom = currMesure.lgm_libelle
      mesure.objets = objetsMesure
      mesure.position = currMesure.position
      const resMethod = await Database.getElement('tr_methode_met', 'met_id', currMesure.lgm_met_id)
      if(resMethod.length >0){
        mesure.methode = resMethod[0].met_nom
      }
      intervention.mesures.push(mesure)
    }
    return intervention
  }
  catch (error) {
    throw "Error"
  }
  
}


/**
 * Create object with all information about a sheet
 * @param {int} id id of sheet
 * @return {*} object with information
 * return format {fiche: {fic_id: int, fic_nom: string, fic_date: date, fic_stf_id: int, fic_uti_id: int, fic_sit_id: int, fic_dis_id: int}, dispositif: string, statut : string, interventions : []}
 */
export async function getFiche(id){
  try {
    var element = {fiche: undefined, dispositif: undefined, statut : 'En cours', interventions : []}
    var res = await Database.getElement('t_fiche_fic','fic_id' ,id)
    if (res.length >0 ){
      const fiche = res[0]
      element.fiche = fiche

      // Get dispositif 
      res= await Database.getElement('ts_dispositif_dis','dis_id' ,fiche.fic_dis_id)
      if(res.length >0){
        element.dispositif = res[0].dis_nom
      }

      // Get Statut saisie
      res = await Database.getElement('t_saisie_sas', 'sas_fiche', fiche.fic_id)
      if(res.length >0){
        res = await Database.getElement('t_statutsaisie_sts','sts_id',res[0].sas_statut)
        if(res.length >0){
          element.statut = res[0].sts_nom
        }
      }

      // Get ligne intervention
      res = await Database.getElement('t_ligneintervention_lii', 'lii_fich_id', fiche.fic_id)
      for(var i = 0; i< res.length; i++){
        const currIntervention = res[i]
        const intervention = await getInterventionForFiche(currIntervention)
        element.interventions.push(intervention)
      }
    }
    return element
  }
  catch (error){
    throw "Error"
  }
}


/** GET SAISIE  */

/**
 * @description return an array with id of selected object for an intervention
 * @param {int} interventionId id of intervention
 * @param {string} table name of table where search selected objects
 * @param {string} nameIdIntervention name of column where search id of intervention
 * @param {string} nameSelectedId name of column where search id of selected object
 * @return {Array} an array with id of selected object for an intervention
 */
export async function getSelectedObject(interventionId, table, nameIdIntervention, nameSelectedId ){
  try {
    var list = []
    if(interventionId !== undefined){
      const resSelectedObject = await Database.getElement(table, nameIdIntervention, interventionId)
      for(var k =0; k < resSelectedObject.length; k++){
        list.push(resSelectedObject[k][nameSelectedId])
      }
    }
  }
  catch (error){
    throw "Error"
  }
  return list
}

/**
 * @description return an array with {name: string, id:int} of object that user can select
 * @param {string} table name of table where search objects
 * @param {string} nameObjectId name of column where search name of object
 * @return {Array}  an array with {name: string, id:int} of object
 */
async function getObjects (table, nameObjectId, nameNameObject ){
  try {
    var list = []
    const res = await Database.getTable(table)
    for(var i = 0; i < res.length; i++){
      const object = {id: res[i][nameObjectId], name: res[i][nameNameObject]}
      list.push(object)
    }
  }
  catch(error){
    throw "Error"
  }
  return list
}


/**
 * @description return id of intervention with same id of entry and type of intervention
 * @param {int} idSaisie id of entry
 * @param {int} typeIntervention id of type of intervention
 * @return {int} id of intervention
 */
export async function searchInterventionId(idSaisie, typeIntervention){
  try{
    var interventionId =undefined
    if (idSaisie !== undefined && typeIntervention !== null ){
      const resIntervention = await Database.getElementTwoConditions('t_intervention_int', 'int_saisie', idSaisie, 'int_type', typeIntervention)
      if(resIntervention.length > 0){
        interventionId = resIntervention[0].int_id
      }
    }
    return interventionId
  }
  catch(error){
    throw "Error"
  }
}


/**
 * @description return an array of settings for an intervention
 * @param {int} interventionId if of intervention
 * @param {int} typeId type of intervention
 * @return {Array} an array of settings {id: int, name: string, value: string}
 */
export async function getParametres(interventionId, typeId){
  try{
    var parametres = []
    if (typeId != null && typeId != undefined){
      const resType = await Database.getElement('tr_typeintervention_tyi', 'tyi_id', typeId)
      if(resType.length >0){
        const resParametres = await Database.getElement('tr_parametre_paa', 'paa_tyi_id', resType[0].tyi_code)
        for(var j = 0; j< resParametres.length; j++){
          var parametre = {id: resParametres[j].paa_id, name: resParametres[j].paa_nom, value: undefined}
          if(interventionId !== undefined){
            const resValueParametre = await Database.getElementTwoConditions('t_valeurparametres_vpa', 'vpa_intervention', interventionId, 'vpa_parametre', parametre.id)
            if(resValueParametre.length > 0){
              parametre.value = resValueParametre[0].vpa_value
            }
          }
          parametres.push(parametre)
        }
      }
    }
    return parametres
  }
  catch(error){
    throw "Error"
  }  
}


/**
 * @description return an array of measurement for an intervention
 * @param {int} ligneInterventionId line of intervention's id
 * @param {int} interventionId intervention's id
 * @return an array of measurement {ligneMesureId:int, mesureId: int, name: string, position: int,
                    bEchantillon: bool, bEquipement: bool, selectedEchantillon: int, selectedEquipements: [int], 
                    selectedParcelle: int, unite: string, methode: string, methodeId: int, value: string }
 */
export async function getMesures(ligneInterventionId, interventionId){
  try{
    var mesures = []
    const resLignesMesures = await Database.getElement('t_lignesmesure_lgm', 'lgm_lii_id', ligneInterventionId)
    for(var i = 0; i< resLignesMesures.length; i++){
      const currLigneMesure = resLignesMesures[i]
      var mesure = {ligneMesureId: currLigneMesure.lgm_id, mesureId: undefined, name: currLigneMesure.lgm_libelle, position: currLigneMesure.lgm_position,
                    bEchantillon: currLigneMesure.lgm_bEchantillon, bEquipement: currLigneMesure.lgm_bEquipement,
                    selectedEchantillon: undefined, selectedEquipements: [], selectedParcelle: undefined, 
                    unite: undefined, methode: undefined, methodeId: undefined, value: undefined }

      const resMethode = await Database.getElement('tr_methode_met', 'met_id', currLigneMesure.lgm_met_id)
      if(resMethode.length > 0){
        const currLigneMethode = resMethode[0]
        mesure.methode = currLigneMethode.met_nom
        mesure.methodeId = currLigneMethode.met_id
        mesure.unite = currLigneMethode.met_uni_id
      }

      if(interventionId != undefined){
        const resMesure = await Database.getElement('t_mesure_mes', 'mes_intervention', interventionId)
        if(resMesure.length > 0){
          const currMesure = resMesure[0]
          mesure.mesureId = currMesure.mes_id
          mesure.selectedParcelle = currMesure.mes_parcelle
          if(mesure.bEchantillon){
            mesure.selectedEchantillon = currMesure.mes_echantillon
          }

          const resValue = await Database.getElement('t_mesurevariable_mvr', 'mvr_mesure', currMesure.mes_id)
          if(resValue.length > 0){
            mesure.value = resValue[0].mvr_value
          }

          if(mesure.bEquipement){
            const resEquipements = await Database.getElement('t_mesureequipement_meq', 'meq_mesure', currMesure.mes_id)
            for(var j = 0; j < resEquipements.length; j++){
              mesure.selectedEquipements.push(resEquipements[j].meq_equipement)
            }
          }
        }
      }
      mesures.push(mesure)
    }
    mesures = sortByPosition(mesures)
    return mesures
  }
  catch(error){
    throw "Error"
  }  
}

/**
 * @description return information about intervention
 * @param {*} currIntervention resultat of query about an intervention
 * @param {int} idSaisie id of entry
 * @return {*} information about intervention {idLigne: int, idIntervention: int, nom: string, position: int, 
      parcelles: bool, echantillons: bool, equipements: bool, animaux: bool, lots: boll, elements: bool, ressources: bool,
      parametres: [setting], mesures: [measurement], selectedObjects: {}}
 */
async function getInterventionForSaisie(currIntervention, idSaisie){
  try{
    var intervention = {idLigne: undefined, idIntervention: undefined, nom: undefined, position: 0, 
      parcelles: false, echantillons: false, equipements: false, animaux: false, lots: false, elements: false, ressources: false,
      parametres: [], mesures: [], selectedObjects: {}}
    intervention.nom = currIntervention.lii_libelle
    intervention.idLigne = currIntervention.lii_id
    intervention.parcelles= currIntervention.lii_bparcelle 
    intervention.echantillons = currIntervention.lii_bechantillon 
    intervention.animaux = currIntervention.lii_banimaux 
    intervention.elements = currIntervention.lii_belementpaysage 
    intervention.equipements = currIntervention.lii_bequipement 
    intervention.ressources = currIntervention.lii_bressource 
    intervention.lots = currIntervention.lii_blotsanimaux 
    intervention.position = currIntervention.position
    intervention.type = currIntervention.lii_tyi_id
    intervention.modeop = currIntervention.lii_mod_id
  
    // search intervention id
    const interventionId = await searchInterventionId(idSaisie, currIntervention.lii_tyi_id)
    intervention.idIntervention = interventionId
  
    // get parametres
    intervention.parametres =  await getParametres(interventionId, currIntervention.lii_tyi_id)
  
    // get mesures
    intervention.mesures = await getMesures(currIntervention.lii_id, interventionId)
  
    // get selected objet 
    const selectedParcelles = await getSelectedObject(interventionId, 't_interventionparcelle_ipa','ipa_intervention' ,'ipa_parcelle')
    const selectedUsers = await getSelectedObject(interventionId, 't_interventionutilisateurs_iut', 'iut_intervention', 'iut_utilisateur')
    const selectedEchantillons = await getSelectedObject(interventionId, 't_interventionechantillon_iec', 'iec_intervention', 'iec_echantillon')
    const selectedEquipements = await getSelectedObject(interventionId, 't_interventionequipement_ieq', 'ieq_intervention', 'ieq_equipement')
    const selectedAnimaux = await getSelectedObject(interventionId, 't_interventionanimaux_ian', 'ian_intervention', 'ian_animal')
    const selectedLots = await getSelectedObject(interventionId, 't_interventionlotanimaux_ila', 'ila_intervention', 'ila_lotanimaux')
    const selectedElements = await getSelectedObject(interventionId, 't_interventionelement_iel', 'iel_intervention', 'iel_element')
    const selectedRessources = await getSelectedObject(interventionId, 't_interventionressource_irc', 'irc_intervention', 'irc_ressource')
  
    intervention.selectedObjects = {
    selectedParcelles : selectedParcelles, selectedUsers : selectedUsers, selectedEchantillons : selectedEchantillons, 
    selectedEquipements : selectedEquipements, selectedAnimaux : selectedAnimaux, selectedLots : selectedLots,
    selectedElements : selectedElements, selectedRessources : selectedRessources}
  
    return intervention

  }
  catch(error){
    throw "Error"
  }
}

/**
 * @description return information about an entry
 * @param {int} idFiche if of sheets associate with this entry
 * @return {*} information about an entry {idSaisie: int, fiche: {}, dateStart: string, dateEdit : string, statut : string,
                parcelles: [{name: string, id: int}], participants: [{name: string, id: int}], echantillons: [{name: string, id: int}],
                equipements: [{name: string, id: int}], animaux: [{name: string, id: int}], lots: [{name: string, id: int}], elements: [{name: string, id: int}],
                ressources: [{name: string, id: int}], lignesintervention: []}
 */
export async function getSaisie(idFiche){
  try {
    var informationForSaisie = {idSaisie: undefined, fiche: undefined, dateStart: undefined, dateEdit : undefined, statut : 'En cours',
                              parcelles: [], participants: [], echantillons: [], equipements: [], animaux: [], lots: [], elements: [], ressources: [],
                              lignesintervention: []}

    // Get Fiche
    var res = await Database.getElement('t_fiche_fic','fic_id' ,idFiche)
    if (res.length >0 ){
      const fiche = res[0]
      informationForSaisie.fiche = fiche
    }

    // Get saisie
    res = await Database.getElement('t_saisie_sas', 'sas_fiche', idFiche)
    if(res.length >0){
      informationForSaisie.dateStart = res[0].sas_creation
      informationForSaisie.dateEdit = res[0].sas_modification
      informationForSaisie.idSaisie = res[0].sas_id
      informationForSaisie.idStatut = res[0].sas_statut
      res = await Database.getElement('t_statutsaisie_sts','sts_id',res[0].sas_statut)
      if(res.length >0){
        informationForSaisie.statut = res[0].sts_nom
      }
    }

    // Get users
    res = await Database.getTable('ts_utilisateur_uti')
    for(var i = 0; i < res.length; i++){
      const name = res[i].uti_prenom + " "+ res[i].uti_nom
      const participant = {id: res[i].uti_id, name: name}
      informationForSaisie.participants.push(participant)
    }

    // Get object's list
    informationForSaisie.parcelles = await getObjects('t_parcelle_prc', 'prc_id', 'prc_nom')
    informationForSaisie.echantillons = await getObjects('t_echantillon_ech', 'ech_id', 'ech_nom')
    informationForSaisie.equipements = await getObjects('t_equipement_eqp', 'eqp_id', 'eqp_nom')
    informationForSaisie.animaux = await getObjects('t_animal_anm', 'anm_id', 'anm_nom')
    informationForSaisie.lots = await getObjects('t_lotanimaux_lax', 'lax_id', 'lax_description')
    informationForSaisie.elements = await getObjects('t_element_elt', 'elt_id', 'elt_nom')
    informationForSaisie.ressources = await getObjects('t_ressources_rsc', 'rsc_id', 'rsc_nom')

    // Get ligne intervention
    res = await Database.getElement('t_ligneintervention_lii', 'lii_fich_id', idFiche)
    for(var i = 0; i< res.length; i++){
        const currIntervention = res[i]
        const intervention = await getInterventionForSaisie(currIntervention,informationForSaisie.idSaisie)
        informationForSaisie.lignesintervention.push(intervention)
    }
    informationForSaisie.lignesintervention = sortByPosition(informationForSaisie.lignesintervention)
    return informationForSaisie
    }
    catch(error){
      throw "Error"
    }  
}


/** UPDATE SAISIE */


/**
 * @description insert an array of selected objects for an intervention
 * @param {Array} selectedObjects array of selected object
 * @param {string} table name of table for insert
 * @param {string} nameIdIntervention name of column with id of intervention
 * @param {int} idIntervention id of intervention
 * @param {string} nameIdObject name of column to insert selected object
 */
async function insertSelectedObjects(selectedObjects, table, nameIdIntervention, idIntervention, nameIdObject){
  try {
    for(var i = 0; i < selectedObjects.length; i ++){
      await Database.insertSelectedObject(table, nameIdIntervention, idIntervention, nameIdObject, selectedObjects[i])
    }
  }
  catch(error){
    throw "Error"
  }
}

/**
 * @description insert an array of settings for an intervention
 * @param {int} idIntervention id of intervention
 * @param {Array} parametres array of settings
 */
async function insertParametres(idIntervention, parametres){
  try{
    for(var i = 0; i < parametres.length; i ++){
      if (parametres[i].value !== undefined){
        await Database.insertParametre(idIntervention, parametres[i].id, parametres[i].value)
      }
    }
  }
  catch(error){
    throw "Error"
  }
}

/**
 * @description insert all type of selected objects in database for an intervention
 * @param {int} idIntervention id of intervention
 * @param {*} ligneIntervention line of intervention
 */
async function insertObjectsAndParametres(idIntervention, ligneIntervention){
  try {
    await insertSelectedObjects(ligneIntervention.selectedObjects.selectedParcelles, 't_interventionparcelle_ipa', 'ipa_intervention', idIntervention, 'ipa_parcelle')
    await insertSelectedObjects(ligneIntervention.selectedObjects.selectedUsers, 't_interventionutilisateurs_iut', 'iut_intervention', idIntervention, 'iut_utilisateur')
    await insertSelectedObjects(ligneIntervention.selectedObjects.selectedEchantillons, 't_interventionechantillon_iec', 'iec_intervention', idIntervention, 'iec_echantillon')
    await insertSelectedObjects(ligneIntervention.selectedObjects.selectedEquipements, 't_interventionequipement_ieq', 'ieq_intervention', idIntervention, 'ieq_equipement')
    await insertSelectedObjects(ligneIntervention.selectedObjects.selectedAnimaux, 't_interventionanimaux_ian', 'ian_intervention', idIntervention, 'ian_animal')
    await insertSelectedObjects(ligneIntervention.selectedObjects.selectedLots, 't_interventionlotanimaux_ila', 'ila_intervention', idIntervention, 'ila_lotanimaux')
    await insertSelectedObjects(ligneIntervention.selectedObjects.selectedElements, 't_interventionelement_iel', 'iel_intervention', idIntervention, 'iel_element')
    await insertSelectedObjects(ligneIntervention.selectedObjects.selectedRessources, 't_interventionressource_irc', 'irc_intervention', idIntervention, 'irc_ressource')
    await insertParametres(idIntervention, ligneIntervention.parametres)
  }
  catch (error){
    throw "Error"
  }
}

/**
 * @description insert a new intervention in database
 * @param {int} idSaisie id of entry for this intervention
 * @param {*} ligneIntervention information about intervention
 * @return {int} id of new intervention
 */
async function insertIntervention(idSaisie, ligneIntervention) {
  try {
    const newId = await Database.createIntervention(idSaisie, ligneIntervention.type, ligneIntervention.modeop)
    return newId
  }
  catch (error){
    throw "Error"
  }
}

/**
 * @description insert new measurement for an intervention
 * @param {int} interventionId id of intervention
 * @param {Array} mesures array with information about measurements
 * @return {Array} array with informtion about measurements updated (ie add id)
 */
async function insertMesures(interventionId, mesures){
  try {
    for(var i = 0; i < mesures.length; i++){
      const currMesure = mesures[i]
      var mesureId = currMesure.mesureId
      if(currMesure.mesureId === undefined || currMesure.mesureId === null){
          mesureId = await Database.createMesure(currMesure.methodeId, interventionId)
          mesures[i].mesureId = mesureId
      }
  
      const selectedParcelle = currMesure.selectedParcelle === undefined ? null : currMesure.selectedParcelle
      const selectedEchantillon = currMesure.selectedEchantillon === undefined ? null : currMesure.selectedEchantillon
      await Database.update('t_mesure_mes','mes_parcelle', selectedParcelle, 'mes_id',mesureId)
      await Database.update('t_mesure_mes','mes_echantillon', selectedEchantillon, 'mes_id', mesureId)
      await Database.delete('t_mesurevariable_mvr', 'mvr_mesure', mesureId)
  
      const newValue =  currMesure.value 
      await Database.insertSelectedObject('t_mesurevariable_mvr', 'mvr_mesure', mesureId, 'mvr_value', newValue)
      await Database.delete('t_mesureequipement_meq', 'meq_mesure', mesureId)
      for (var j = 0; j < currMesure.selectedEquipements.length ; j ++){
        await Database.insertSelectedObject('t_mesureequipement_meq', 'meq_mesure', mesureId, 'meq_equipement', currMesure.selectedEquipements[j])
      }
    }
    return mesures
  }
  catch(error){
    throw "Error"
  }
}

/**
 * @description insert a new entry and dependency in database
 * @param {int} idFiche id of sheet
 * @param {Array} lignesIntervention array with information about lines of intervention
 * @return {*} data updated with new id
 */
async function saveNewSaisie(idFiche, lignesIntervention){
  try{
    var idSaisie = undefined
    const resUser = await Database.getElement('ts_utilisateur_uti', 'uti_login', DeviceData.userName)
    if (resUser.length > 0){
        const user = resUser[0]
        idSaisie = await Database.createSaisie(user.uti_id, idFiche)
        for(var i = 0; i < lignesIntervention.length; i++){
          const idIntervention = await insertIntervention(idSaisie, lignesIntervention[i])
          await insertObjectsAndParametres(idIntervention, lignesIntervention[i])
          lignesIntervention[i].idIntervention = idIntervention
          lignesIntervention[i].mesures = await insertMesures(idIntervention, lignesIntervention[i].mesures)
        }
    }
    return {idSaisie: idSaisie, lignesIntervention: lignesIntervention}
  }
  catch (error){
    throw "Error"
  }
}

/**
 * @description update an entry and dependency in database
 * @param {int} idSaisie id of entry
 * @param {*} lignesIntervention array with information about lines of intervention
 */
async function updateSaisie(idSaisie, lignesIntervention){
  try {
    const date = "'" +  new Date().toString() + "'"
    await Database.update('t_saisie_sas','sas_modification', date, 'sas_id', idSaisie)
    for(var i = 0; i < lignesIntervention.length; i++){
      const idIntervention = lignesIntervention[i].idIntervention
      await Database.delete('t_interventionparcelle_ipa', 'ipa_intervention', idIntervention)
      await Database.delete('t_interventionutilisateurs_iut', 'iut_intervention', idIntervention)
      await Database.delete('t_interventionechantillon_iec', 'iec_intervention', idIntervention)
      await Database.delete('t_interventionequipement_ieq', 'ieq_intervention', idIntervention)
      await Database.delete('t_interventionanimaux_ian', 'ian_intervention', idIntervention)
      await Database.delete('t_interventionlotanimaux_ila', 'ila_intervention', idIntervention)
      await Database.delete('t_interventionelement_iel', 'iel_intervention', idIntervention)
      await Database.delete('t_interventionressource_irc', 'irc_intervention', idIntervention)
      await Database.delete('t_valeurparametres_vpa', 'vpa_intervention', idIntervention)
      await insertObjectsAndParametres(idIntervention, lignesIntervention[i])
      await insertMesures(idIntervention, lignesIntervention[i].mesures)
    }
  }
  catch (error) {
    throw "Error update"
  }
}

/**
 * @description save an entry (insert or update information)
 * @param {int} idSaisie id of entry
 * @param {int} idFiche if of sheet
 * @param {*} lignesIntervention array with information about lines of intervention
 * @return data updated with new id
 */
export async function saveSaisie(idSaisie, idFiche, lignesIntervention){
  try {
    var updatedData = {idSaisie: idSaisie, lignesIntervention: lignesIntervention} 
    if (idSaisie === null || idSaisie === undefined){
      updatedData = await saveNewSaisie(idFiche, lignesIntervention)
    }
    else{
      await updateSaisie(idSaisie, lignesIntervention)
    }
    return updatedData
  }
  catch (error){
    throw "Error"
  }
}

/**
 * @description close an entry (save informations and change state )
 * @param {int} idSaisie  id of entry
 * @param {int} idFiche if of sheet
 * @param {*} lignesIntervention array with information about lines of intervention
 * @return data updated with new id
 */
export async function closeSaisie(idSaisie, idFiche, lignesIntervention){
  try{
    var updatedData = {idSaisie: idSaisie, lignesIntervention: lignesIntervention} 
    updatedData = await saveSaisie(idSaisie, idFiche, lignesIntervention)
    await Database.update('t_saisie_sas','sas_statut', 2, 'sas_id', updatedData.idSaisie)
    return updatedData
  }
  catch(error){
    throw "Error"
  }
}