import Database from '../Database'

/**
 * @description delete all measurements and dependency for an intervention
 * @param {int} idIntervention id of intervention
 */
async function deleteMesures(idIntervention){
    try {
        const resMesures =  await Database.getElement('t_intervention_int','int_saisie', idIntervention)
        for(var i =0; i <  resMesures.length; i++){
            const idMesure = resMesures[i].mes_id
            await Database.delete('t_mesurevariable_mvr', 'mvr_mesure', idMesure)
            await Database.delete('t_mesureequipement_meq', 'meq_mesure', idMesure)
        }
        await Database.delete('t_intervention_int','int_saisie', idIntervention)
    }
    catch(error){
        throw "Error delete measurement"
    }
}

/**
 * @description delete all information about an intervention and dependency for an entry
 * @param {int} idSaisie id of entry
 */
async function deleteInterventions(idSaisie){
    try {
      const resIntervention = await Database.getElement('t_intervention_int','int_saisie', idSaisie)
      for(var i = 0; i < resIntervention.length; i++){
        const idIntervention = resIntervention[i].int_id
        await Database.delete('t_interventionparcelle_ipa', 'ipa_intervention', idIntervention)
        await Database.delete('t_interventionutilisateurs_iut', 'iut_intervention', idIntervention)
        await Database.delete('t_interventionechantillon_iec', 'iec_intervention', idIntervention)
        await Database.delete('t_interventionequipement_ieq', 'ieq_intervention', idIntervention)
        await Database.delete('t_interventionanimaux_ian', 'ian_intervention', idIntervention)
        await Database.delete('t_interventionlotanimaux_ila', 'ila_intervention', idIntervention)
        await Database.delete('t_interventionelement_iel', 'iel_intervention', idIntervention)
        await Database.delete('t_interventionressource_irc', 'irc_intervention', idIntervention)
        await Database.delete('t_valeurparametres_vpa', 'vpa_intervention', idIntervention)
        await deleteMesures(idIntervention)
      }
      await  Database.delete('t_intervention_int','int_saisie', idSaisie)
    }
    catch (error) {
      throw "Error delete intervention"
    }
}

/**
 * @description delete all information about entry and dependency without sheet
 */
async function deleteSaisies(){
    try{
        const resAllSaisies = await Database.getTable('t_saisie_sas')
        for(var i =0; i <resAllSaisies.length; i++){
            const currSaisie = resAllSaisies[i]
            const resFiche = await Database.getElement('t_fiche_fic','fic_id' ,currSaisie.sas_fiche)
            if(resFiche.length === 0){
                await deleteInterventions(currSaisie.sas_id)
                await Database.delete('t_saisie_sas', 'sas_id',currSaisie.sas_id)
            }
        }
    }
    catch(error){
        throw "Error delete"
    }
}

/**
 * @description save new data in database (replaces old information) and delete entry without sheet
 * @param {*} data new data
 */
export async function updateData(data){
    try {
        await Database.saveData(data)
        await deleteSaisies()
    }
    catch(error){
        throw "Error"
    }
}