import Database from '../Database'


/**
 * Business layer fo general information (list and details)
 */



/** LIST */



/**
 * @description return a list with data of specific categorie (ie table)
 * @param {string} categorie
 * @return {Array} a array with data {id, name}
 */
export async function getList (categorie) {
    var list = []
    switch(categorie) {
        case 'Animaux':
          list = await createListCategorie('t_animal_anm', 'anm_id', 'anm_nom')
          break;
        case 'Dispositifs':
          list = await createListCategorie('ts_dispositif_dis','dis_id', 'dis_nom' )
          break;
        case 'Echantillons':
          list = await createListCategorie('t_echantillon_ech', 'ech_id','ech_nom')
          break;
        case 'Elément du paysage':
          list = await createListCategorie('t_element_elt', 'elt_id','elt_nom')
          break;
        case 'Equipements':
          list = await createListCategorie('t_equipement_eqp', 'eqp_id', 'eqp_nom')
          break;
        case "Lots d'animaux": 
          list = await createListCategorie('t_lotanimaux_lax', 'lax_id','lax_description')
          break;
        case "Méthodes de mesure":
          list = await createListCategorie('tr_methode_met', 'met_id', 'met_nom')
          break;
        case "Modes opératoires": 
          list = await createListCategorie('tr_modeoperatoire_mod', 'mod_id', 'mod_nom')
          break;
        case "Paramètres":
          list = await createListCategorie('tr_parametre_paa', 'paa_id', 'paa_description')
          break;
        case "Parcelles":
          list = await createListCategorie('t_parcelle_prc', 'prc_id', 'prc_nom')
          break;
        case "Ressources":
          list = await createListCategorie('t_ressources_rsc', 'rsc_id', 'rsc_nom')
          break;
        case "Types d'intervention":
          list = await createListCategorie('tr_typeintervention_tyi', 'tyi_id', 'tyi_nom')
          break; 
        case "Unités":
          list = await createListCategorie('tr_unite_uni', 'uni_id', 'uni_nom')
          break; 
        default:
      }
    return list
}


/**
 * @description return a list of all element of a category 
 * @param {string} table name of table where search element
 * @param {string} nameColumnId name of column with element's id
 * @param {string} nameColumnName name of column wiht element's name
 * @return {Array} a array with animals {id, name}
 */
export async function createListCategorie(table, nameColumnId, nameColumnName){
    var list = []
    const res = await Database.getTable(table)
    for(var i = 0; i < res.length; i++){
      list.push({id: res[i][nameColumnId], name: res[i][nameColumnName]})
    }
    return list
}


/** DETAILS */

/**
 * @description return a element of specific categorie (ie table) with a specific id
 * @param {string} categorie categorie of element
 * @param {int} id id of element
 * @return {*} element 
 */
export async function getElement (categorie, id) {
  var element = undefined
  switch(categorie) {
      case 'Animaux':
        element = await getFirstElement('t_animal_anm','anm_id' ,id)
        break;
      case 'Dispositifs':
        element = await getFirstElement('ts_dispositif_dis','dis_id' ,id)
        break;
      case 'Echantillons':
        element = await getFirstElement('t_echantillon_ech','ech_id' ,id)
        break;
      case 'Elément du paysage':
        element = await getFirstElement('t_element_elt','elt_id' ,id)
        break;
      case 'Equipements':
        element = await getFirstElement('t_equipement_eqp','eqp_id' ,id)
        break;
      case "Lots d'animaux": 
        element = await getFirstElement('t_lotanimaux_lax','lax_id' ,id)
        break;
      case "Méthodes de mesure":
        element = await getFirstElement('tr_methode_met','met_id' ,id)
        break;
      case "Modes opératoires": 
        element = await getFirstElement('tr_modeoperatoire_mod','mod_id' ,id)
        break;
      case "Paramètres":
        element = await getFirstElement('tr_parametre_paa','paa_id' ,id)
        break;
      case "Parcelles":
        element = await getFirstElement('t_parcelle_prc','prc_id' ,id)
        break;
      case "Ressources":
        element = await getFirstElement('t_ressources_rsc','rsc_id' ,id)
        break;
      case "Types d'intervention":
        element = await getFirstElement('tr_typeintervention_tyi','tyi_id' ,id)
        break; 
      case "Unités":
        element = await getFirstElement('tr_unite_uni','uni_id' ,id)
        break; 
      default:
    }
  return element
}

/**
 * @description get details information about the first element that meets the conditions
 * @param {string} table name of table where search information
 * @param {string} nameColumnId name of column with id
 * @param {*} id 
 * @return {*} object with information
 */
export async function getFirstElement(table, nameColumnId, id){
  var element = undefined
  const res = await Database.getElement(table, nameColumnId ,id)
  if (res.length >0 ){
    element = res[0]
  }
  return element
}