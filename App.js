import 'react-native-gesture-handler'
import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { DefaultTheme, Provider as PaperProvider } from 'react-native-paper';

import HomeScreen from './screen/HomeScreen'
import LoginScreen from './screen/LoginScreen'
import SiteScreen from './screen/SiteScreen'
import UploadScreen from './screen/UploadScreen'
import DownloadScreen from './screen/DownloadScreen'
import FichesScreen from './screen/FichesScreen'
import InformationsScreen from './screen/InformationsScreen'
import ListScreen from './screen/ListScreen'
import DeviceData from './sessionData/DeviceData'
import AnimalDetailsScreen from './screen/detailsScreen/AnimalDetailsScreen'
import DispositifDetailsScreen from './screen/detailsScreen/DispositifDetailsScreen'
import EchantillonDetailsScreen from './screen/detailsScreen/EchantillonDetailsScreen'
import ElementDetailsScreen from './screen/detailsScreen/ElementDetailsScreen'
import EquipementDetailsScreen from './screen/detailsScreen/EquipementDetailsScreen'
import LotDetailsScreen from './screen/detailsScreen/LotDetailsScreen'
import MethodeDetailsScreen from './screen/detailsScreen/MethodeDetailsScreen'
import ModeOperatoireDetailsScreen from './screen/detailsScreen/ModeOpDetailsScreen'
import ParametreDetailsScreen from './screen/detailsScreen/ParametreDetailsScreen'
import ParcelleDetailsScreen from './screen/detailsScreen/ParcelleDetailsScreen'
import RessourceDetailsScreen from './screen/detailsScreen/RessourceDetailsScreen'
import TypeInterventionDetailsScreen from './screen/detailsScreen/TypeInterventionDetailsScreen'
import UniteDetailsScreen from './screen/detailsScreen/UniteDetailsScreen'
import FicheAndSaisieScreen from './screen/ficheAndSaisie/BottomNavigationScreen'
import FicheScreen from './screen/ficheAndSaisie/FicheScreen'
import Database from './Database'

const Stack = createStackNavigator();

const theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: '#4caf50',
    accent: '#80ccc4',
  },
};



function App() {
  DeviceData.init()
  Database.initDB()
  console.disableYellowBox = true
  return (
    <NavigationContainer>
      <PaperProvider theme={theme}>
        <Stack.Navigator initialRouteName="Login" headerMode="none">
          <Stack.Screen name="Login" component={LoginScreen} />
          <Stack.Screen name="Home" component={HomeScreen} />
          <Stack.Screen name="Site" component={SiteScreen} />
          <Stack.Screen name="Upload" component={UploadScreen} />
          <Stack.Screen name="Download" component={DownloadScreen} />
          <Stack.Screen name="Fiches" component={FichesScreen} />
          <Stack.Screen name="Informations" component={InformationsScreen} />
          <Stack.Screen name="List" component={ListScreen} />
          <Stack.Screen name="AnimalDetails" component={AnimalDetailsScreen} />
          <Stack.Screen name="DispositifDetails" component={DispositifDetailsScreen} />
          <Stack.Screen name="EchantillonDetails" component={EchantillonDetailsScreen} />
          <Stack.Screen name="ElementDetails" component={ElementDetailsScreen} />
          <Stack.Screen name="EquipementDetails" component={EquipementDetailsScreen} />
          <Stack.Screen name="LotDetails" component={LotDetailsScreen} />
          <Stack.Screen name="MethodeDetails" component={MethodeDetailsScreen} />
          <Stack.Screen name="ModeOpDetails" component={ModeOperatoireDetailsScreen} />
          <Stack.Screen name="ParametreDetails" component={ParametreDetailsScreen} />
          <Stack.Screen name="ParcelleDetails" component={ParcelleDetailsScreen} />
          <Stack.Screen name="RessourceDetails" component={RessourceDetailsScreen} />
          <Stack.Screen name="TypeInterventionDetails" component={TypeInterventionDetailsScreen} />
          <Stack.Screen name="UniteDetails" component={UniteDetailsScreen} />
          <Stack.Screen name="FicheAndSaisie" component={FicheAndSaisieScreen} />
          <Stack.Screen name="Fiche" component={FicheScreen} />
        </Stack.Navigator>
      </PaperProvider>
    </NavigationContainer>
  );
}

export default App;
