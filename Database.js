import SQLite from "react-native-sqlite-storage"

SQLite.enablePromise(true)

const database_name = "GaelOffline.db";
const database_version = "1.0";
const database_displayname = "SQLite React Offline Database for Gael app";
const database_size = 200000;


/**
 * Database : static class with method to manage database
 */
export default class Database {

    static db = null

    /**
     * @description create database and table
     */
    static initDB() {
        return new Promise((resolve) => {
          SQLite.echoTest()
            .then(() => {
              SQLite.openDatabase(database_name, database_version, database_displayname,database_size)
                .then(DB => {
                  this.db = DB
                  this.db.executeSql("SELECT * FROM ts_utilisateur_uti").then(() => {
                      console.log("Database is ready")
                }).catch((error) =>{
                    this.db.transaction((tx) => {
                        tx.executeSql('CREATE TABLE IF NOT EXISTS t_animal_anm (anm_id integer NOT NULL PRIMARY KEY, anm_code1 character varying, anm_code2 character varying, anm_nom character varying, anm_sex character varying, anm_debut date, anm_fin date, anm_site character varying )')
                        tx.executeSql('CREATE TABLE IF NOT EXISTS t_echantillon_ech (ech_id integer NOT NULL PRIMARY KEY, ech_nom character varying)')
                        tx.executeSql('CREATE TABLE IF NOT EXISTS t_element_elt (elt_id integer NOT NULL PRIMARY KEY, elt_code character varying, elt_nom character varying, elt_debut date, elt_fin date, elt_1 "char", elt_2 "char", elt_3 "char", elt_4 "char", elt_5 "char", elt_6 "char", elt_7 "char", elt_lieu character varying)')
                        tx.executeSql('CREATE TABLE IF NOT EXISTS t_equipement_eqp (eqp_id integer NOT NULL PRIMARY KEY, eqp_idb integer, eqp_code character varying, eqp_site character varying, eqp_debut date, eqp_fin date, eqp_commentaire character varying, eqp_type character varying, eqp_nom character varying)')                     
                        tx.executeSql('CREATE TABLE IF NOT EXISTS t_fiche_fic (fic_id integer NOT NULL PRIMARY KEY, fic_nom character varying(50), fic_date date, fic_idb integer, fic_stf_id integer, fic_uti_id integer, fic_sit_id integer,fic_dis_id integer)')

                        tx.executeSql('CREATE TABLE IF NOT EXISTS t_intervention_int (int_id integer NOT NULL PRIMARY KEY AUTOINCREMENT, int_saisie integer, int_type integer, int_modeop integer);')
                        tx.executeSql('CREATE TABLE IF NOT EXISTS t_interventionanimaux_ian (ian_id integer NOT NULL PRIMARY KEY AUTOINCREMENT, ian_intervention integer, ian_animal integer)')
                        tx.executeSql('CREATE TABLE IF NOT EXISTS t_interventionechantillon_iec (iec_id integer NOT NULL PRIMARY KEY AUTOINCREMENT, iec_intervention integer, iec_echantillon integer)')
                        tx.executeSql('CREATE TABLE IF NOT EXISTS t_interventionelement_iel (iel_id integer NOT NULL PRIMARY KEY AUTOINCREMENT, iel_intervention integer,iel_element integer);')
                        tx.executeSql('CREATE TABLE IF NOT EXISTS t_interventionequipement_ieq (ieq_id integer NOT NULL PRIMARY KEY AUTOINCREMENT, ieq_intervention integer, ieq_equipement integer);')

                        tx.executeSql('CREATE TABLE IF NOT EXISTS t_interventionlotanimaux_ila (ila_id integer NOT NULL PRIMARY KEY AUTOINCREMENT, ila_intervention integer, ila_lotanimaux integer)')   
                        tx.executeSql('CREATE TABLE IF NOT EXISTS t_interventionparcelle_ipa (ipa_id integer NOT NULL PRIMARY KEY AUTOINCREMENT, ipa_intervention integer,ipa_parcelle integer);')
                        tx.executeSql('CREATE TABLE IF NOT EXISTS t_interventionressource_irc (irc_id integer NOT NULL PRIMARY KEY AUTOINCREMENT, irc_intervention integer, irc_ressource integer)')
                        tx.executeSql('CREATE TABLE IF NOT EXISTS t_interventionutilisateurs_iut (iut_id integer NOT NULL PRIMARY KEY AUTOINCREMENT, iut_intervention integer, iut_utilisateur integer)')
                        tx.executeSql('CREATE TABLE IF NOT EXISTS t_ligneintervention_lii (lii_id integer NOT NULL PRIMARY KEY, lii_libelle character varying, lii_bparcelle boolean, lii_bechantillon boolean, lii_banimaux boolean, lii_belementpaysage boolean, lii_bequipement boolean, lii_bressource boolean, lii_blotsanimaux boolean, position integer, lii_tyi_id integer, lii_mod_id integer, lii_fich_id integer)')
                        
                        tx.executeSql('CREATE TABLE IF NOT EXISTS t_lignesmesure_lgm (lgm_id integer NOT NULL PRIMARY KEY AUTOINCREMENT, lgm_libelle character varying, lgm_bEchantillon boolean, lgm_bEquipement boolean, lgm_met_id integer, lgm_position integer, lgm_lii_id integer)')
                        tx.executeSql('CREATE TABLE IF NOT EXISTS t_lotanimaux_lax (lax_id integer NOT NULL PRIMARY KEY, lax_code character varying, lax_description character varying, lax_debut date, lax_fin date);')
                        tx.executeSql('CREATE TABLE IF NOT EXISTS t_mesure_mes (mes_id integer NOT NULL PRIMARY KEY AUTOINCREMENT, mes_parcelle integer, mes_methode integer, mes_echantillon integer,mes_intervention integer);')
                        tx.executeSql('CREATE TABLE IF NOT EXISTS t_mesureequipement_meq ( meq_id integer NOT NULL PRIMARY KEY AUTOINCREMENT, meq_mesure integer, meq_equipement integer);')
                        tx.executeSql('CREATE TABLE IF NOT EXISTS t_mesurevariable_mvr (mvr_id integer NOT NULL PRIMARY KEY AUTOINCREMENT, mvr_mesure integer, mvr_value character varying);')
                        
                        tx.executeSql('CREATE TABLE IF NOT EXISTS t_parcelle_prc (prc_id integer NOT NULL PRIMARY KEY , prc_idb integer, prc_nom character varying, prc_debut date, prc_fin date, prc_commentaire character varying, prc_point character varying, prc_site character varying );')
                        tx.executeSql('CREATE TABLE IF NOT EXISTS t_ressources_rsc (rsc_id integer NOT NULL PRIMARY KEY, rsc_nom character varying, rsc_type character varying);')
                        tx.executeSql('CREATE TABLE IF NOT EXISTS t_saisie_sas (sas_id integer NOT NULL PRIMARY KEY AUTOINCREMENT, sas_creation date, sas_modification date,  sas_utilisateur integer, sas_statut integer, sas_fiche integer);')
                        tx.executeSql('CREATE TABLE IF NOT EXISTS t_statut_st (st_id integer NOT NULL PRIMARY KEY, st_nom character varying);')
                        tx.executeSql('CREATE TABLE IF NOT EXISTS t_statutsaisie_sts (sts_id integer NOT NULL PRIMARY KEY, sts_nom character varying)')

                        tx.executeSql('CREATE TABLE IF NOT EXISTS t_valeurparametres_vpa (vpa_id integer NOT NULL PRIMARY KEY AUTOINCREMENT, vpa_intervention integer, vpa_parametre integer, vpa_value character varying)')
                        tx.executeSql('CREATE TABLE IF NOT EXISTS tj_ficheligneintervention_fli (fli_id integer NOT NULL PRIMARY KEY, fli_fiche_id integer, fli_intervention_id integer);')
                        tx.executeSql('CREATE TABLE IF NOT EXISTS tj_utilisateursite_usi (usi_id integer NOT NULL PRIMARY KEY AUTOINCREMENT, usi_uti_id character varying, usi_sit_id character varying, usi_rattachement boolean, usi_role character varying);')
                        tx.executeSql('CREATE TABLE IF NOT EXISTS tr_methode_met ( met_id integer NOT NULL PRIMARY KEY, met_nom character varying, met_code character varying, met_description character varying, met_seuil_inf real, met_seuil_sup real, met_precision real, met_uni_id character varying, met_site_id character varying);')
                        tx.executeSql('CREATE TABLE IF NOT EXISTS tr_modeoperatoire_mod ( mod_id integer NOT NULL PRIMARY KEY, mod_nom character varying, mod_code character varying, mod_description character varying, mod_document character varying, mod_debut date, mod_fin date, mod_tyi_id character varying, mod_sit_id character varying, mod_reference character varying);')

                        tx.executeSql('CREATE TABLE IF NOT EXISTS tr_parametre_paa (paa_id integer NOT NULL PRIMARY KEY, paa_nom character varying, paa_code character varying, paa_description character varying, paa_tyi_id character varying);')
                        tx.executeSql('CREATE TABLE IF NOT EXISTS tr_typeintervention_tyi (tyi_id integer NOT NULL PRIMARY KEY, tyi_nom character varying, tyi_code character varying,tyi_description character varying, tyi_document character varying,tyi_produit character varying);')
                        tx.executeSql('CREATE TABLE IF NOT EXISTS tr_unite_uni (uni_id integer NOT NULL PRIMARY KEY, uni_symbole character varying, uni_nom character varying);')
                        tx.executeSql('CREATE TABLE IF NOT EXISTS ts_dispositif_dis (dis_id integer NOT NULL PRIMARY KEY, dis_nom character varying(250), dis_debut date, dis_fin date, dis_logo character varying, dis_reference character varying, dis_description character varying, dis_protocole character varying, dis_site_id character varying);')
                        tx.executeSql('CREATE TABLE IF NOT EXISTS ts_site_sit (sit_id integer NOT NULL PRIMARY KEY AUTOINCREMENT, sit_nom character varying, sit_departement integer);')

                        tx.executeSql('CREATE TABLE IF NOT EXISTS ts_utilisateur_uti (uti_id integer NOT NULL PRIMARY KEY, uti_nom character varying, uti_prenom character varying, uti_actif character varying, dn character varying, uti_login character varying, uti_mobile_mdp character varying);')

                    }).then(() => {
                        console.log("Table created successfully")
                    }).catch(error => {
                        console.log(error)
                    })
                })
                })
                .catch(error => {
                  console.log(error)
                })
            })
            .catch(error => {
              console.log("echoTest failed - plugin not functional")
            });
          });
    }


    /** SAVE DATA IN TABLE  */


    static deleteInformationAboutEntry(){
      return new Promise((resolve, reject) => {
          this.db.transaction((tx) => {
            tx.executeSql('DELETE FROM t_saisie_sas')
            tx.executeSql('DELETE FROM t_intervention_int ')
            tx.executeSql('DELETE FROM t_interventionanimaux_ian')
            tx.executeSql('DELETE FROM t_interventionechantillon_iec')
            tx.executeSql('DELETE FROM t_interventionelement_iel')
            tx.executeSql('DELETE FROM t_interventionequipement_ieq')
            tx.executeSql('DELETE FROM t_interventionlotanimaux_ila')   
            tx.executeSql('DELETE FROM t_interventionparcelle_ipa')
            tx.executeSql('DELETE FROM t_interventionressource_irc')
            tx.executeSql('DELETE FROM t_interventionutilisateurs_iut')
            tx.executeSql('DELETE FROM t_mesureequipement_meq')
            tx.executeSql('DELETE FROM t_mesurevariable_mvr')
            tx.executeSql('DELETE FROM t_valeurparametres_vpa')
        }).then(() => {
            resolve('Delete')
        }).catch(error => {
            reject(error)
        })
      }) 
    }

    /**
     * @description save data in database
     * @param {*} data data to save 
     */
    static saveData(data) {
      this.saveParametres(data.parametres)
      this.saveUnites(data.unites)
      this.saveStatuts(data.statuts)
      this.saveStatutsSaisies(data.statutsaisie)
      this.saveTypesIntervention(data.typesintervention)
      this.saveLotsAnimaux(data.lotsanimaux)
      this.saveRessources(data.ressources)
      this.saveEchantillons(data.echantillons)
      this.saveElements(data.elements)
      this.saveUsers(data.users)
      this.saveDispositifs(data.dispositifs)
      this.saveModeOp(data.modop)
      this.saveMethodes(data.methodes)
      this.saveParcelles(data.parcelles)
      this.saveEquipements(data.equipements)
      this.saveFiches(data.fiches)
      this.saveLinkFicheIntervention(data.linkficheintervention)
      this.saveLignesInterventions(data.interventions)
      this.saveLignesMesures(data.mesures)
      this.saveAnimaux(data.animaux)
    }

    /**
     * @description save an array of settings in database 
     * @param {array} parametres array of settings
     */
    static saveParametres(parametres){
      this.db.transaction((tx) => {
        tx.executeSql('DELETE FROM tr_parametre_paa')
        for(var i in parametres){
            var query = 'INSERT INTO tr_parametre_paa(paa_id, paa_nom, paa_code, paa_description, paa_tyi_id) VALUES(?,?,?,?,?)'
            tx.executeSql(query, [parametres[i].paa_id, parametres[i].paa_nom, parametres[i].paa_code, parametres[i].paa_description, parametres[i].paa_tyi_id])
          }
        })
    }

    /**
     * @description save an array of units in database 
     * @param {array} unites array of units
     */
    static saveUnites(unites){
      this.db.transaction((tx) => {
        tx.executeSql('DELETE FROM tr_unite_uni')
        for(var i in unites){
            var query = 'INSERT INTO tr_unite_uni(uni_id, uni_symbole, uni_nom) VALUES(?,?,?)'
            tx.executeSql(query, [unites[i].uni_id, unites[i].uni_symbole, unites[i].uni_nom])
          }
        })
    }

    /**
     * @description save an array of statuts in database 
     * @param {array} statuts array of statuts
     */
    static saveStatuts(statuts){
      this.db.transaction((tx) => {
        tx.executeSql('DELETE FROM t_statut_st')
        for(var i in statuts){
            var query = 'INSERT INTO t_statut_st(st_id, st_nom) VALUES(?,?)'
            tx.executeSql(query, [statuts[i].st_id, statuts[i].st_nom])
          }
        })
    }

    /**
     * @description save an array of entry status in database 
     * @param {array} statutSaisie array of entry status
     */
    static saveStatutsSaisies(statutSaisie){
      this.db.transaction((tx) => {
        tx.executeSql('DELETE FROM t_statutsaisie_sts')
        for(var i in statutSaisie){
            var query = 'INSERT INTO t_statutsaisie_sts(sts_id, sts_nom) VALUES(?,?)'
            tx.executeSql(query, [statutSaisie[i].sts_id, statutSaisie[i].sts_nom])
          }
        })
    }

    /**
     * @description save an array with type of intervention in database 
     * @param {array} typesIntervention array with type of intervention
     */
    static saveTypesIntervention(typesIntervention){
      this.db.transaction((tx) => {
        tx.executeSql('DELETE FROM tr_typeintervention_tyi')
        for(var i in typesIntervention){
            var query = 'INSERT INTO tr_typeintervention_tyi(tyi_id, tyi_nom, tyi_code, tyi_description, tyi_document, tyi_produit) VALUES(?,?,?,?,?,?)'
            tx.executeSql(query, [typesIntervention[i].tyi_id, typesIntervention[i].tyi_nom, typesIntervention[i].tyi_code, typesIntervention[i].tyi_description, typesIntervention[i].tyi_document, typesIntervention[i].tyi_produit])
          }
        })
    }

    /**
     * @description save an array with animals in database 
     * @param {array} animaux array with animals
     */
    static saveAnimaux(animaux){
      this.db.transaction((tx) => {
        tx.executeSql('DELETE FROM t_animal_anm')
        for(var i in animaux){
            var query = 'INSERT INTO t_animal_anm(anm_id, anm_code1, anm_code2, anm_nom, anm_sex, anm_debut, anm_fin, anm_site) VALUES(?,?,?,?,?,?,?,?)'
            tx.executeSql(query, [animaux[i].anm_id, animaux[i].anm_code1, animaux[i].anm_code2, animaux[i].anm_nom, animaux[i].anm_sex, animaux[i].anm_debut, animaux[i].anm_fin, animaux[i].anm_site])
          }
        })
    }

    /**
     * @description save an array with lots of animals in database 
     * @param {array} lotsAnimaux array with lots of animals
     */
    static saveLotsAnimaux(lotsAnimaux){
      this.db.transaction((tx) => {
        tx.executeSql('DELETE FROM t_lotanimaux_lax')
        for(var i in lotsAnimaux){
            var query = 'INSERT INTO t_lotanimaux_lax(lax_id, lax_code, lax_description, lax_debut, lax_debut) VALUES(?,?,?,?,?)'
            tx.executeSql(query, [lotsAnimaux[i].lax_id, lotsAnimaux[i].lax_code, lotsAnimaux[i].lax_description, lotsAnimaux[i].lax_debut, lotsAnimaux[i].lax_debut])
          }
        })
    }

    /**
     * @description save an array with resources in database 
     * @param {array} ressources array with resources
     */
    static saveRessources(ressources){
      this.db.transaction((tx) => {
        tx.executeSql('DELETE FROM t_ressources_rsc')
        for(var i in ressources){
            var query = 'INSERT INTO t_ressources_rsc(rsc_id, rsc_nom, rsc_type) VALUES(?,?,?)'
            tx.executeSql(query, [ressources[i].rsc_id, ressources[i].rsc_nom, ressources[i].rsc_type])
          }
        })
    }

    /**
     * @description save an array with samples in database 
     * @param {array} echantillons array with samples
     */
    static saveEchantillons(echantillons){
      this.db.transaction((tx) => {
      tx.executeSql('DELETE FROM t_echantillon_ech')
      for(var i in echantillons){
          var query = 'INSERT INTO t_echantillon_ech(ech_id, ech_nom) VALUES(?,?)'
          tx.executeSql(query, [echantillons[i].ech_id, echantillons[i].ech_nom])
        }
      })
    }

    /**
     * @description save an array with elements in database 
     * @param {array} elements array with elements
     */
    static saveElements(elements){
      this.db.transaction((tx) => {
      tx.executeSql('DELETE FROM t_element_elt')
      for(var i in elements){
          var query = 'INSERT INTO t_element_elt(elt_id, elt_code, elt_nom, elt_debut, elt_fin, elt_1, elt_2, elt_3, elt_4, elt_5, elt_6, elt_7, elt_lieu) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)'
          tx.executeSql(query, [elements[i].elt_id, elements[i].elt_code, elements[i].elt_nom, elements[i].elt_debut, elements[i].elt_fin, elements[i].elt_1, elements[i].elt_2, elements[i].elt_3, elements[i].elt_4, elements[i].elt_5, elements[i].elt_6, elements[i].elt_7, elements[i].elt_lieu])
        }
      })
    }

    /**
     * @description save an array with users in database 
     * @param {array} users array with users
     */
    static saveUsers(users){
      this.db.transaction((tx) => {
      tx.executeSql('DELETE FROM ts_utilisateur_uti')
      for(var i in users){
          var query = 'INSERT INTO ts_utilisateur_uti(uti_id, uti_nom, uti_prenom, dn, uti_login, uti_mobile_mdp) VALUES(?,?,?,?,?,?)'
          tx.executeSql(query, [users[i].uti_id, users[i].uti_nom, users[i].uti_prenom, users[i].dn, users[i].uti_login, users[i].uti_mobile_mdp])
        }
      })
    }

    /**
     * @description save an array with plans in database 
     * @param {array} dispositifs array with plans
     */
    static saveDispositifs(dispositifs){
      this.db.transaction((tx) => {
      tx.executeSql('DELETE FROM ts_dispositif_dis')
      for(var i in dispositifs){
          var query = 'INSERT INTO ts_dispositif_dis(dis_id, dis_nom, dis_debut, dis_fin, dis_logo, dis_reference, dis_description, dis_protocole, dis_site_id) VALUES(?,?,?,?,?,?,?,?,?)'
          tx.executeSql(query, [dispositifs[i].dis_id, dispositifs[i].dis_nom, dispositifs[i].dis_debut, dispositifs[i].dis_fin, dispositifs[i].dis_logo, dispositifs[i].dis_reference, dispositifs[i].dis_description, dispositifs[i].dis_protocole, dispositifs[i].dis_site_id])
        }
      })
    }

    /**
     * @description save an array with operating mode in database 
     * @param {array} modeop array with operating mode
     */
    static saveModeOp(modeop){
      this.db.transaction((tx) => {
      tx.executeSql('DELETE FROM tr_modeoperatoire_mod')
      for(var i in modeop){
          var query = 'INSERT INTO tr_modeoperatoire_mod(mod_id, mod_nom, mod_code, mod_description, mod_document, mod_debut, mod_fin, mod_tyi_id, mod_sit_id, mod_reference) VALUES(?,?,?,?,?,?,?,?,?,?)'
          tx.executeSql(query, [modeop[i].mod_id, modeop[i].mod_nom, modeop[i].mod_code, modeop[i].mod_description, modeop[i].mod_document, modeop[i].mod_debut,modeop[i].mod_fin, modeop[i].mod_tyi_id, modeop[i].mod_sit_id, modeop[i].mod_reference])
        }
      })
    }

    /**
     * @description save an array with methods in database 
     * @param {array} methodes array with methods
     */
    static saveMethodes(methodes){
      this.db.transaction((tx) => {
      tx.executeSql('DELETE FROM tr_methode_met')
      for(var i in methodes){
          var query = 'INSERT INTO tr_methode_met(met_id, met_nom, met_code, met_description, met_seuil_inf, met_seuil_sup, met_precision, met_uni_id, met_site_id) VALUES(?,?,?,?,?,?,?,?,?)'
          tx.executeSql(query, [methodes[i].met_id, methodes[i].met_nom, methodes[i].met_code, methodes[i].met_description, methodes[i].met_seuil_inf, methodes[i].met_seuil_sup, methodes[i].met_precision, methodes[i].met_uni_id, methodes[i].met_site_id])
        }
      })
    }

    /**
     * @description save an array with plots in database 
     * @param {array} parcelles array with plots
     */
    static saveParcelles(parcelles){
      this.db.transaction((tx) => {
      tx.executeSql('DELETE FROM t_parcelle_prc')
      for(var i in parcelles){
          var query = 'INSERT INTO t_parcelle_prc(prc_id, prc_idb, prc_nom, prc_debut, prc_fin, prc_commentaire, prc_point, prc_site) VALUES(?,?,?,?,?,?,?,?)'
          tx.executeSql(query, [parcelles[i].prc_id, parcelles[i].prc_idb, parcelles[i].prc_nom, parcelles[i].prc_debut, parcelles[i].prc_fin, parcelles[i].prc_commentaire, parcelles[i].prc_point, parcelles[i].prc_site])
        }
      })
    }

    /**
     * @description save an array with equipment in database 
     * @param {array} equipements array with equipment
     */
    static saveEquipements(equipements){
      this.db.transaction((tx) => {
      tx.executeSql('DELETE FROM t_equipement_eqp')
      for(var i in equipements){
          var query = 'INSERT INTO t_equipement_eqp(eqp_id, eqp_idb, eqp_code, eqp_site, eqp_debut, eqp_fin, eqp_commentaire, eqp_type, eqp_nom) VALUES(?,?,?,?,?,?,?,?,?)'
          tx.executeSql(query, [equipements[i].eqp_id, equipements[i].eqp_idb, equipements[i].eqp_code, equipements[i].eqp_site, equipements[i].eqp_debut, equipements[i].eqp_fin, equipements[i].eqp_commentaire, equipements[i].eqp_type, equipements[i].eqp_nom])
        }
      })
    }

    /**
     * @description save an array with sheets in database 
     * @param {array} fiches array with sheets
     */
    static saveFiches(fiches){
      this.db.transaction((tx) => {
      tx.executeSql('DELETE FROM t_fiche_fic')
      for(var i in fiches){
          var query = 'INSERT INTO t_fiche_fic(fic_id, fic_nom, fic_date, fic_idb, fic_stf_id, fic_uti_id, fic_sit_id, fic_dis_id) VALUES(?,?,?,?,?,?,?,?)'
          tx.executeSql(query, [fiches[i].fic_id, fiches[i].fic_nom, fiches[i].fic_date, fiches[i].fic_idb, fiches[i].fic_stf_id, fiches[i].fic_uti_id, fiches[i].fic_sit_id, fiches[i].fic_dis_id])
        }
      })
    }

    /**
     * @description save an array with link between sheets and intervention in database 
     * @param {array} linkficheintervention array with link between sheets and intervention
     */
    static saveLinkFicheIntervention(linkficheintervention){
      this.db.transaction((tx) => {
      tx.executeSql('DELETE FROM tj_ficheligneintervention_fli')
      for(var i in linkficheintervention){
          var query = 'INSERT INTO tj_ficheligneintervention_fli(fli_id, fli_fiche_id, fli_intervention_id) VALUES(?,?,?)'
          tx.executeSql(query, [linkficheintervention[i].fli_id, linkficheintervention[i].fli_fiche_id, linkficheintervention[i].fli_intervention_id])
        }
      })
    }

    /**
     * @description save an array withintervention line in database 
     * @param {array} lignesinterventions array with intervention line
     */
    static saveLignesInterventions(lignesinterventions){
      this.db.transaction((tx) => {
      tx.executeSql('DELETE FROM t_ligneintervention_lii')
      for(var i in lignesinterventions){
          var query = 'INSERT INTO t_ligneintervention_lii(lii_id, lii_libelle, lii_bparcelle, lii_bechantillon, lii_banimaux, lii_belementpaysage, lii_bequipement, lii_bressource, lii_blotsanimaux, position, lii_tyi_id, lii_mod_id, lii_fich_id) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)'
          tx.executeSql(query, [lignesinterventions[i].lii_id, lignesinterventions[i].lii_libelle, lignesinterventions[i].lii_bparcelle, lignesinterventions[i].lii_bechantillon, lignesinterventions[i].lii_banimaux, lignesinterventions[i].lii_belementpaysage, lignesinterventions[i].lii_bequipement, lignesinterventions[i].lii_bressource, lignesinterventions[i].lii_blotsanimaux, lignesinterventions[i].position, lignesinterventions[i].lii_tyi_id, lignesinterventions[i].lii_mod_id, lignesinterventions[i].lii_fich_id])
        }
      })
    }

    /**
     * @description save an array with measurement line in database 
     * @param {array} lignesmesure array with measurement line
     */
    static saveLignesMesures(lignesmesure){
      this.db.transaction((tx) => {
      tx.executeSql('DELETE FROM t_lignesmesure_lgm')
      for(var i in lignesmesure){
          var query = 'INSERT INTO t_lignesmesure_lgm(lgm_id, lgm_libelle, lgm_bEchantillon, lgm_bEquipement, lgm_met_id, lgm_position, lgm_lii_id) VALUES(?,?,?,?,?,?,?)'
          tx.executeSql(query, [lignesmesure[i].lgm_id, lignesmesure[i].lgm_libelle, lignesmesure[i].lgm_bEchantillon, lignesmesure[i].lgm_bEquipement, lignesmesure[i].lgm_met_id, lignesmesure[i].lgm_position, lignesmesure[i].lgm_lii_id])
        }
      })
    }




    /** BASIC ACTION ON DATABASE  */


    /**
     * @description convert resultat of query to array
     * @param {*} res 
     * @return {Array} 
     */
    static convertResQueryToArray(res){
      var elements = []
      for (var i =0; i < res.rows.length; i++){
        elements.push(res.rows.item(i))
      }
      return elements
    }

    /**
     * @description get all rows of a table
     * @param {string} nameTable name of table
     * @return {Promise} promise with result of query
     */
    static getTable(nameTable){
      const query = 'SELECT * FROM ' + nameTable
      return new Promise((resolve, reject) => {
        this.db.transaction((tx) => {
          tx.executeSql(query)
            .then(([tx,results]) => {
              const elements = Database.convertResQueryToArray(results)
              resolve(elements)
            })
            .catch(() => reject())
        })
      }) 
    }

    /**
     * @description get one element of a table with one condition
     * @param {string} nameTable name of table
     * @param {string} nameId name of column of condition
     * @param {*} id value fo condition
     * @return {Promise} promise with result of query
     */
    static getElement(nameTable, nameId, id){
      const query = 'SELECT * FROM ' + nameTable + ' WHERE ' + nameId + " = ?"
      return new Promise((resolve, reject) => {
        this.db.transaction((tx) => {
          tx.executeSql(query, [id]).then(([tx,results]) => {
            const elements = Database.convertResQueryToArray(results)
            resolve(elements)
          })
          .catch(err => { reject()})
        })
      }) 
    }

    /**
     * @description get one element of a table with two condition
     * @param {string} nameTable name of table
     * @param {string} nameId name of column of condition
     * @param {*} id value fo condition
     * @return {Promise} promise with result of query
     */
    static getElementTwoConditions(nameTable, nameId, id, nameId2, id2){
      const query = 'SELECT * FROM ' + nameTable + ' WHERE ' + nameId + " = ?  AND " + nameId2 + " = ?" 
      return new Promise((resolve) => {
        this.db.transaction((tx) => {
          tx.executeSql(query, [id, id2]).then(([tx,results]) => {
            const elements = Database.convertResQueryToArray(results)
            resolve(elements)
          })
          .catch(err => reject())
        })
      }) 
    }

    /**
     * @description update element(s) of a table with one condition
     * @param {string} table name of table
     * @param {string} nameColumn name of column with update
     * @param {*} newValue new value 
     * @param {string} conditionColumn name of column for condition
     * @param {string} conditionValue value for the condition
     * @return {Promise} promise with result of query
     */
    static update(table, nameColumn, newValue, conditionColumn, conditionValue) {
      const query = 'UPDATE ' + table + ' SET ' + nameColumn + " = " + newValue + " WHERE " + conditionColumn + " = " + conditionValue
      return new Promise((resolve, reject) => {
        this.db.transaction((tx) => {
          tx.executeSql(query).then(([tx,results]) => {
            resolve('Update')
          })
          .catch(err => reject())
        })
      }) 
    }

    /**
     * @description delete element(s) of a table with one condition
     * @param {string} table name of table 
     * @param {string} conditionColumn name of column for condition
     * @param {string} conditionValue value for the condition
     * @return {Promise} promise with result of query
     */
    static delete(table, conditionColumn, conditionValue){
      const query = 'DELETE FROM '+ table + " WHERE " + conditionColumn + " = " + conditionValue
      return new Promise((resolve, reject) => {
        this.db.transaction((tx) => {
          tx.executeSql(query)
          .then(([tx,results]) => {
            resolve('delete')
          })
          .catch(err => reject())
        })
      }) 
    }



    /** INSERT SPECIFIC DATA IN DATABASE  */

    


    /**
     * @description insert a new entry in t_saisie_sas
     * @param {int} idUser id of author
     * @param {int} idFiche if of sheets
     * @return {Promise} promise with result of query
     */
    static createSaisie(idUser, idFiche){
      return new Promise((resolve, reject) => {
        this.db.transaction((tx) => {
          const date = new Date().toString()
          var query = 'INSERT INTO t_saisie_sas (sas_creation, sas_utilisateur, sas_statut, sas_fiche) VALUES(?,?,?,?)'
          tx.executeSql(query, [date,idUser, 1, idFiche])
          .then(([tx,results]) => {
            resolve(results.insertId)
          })
          .catch((err) => reject())
        })
      })
    }

    /**
     * @description insert a new measurement in t_mesure_mes
     * @param {int} methode id of methode to associate
     * @param {int} interventionId id of intervention to associate
     * @return {Promise} promise with result of query
     */
    static createMesure(methode, interventionId){
      return new Promise((resolve, reject) => {
        this.db.transaction((tx) => {
          var query = 'INSERT INTO t_mesure_mes (mes_methode, mes_intervention) VALUES(?,?)'
          tx.executeSql(query, [methode, interventionId])
          .then(([tx,results]) => {
            resolve(results.insertId)
          })
          .catch((err) => reject())
        })
      })
    }

    /**
     * @description insert a new intervention in t_intervention_int
     * @param {int} idSaisie id of entry to associate
     * @param {int} type id of type of intervention to associate
     * @param {int} int_modeop id of operating mode to associate
     * @return {Promise} promise with result of query
     */
    static createIntervention(idSaisie, type, modeop){
      return new Promise((resolve, reject) => {
        this.db.transaction((tx) => {
          var query = 'INSERT INTO t_intervention_int (int_saisie, int_type, int_modeop) VALUES(?,?,?)'
          tx.executeSql(query, [idSaisie, type, modeop])
          .then(([tx,results]) => {
            resolve(results.insertId)
          })
          .catch((err) => reject())
        })
      })
    }

    /**
     * @description insert a new line (selected object)
     * @param {string} table name of table to insert
     * @param {string} nameColumn1 name of first colum
     * @param {*} valueColumn1 value to insert in first column
     * @param {string} nameColumn2 name of second colum
     * @param {*} valueColumn2 value to insert in second column
     * @return {Promise} promise with result of query
     */
    static insertSelectedObject(table, nameColumn1, valueColumn1, nameColumn2, valueColumn2){
      return new Promise((resolve, reject) => {
        this.db.transaction((tx) => {
          var query = 'INSERT INTO ' + table + '(' + nameColumn1 + ',' +  nameColumn2 + ') VALUES(?,?) '
          tx.executeSql(query, [valueColumn1, valueColumn2])
          .then(([tx,results]) => resolve(results.insertId))
          .catch((error) => { reject()})
        })
      })
    }

    /**
     * @description insert a new parameter in t_valeurparametres_vpa
     * @param {int} idIntervention id of intervention with this parameter
     * @param {int} idParametre id of parameter
     * @param {string} value value of this new parameter
     * @return {Promise} promise with result of query
     */
    static insertParametre(idIntervention, idParametre, value) {
      return new Promise((resolve, reject) => {
        this.db.transaction((tx) => {
          const  query = 'INSERT INTO t_valeurparametres_vpa (vpa_intervention, vpa_parametre, vpa_value) VALUES(?,?,?) '
          tx.executeSql(query, [idIntervention, idParametre, value])
          .then(([tx,results]) => resolve(results.insertId))
          .catch((error) => { reject()})})
      })
    }

    /**
     * @description insert a new value for a measurement in t_mesurevariable_mvr
     * @param {string} value value of measurement
     * @param {int} mesureId id of measurement with this value
     * @return {Promise} promise with result of query 
     */
    static insertValueMesure(value, mesureId){
      return new Promise((resolve, reject) => {
        this.db.transaction((tx) => {
          var query = 'INSERT INTO  t_mesurevariable_mvr (mvr_mesure, mvr_value) VALUES(?,?) '
          tx.executeSql(query, [value, mesureId])
          .then(([tx,results]) => resolve(results.insertId))
          .catch(() => reject())
        })
      })
    }
}