import React from 'react'
import DownloadScreen from '../screen/DownloadScreen'
import FichesScreen from '../screen/FichesScreen'
import HomeScreen from '../screen/HomeScreen'
import InformationsScreen from '../screen/InformationsScreen'
import LoginScreen from '../screen/LoginScreen'
import SiteScreen from '../screen/SiteScreen'
import UploadScreen from '../screen/UploadScreen'

import renderer from 'react-test-renderer'

test('renders correctly HomeScreen', () => {
    const tree = renderer.create(<HomeScreen  />).toJSON();
    expect(tree).toMatchSnapshot();
})

test('renders correctly InformationsScreen', () => {
    const tree = renderer.create(<InformationsScreen  />).toJSON();
    expect(tree).toMatchSnapshot();
})

test('renders correctly LoginScreen', () => {
    const tree = renderer.create(<LoginScreen  />).toJSON();
    expect(tree).toMatchSnapshot();
})

test('renders correctly SiteScreen', () => {
    const tree = renderer.create(<SiteScreen  />).toJSON();
    expect(tree).toMatchSnapshot();
})