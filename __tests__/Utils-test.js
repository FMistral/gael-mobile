import {isEmpty, isBlank, sortByPosition, dateToString} from '../Utils/Utils'

describe("isEmpty function", () => {
    test("it should check if 'testString' is not empty", () => {
        const input = 'testString'
        const output = false
        expect(isEmpty(input)).toEqual(output)
    })

    test("it should check if '' is empty", () => {
        const input = ''
        const output = true
        expect(isEmpty(input)).toEqual(output)
    })

    test("it should check if undefined is empty", () => {
        const input = undefined
        const output = true
        expect(isEmpty(input)).toEqual(output)
    })

    test("it should check if null is empty", () => {
        const input = null
        const output = true
        expect(isEmpty(input)).toEqual(output)
    })

    test("it should check if '   ' is not empty", () => {
        const input = null
        const output = true
        expect(isEmpty(input)).toEqual(output)
    })
})

describe("isBlank function", () => {
    test("it should check if 'testString' is not blank", () => {
        const input = 'testString'
        const output = false
        expect(isBlank(input)).toEqual(output)
    })

    test("it should check if '' is blank", () => {
        const input = ''
        const output = true
        expect(isBlank(input)).toEqual(output)
    })

    test("it should check if undefined is blank", () => {
        const input = undefined
        const output = true
        expect(isEmpty(input)).toEqual(output)
    })

    test("it should check if null is blank", () => {
        const input = null
        const output = true
        expect(isBlank(input)).toEqual(output)
    })

    test("it should check if '   ' is blank", () => {
        const input = null
        const output = true
        expect(isBlank(input)).toEqual(output)
    })

    test("it should check if ' d  ' is not blank", () => {
        const input = ' d  '
        const output = false
        expect(isBlank(input)).toEqual(output)
    })
})

describe("sortByPosition function", () => {
    test("it should sort empty array", () => {
        const input = []
        const output = []
        expect(sortByPosition(input)).toEqual(output)
    })

    test("it should sort sorted  array", () => {
        const input = [{position: 1}, {position: 2}, {position: 3}]
        const output = [{position: 1}, {position: 2}, {position: 3}]
        expect(sortByPosition(input)).toEqual(output)
    })

    test("it should sort inverse sorted  array", () => {
        const input = [{position: 3}, {position: 2}, {position: 1}]
        const output = [{position: 1}, {position: 2}, {position: 3}]
        expect(sortByPosition(input)).toEqual(output)
    })

    test("it should sort disorderly array", () => {
        const input = [{position: 3}, {position: 1}, {position: 2}]
        const output = [{position: 1}, {position: 2}, {position: 3}]
        expect(sortByPosition(input)).toEqual(output)
    })

    test("it should not sort a bad array", () => {
        const input = [{bad: 3}, {position: 1}, {position: 2}]
        const output = [{bad: 3}, {position: 1}, {position: 2}]
        expect(sortByPosition(input)).toEqual(output)
    })
})

describe("dateToString function", () => {
    test("it should transform date (22/04/2020) to date's string", () => {
        const input = new Date('2020-04-22T00:00:00')
        const output = '22/4/2020'
        expect(dateToString(input)).toEqual(output)
    })

    test("it should try transform undefined to date's string", () => {
        const input = undefined
        const output = ''
        expect(dateToString(input)).toEqual(output)
    })

    test("it should try transform null to date's string", () => {
        const input = null
        const output = ''
        expect(dateToString(input)).toEqual(output)
    })

    test("it should try transform string to date's string", () => {
        const input = "ceci n'est pas une datte"
        const output = ''
        expect(dateToString(input)).toEqual(output)
    })

    test("it should try transform int to date's string", () => {
        const input = 22042020
        const output = ''
        expect(dateToString(input)).toEqual(output)
    })
})