import {getSelectedObject, getMesure, getIntervention} from '../businessLayer/Upload'
import Database from '../Database'

jest.mock('../Database')


describe("getSelectedObject", () => {
    beforeEach(() => {
        Database.mockClear()
    })

    test("get selected object with correct id and correct nameSelectedId", async () => {
        Database.getElement.mockResolvedValue([{ id: 1, name: 'FARINE' }, { id: 2, name: 'HABI' }])
        const reelOuput = await getSelectedObject(8, 'table', 'nameIdIntervention', 'name')
        const expectOutput = ['FARINE', 'HABI']
        expect(reelOuput).toEqual(expectOutput)
    })

    test("get selected object with correct id and bad nameSelectedId", async () => {
        Database.getElement.mockResolvedValue([{ id: 1, name: 'FARINE' }, { id: 2, name: 'HABI' }])
        const reelOuput = await getSelectedObject(8, 'table', 'nameIdIntervention', 'Badname')
        const expectOutput = [undefined, undefined]
        expect(reelOuput).toEqual(expectOutput)
    })

    test("get selected object with undefined id and correct nameSelectedId", async () => {
        Database.getElement.mockResolvedValue([{ id: 1, name: 'FARINE' }, { id: 2, name: 'HABI' }])
        const reelOuput = await getSelectedObject(undefined, 'table', 'nameIdIntervention', 'name')
        const expectOutput = []
        expect(reelOuput).toEqual(expectOutput)
    })

    test("get selected object with good id and correct nameSelectedId but empty res for query", async () => {
        Database.getElement.mockResolvedValue([])
        const reelOuput = await getSelectedObject(undefined, 'table', 'nameIdIntervention', 'name')
        const expectOutput = []
        expect(reelOuput).toEqual(expectOutput)
    })

})

describe("getMesure", () => {
    beforeEach(() => {
        Database.mockClear()
    })

    test("get measurement with value and equipement", async () => {
        Database.getElement.mockResolvedValue([{ mvr_value: "1", meq_equipement: 'FARINE' }, { mvr_value: "2", meq_equipement: 'HABI' }])
        const input = {mes_parcelle: 1, mes_methode: 1, mes_echantillon : 1}
        const reelOuput = await getMesure(input)
        const expectOutput = {mes_parcelle: 1, mes_methode: 1, mes_echantillon : 1, equipements : ['FARINE', 'HABI'], valeur: "1"}
        expect(reelOuput).toEqual(expectOutput)
    })

    test("get measurement without value and equipement", async () => {
        Database.getElement.mockResolvedValue([])
        const input = {mes_parcelle: 1, mes_methode: 1, mes_echantillon : 1}
        const reelOuput = await getMesure(input)
        const expectOutput = {mes_parcelle: 1, mes_methode: 1, mes_echantillon : 1, equipements : [], valeur: ""}
        expect(reelOuput).toEqual(expectOutput)
    })

    test("get measurement with undefined in entry", async () => {
        try {
            Database.getElement.mockResolvedValue([])
            const reelOuput = await getMesure(undefined)
            expect("Catch error").toEqual("Not throws")
        }
        catch(error) {
            expect(1).toEqual(1)
        }
    })
})