import {getMesures, getParametres, searchInterventionId, getSelectedObject} from '../businessLayer/FicheAndSaisie'
import Database from '../Database'

jest.mock('../Database')


describe("getMesures", () => {
    beforeEach(() => {
        Database.mockClear()
    })

    test("get measurement with undefined interventionId", async () => {
        const outputDatabase = [{lgm_id: 57, lgm_libelle: "Mesure 2", lgm_position: 2, lgm_bEchantillon: true, lgm_bEquipement: false,
        met_nom:"Test", met_id:1, met_uni_id: "cm" }, 
            {lgm_id: 56, lgm_libelle: "Mesure 1", lgm_position: 1, lgm_bEchantillon: true, lgm_bEquipement: false, }]
        const firstOutput = {ligneMesureId: 56, name: "Mesure 1",mesureId: undefined, position: 1, bEchantillon: true, bEquipement: false,
            selectedEchantillon: undefined, selectedEquipements: [], selectedParcelle: undefined, unite: "cm", methode: "Test", methodeId: 1,
            value: undefined }
        const secondOutput = {ligneMesureId: 57,name: "Mesure 2", mesureId: undefined, position: 2, bEchantillon: true, bEquipement: false,
            selectedEchantillon: undefined, selectedEquipements: [], selectedParcelle: undefined, unite: "cm", methode: "Test", methodeId: 1,
            value: undefined }
        const expectOutput = [firstOutput, secondOutput]
        
        Database.getElement.mockResolvedValue(outputDatabase)
        const reelOuput = await getMesures(1, undefined)
        expect(reelOuput).toEqual(expectOutput)
    })

    test("get measurement without equipement", async () => {
        const outputDatabase = [{lgm_id: 57, lgm_libelle: "Mesure 2", lgm_position: 2, lgm_bEchantillon: true, lgm_bEquipement: false,
        met_nom:"Test", met_id:1, met_uni_id: "cm",
        mes_parcelle: 5, mes_id: 45, mes_echantillon: 8, 
        mvr_value: "Valeur mesuree",
        meq_equipement: 45}]
        const expectOutput = [{ligneMesureId: 57,name: "Mesure 2", mesureId: 45, position: 2, bEchantillon: true, bEquipement: false,
        selectedEchantillon: 8, selectedEquipements: [], selectedParcelle: 5, unite: "cm", methode: "Test", methodeId: 1,
        value: "Valeur mesuree"}]
        
        Database.getElement.mockResolvedValue(outputDatabase)
        const reelOuput = await getMesures(1, 89)
        expect(reelOuput).toEqual(expectOutput)
    })

    test("get measurement without samples", async () => {
        const outputDatabase = [{lgm_id: 57, lgm_libelle: "Mesure 2", lgm_position: 2, lgm_bEchantillon: false, lgm_bEquipement: true,
        met_nom:"Test", met_id:1, met_uni_id: "cm",
        mes_parcelle: 5, mes_id: 45, mes_echantillon: 8, 
        mvr_value: "Valeur mesuree",
        meq_equipement: 45}]
        const expectOutput = [{ligneMesureId: 57,name: "Mesure 2", mesureId: 45, position: 2, bEchantillon: false, bEquipement: true,
        selectedEchantillon: undefined, selectedEquipements: [45], selectedParcelle: 5, unite: "cm", methode: "Test", methodeId: 1,
        value: "Valeur mesuree"}]
        
        Database.getElement.mockResolvedValue(outputDatabase)
        const reelOuput = await getMesures(1, 89)
        expect(reelOuput).toEqual(expectOutput)
    })

    test("get mesures without samples", async () => {      
        Database.getElement.mockResolvedValue([])
        const reelOuput = await getMesures(1, 89)
        const expectOutput = []
        expect(reelOuput).toEqual(expectOutput)
    })


})

describe("getParametres", () => {
    beforeEach(() => {
        Database.mockClear()
    })

    test("get settings", async () => {
        const outputDatabase = [{tyi_code: 45, 
            paa_id: 85, paa_nom: "setting test", },
            {paa_id: 86, paa_nom: "setting test 2" }]
        Database.getElement.mockResolvedValue(outputDatabase)
        Database.getElementTwoConditions.mockResolvedValue([{vpa_value: "value"}])

        const reelOuput = await getParametres(47, 45)
        const expectOutput = [{id: 85, name: "setting test", value: "value"}, {id: 86, name: "setting test 2", value: "value"}]
        expect(reelOuput).toEqual(expectOutput)
    })

    test("get settings with undefined interventionId", async () => {
        const outputDatabase = [{tyi_code: 45, 
            paa_id: 85, paa_nom: "setting test"},
            {paa_id: 86, paa_nom: "setting test 2"}]        
        Database.getElement.mockResolvedValue(outputDatabase)
        Database.getElementTwoConditions.mockResolvedValue([{vpa_value: "value"}])

        const reelOuput = await getParametres(undefined, 45)
        const expectOutput = [{id: 85, name: "setting test", value: undefined}, {id: 86, name: "setting test 2", value: undefined}]
        expect(reelOuput).toEqual(expectOutput)
    })

    test("get settings with null typeId", async () => {
        const outputDatabase = [{tyi_code: 45, 
            paa_id: 85, paa_nom: "setting test"},
            {paa_id: 86, paa_nom: "setting test 2"}]        
        Database.getElement.mockResolvedValue(outputDatabase)
        Database.getElementTwoConditions.mockResolvedValue([{vpa_value: "value"}])

        const reelOuput = await getParametres(47, null)
        const expectOutput = []
        expect(reelOuput).toEqual(expectOutput)
    })

    test("get settings with empty return of database", async () => {
        const outputDatabase = []        
        Database.getElement.mockResolvedValue(outputDatabase)
        Database.getElementTwoConditions.mockResolvedValue([])

        const reelOuput = await getParametres(47, 57)
        const expectOutput = []
        expect(reelOuput).toEqual(expectOutput)
    })
})

describe("searchInterventionId", () => {
    beforeEach(() => {
        Database.mockClear()
    })

    test("search id of intervention", async () => {
        Database.getElementTwoConditions.mockResolvedValue([{int_id: 5}, {int_id:8}])

        const reelOuput = await searchInterventionId(47, 45)
        expect(reelOuput).toEqual(5)
    })

    test("search id of intervention with undefined idSaisie", async () => {
        Database.getElementTwoConditions.mockResolvedValue([{int_id: 5}, {int_id:8}])

        const reelOuput = await searchInterventionId(undefined, 45)
        expect(reelOuput).toEqual(undefined)
    })

    test("search id of intervention with null typeIntervention", async () => {
        Database.getElementTwoConditions.mockResolvedValue([{int_id: 5}, {int_id:8}])

        const reelOuput = await searchInterventionId(47, null)
        expect(reelOuput).toEqual(undefined)
    })

    test("search id of intervention with empty return of database", async () => {
        Database.getElementTwoConditions.mockResolvedValue([])

        const reelOuput = await searchInterventionId(47, 45)
        expect(reelOuput).toEqual(undefined)
    })
})

describe("getSelectedObject", () => {
    beforeEach(() => {
        Database.mockClear()
    })

    test("get selected object", async () => {
        Database.getElement.mockResolvedValue([{test: 5}, {test:8}, {test:9}, {test:3}])

        const reelOuput = await getSelectedObject(47, 'table', 'nameIdIntervention', 'test')
        expect(reelOuput).toEqual([5,8,9,3])
    })

    test("get selected object with bad name of selected id ", async () => {
        Database.getElement.mockResolvedValue([{test: 5}, {test:8}, {test:9}, {test:3}])

        const reelOuput = await getSelectedObject(47, 'table', 'nameIdIntervention', 'badName')
        expect(reelOuput).toEqual([undefined, undefined, undefined, undefined])
    })

    test("get selected object with undefined interventionId", async () => {
        Database.getElement.mockResolvedValue([{test: 5}, {test:8}, {test:9}, {test:3}])

        const reelOuput = await getSelectedObject(undefined, 'table', 'nameIdIntervention', 'test')
        expect(reelOuput).toEqual([])
    })

    test("get selected object with empty return of database ", async () => {
        Database.getElement.mockResolvedValue([])

        const reelOuput = await getSelectedObject(78, 'table', 'nameIdIntervention', 'test')
        expect(reelOuput).toEqual([])
    })
})