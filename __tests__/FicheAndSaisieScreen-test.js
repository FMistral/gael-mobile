import React from 'react'
import BottomNavigationScreen from '../screen/ficheAndSaisie/BottomNavigationScreen'
import FicheScreen from '../screen/ficheAndSaisie/FicheScreen'
import InterventionSaisie from '../screen/ficheAndSaisie/InterventionSaisie'
import MesureSaisie from '../screen/ficheAndSaisie/MesureSaisie'
import ParametreSaisie from '../screen/ficheAndSaisie/ParametreSaisie'
import SaisieScreen from '../screen/ficheAndSaisie/SaisieScreen'

import renderer from 'react-test-renderer'

test('renders correctly FicheScreen', () => {
    const tree = renderer.create(<FicheScreen  />).toJSON();
    expect(tree).toMatchSnapshot();
})

test('renders correctly ParametreSaisie', () => {
    const tree = renderer.create(<ParametreSaisie  />).toJSON();
    expect(tree).toMatchSnapshot();
})

test('renders correctly InterventionSaisie', () => {
    const ligneintervention = {parametres:[], mesures:[]}
    const selection = {selectedParcelles : [], selectedUsers : [], selectedEchantillons : [], selectedEquipements : [],
                     selectedAnimaux : [], selectedLots : [], selectedElements : [], selectedRessources : []}
    const tree = renderer.create(<InterventionSaisie  ligneintervention={ligneintervention} selection={selection} parcelles={[]} participants={[]} echantillons={[]} equipements={[]} animaux={[]} lots={[]} elements={[]} ressourcesselection={{}}/>).toJSON();
    expect(tree).toMatchSnapshot();
})

test('renders correctly MesureSaisie', () => {
    const tree = renderer.create(<MesureSaisie mesure={{selectedEquipements: []}} parcelles={[]} echantillons={[]} equipements={[]} />).toJSON();
    expect(tree).toMatchSnapshot();
})