import {createListCategorie, getList, getFirstElement, getElement} from '../businessLayer/GeneralInformation'
import Database from '../Database'
import {data} from '../res/test/DataForTest'

jest.mock('../Database')


describe("createListCategorie", () => {
    beforeEach(() => {
        Database.mockClear()
    })

    test("create list with correct nameColumnId and correct nameColumnName", async () => {
        Database.getTable.mockResolvedValue(data.animaux)
        const reelOuput = await createListCategorie('test','anm_id','anm_nom')
        const expectOutput = [{ id: 1, name: 'FARINE' }, { id: 2, name: 'HABI' }]
        expect(reelOuput).toEqual(expectOutput)
    })

    test("create list with undefined nameColumnId and correct nameColumnName", async () => {
        Database.getTable.mockResolvedValue(data.animaux)
        const reelOuput = await createListCategorie('test',undefined,'anm_nom')
        const expectOutput = [{ id: undefined, name: 'FARINE' }, { id: undefined, name: 'HABI' }]
        expect(reelOuput).toEqual(expectOutput)
    })

    test("create list with null nameColumnId and correct nameColumnName", async () => {
        Database.getTable.mockResolvedValue(data.animaux)
        const reelOuput = await createListCategorie('test', null ,'anm_nom')
        const expectOutput = [{ id: undefined, name: 'FARINE' }, { id: undefined, name: 'HABI' }]
        expect(reelOuput).toEqual(expectOutput)
    })

    test("create list with bad nameColumnId and correct nameColumnName", async () => {
        Database.getTable.mockResolvedValue(data.animaux)
        const reelOuput = await createListCategorie('test', 'badName' ,'anm_nom')
        const expectOutput = [{ id: undefined, name: 'FARINE' }, { id: undefined, name: 'HABI' }]
        expect(reelOuput).toEqual(expectOutput)
    })

    test("create list with correct nameColumnId and undefined nameColumnName", async () => {
        Database.getTable.mockResolvedValue(data.animaux)
        const reelOuput = await createListCategorie('test','anm_id', undefined)
        const expectOutput = [{ id: 1, name: undefined }, { id: 2, name: undefined }]
        expect(reelOuput).toEqual(expectOutput)
    })

    test("create list with correct nameColumnId and null nameColumnName", async () => {
        Database.getTable.mockResolvedValue(data.animaux)
        const reelOuput = await createListCategorie('test','anm_id', null)
        const expectOutput = [{ id: 1, name: undefined }, { id: 2, name: undefined }]
        expect(reelOuput).toEqual(expectOutput)
    })

    test("create list with correct nameColumnId and bad nameColumnName", async () => {
        Database.getTable.mockResolvedValue(data.animaux)
        const reelOuput = await createListCategorie('test','anm_id', 'badName')
        const expectOutput = [{ id: 1, name: undefined }, { id: 2, name: undefined }]
        expect(reelOuput).toEqual(expectOutput)
    })

    test("create list with empty data", async () => {
        Database.getTable.mockResolvedValue([])
        const reelOuput = await createListCategorie('test','anm_id', 'anm_nom')
        const expectOutput = []
        expect(reelOuput).toEqual(expectOutput)
    })
})

describe("getList", () => {
    beforeEach(() => {
        Database.mockClear()
    })

    test("getList for 'Animaux' categorie", async () => {
        Database.getTable.mockResolvedValue(data.animaux)
        const reelOuput = await getList('Animaux')
        const expectOutput = [{ id: 1, name: 'FARINE' }, { id: 2, name: 'HABI' }]
        expect(reelOuput).toEqual(expectOutput)
    })

    test("getList for 'Dispositifs' categorie", async () => {
        Database.getTable.mockResolvedValue(data.dispositifs)
        const reelOuput = await getList('Dispositifs')
        const expectOutput = [{ id: 5, name: 'OasYsT' }]
        expect(reelOuput).toEqual(expectOutput)
    })

    test("getList for 'Echantillons' categorie", async () => {
        Database.getTable.mockResolvedValue(data.echantillons)
        const reelOuput = await getList('Echantillons')
        const expectOutput =  [ { id: 1, name: 'echantillon 1' }, { id: 2, name: 'échantillon 2' } ]
        expect(reelOuput).toEqual(expectOutput)
    })

    test("getList for 'Elément du paysage' categorie", async () => {
        Database.getTable.mockResolvedValue(data.elements)
        const reelOuput = await getList('Elément du paysage')
        const expectOutput = [ { id: 10, name: 'Biomasse 1' }, { id: 11, name: 'Biomasse 2' } ]
        expect(reelOuput).toEqual(expectOutput)
    })

    test("getList for 'Equipements' categorie", async () => {
        Database.getTable.mockResolvedValue(data.equipements)
        const reelOuput = await getList('Equipements')
        const expectOutput =  [{ id: 131, name: 'ex tracteur 1' }, { id: 132, name: 'ex tracteur 2' }]
        expect(reelOuput).toEqual(expectOutput)
    })

    test("getList for 'Lots d'animaux' categorie", async () => {
        Database.getTable.mockResolvedValue(data.lotsanimaux)
        const reelOuput = await getList("Lots d'animaux")
        const expectOutput =  [{ id: 1, name: '24 VL Bota 2015-2016 saison 5' }, { id: 2, name: '24 VL Bota 2015-2016 saison 5' }]
        expect(reelOuput).toEqual(expectOutput)
    })

    test("getList for 'Méthodes de mesure' categorie", async () => {
        Database.getTable.mockResolvedValue(data.methodes)
        const reelOuput = await getList("Méthodes de mesure")
        const expectOutput =   [ { id: 1, name: 'pH eau du sol' }, { id: 3, name: "Poids sec de l'échantillon 72h 60°C" }]
        expect(reelOuput).toEqual(expectOutput)
    })

    test("getList for 'Modes opératoires' categorie", async () => {
        Database.getTable.mockResolvedValue(data.modop)
        const reelOuput = await getList("Modes opératoires")
        const expectOutput =  [ { id: 24, name: "Brûlage d'andain" }, { id: 25, name: 'Broyage' } ]
        expect(reelOuput).toEqual(expectOutput)
    })

    test("getList for 'Paramètres' categorie", async () => {
        Database.getTable.mockResolvedValue(data.parametres)
        const reelOuput = await getList("Paramètres")
        const expectOutput = [{ id: 1, name: "Nombre de passages de la pirouette après coupe lorsque le type d'intervention Fanage n'est pas renseigné"},
                            { id: 2,  name: 'Nombre de bottes récoltées sur la surface concernée'}]
        expect(reelOuput).toEqual(expectOutput)
    })

    test("getList for 'Parcelles' categorie", async () => {
        Database.getTable.mockResolvedValue(data.parcelles)
        const reelOuput = await getList("Parcelles")
        const expectOutput = [{ id: 1, name: 'BH3BPC + BH4 2011' }, { id: 2, name: 'BH3BPC + Communal' } ]
        expect(reelOuput).toEqual(expectOutput)
    })

    test("getList for 'Ressources' categorie", async () => {
        Database.getTable.mockResolvedValue(data.ressources)
        const reelOuput = await getList("Ressources")
        const expectOutput = [ { id: 1, name: 'Foin' }, { id: 2, name: 'Regain' } ]
        expect(reelOuput).toEqual(expectOutput)
    })

    test("getList for 'Paramètres' categorie", async () => {
        Database.getTable.mockResolvedValue(data.parametres)
        const reelOuput = await getList("Paramètres")
        const expectOutput =    [{id: 1, name: "Nombre de passages de la pirouette après coupe lorsque le type d'intervention Fanage n'est pas renseigné"},
                                {id: 2, name: 'Nombre de bottes récoltées sur la surface concernée'}]
        expect(reelOuput).toEqual(expectOutput)
    })

    test("getList for 'Types d'intervention' categorie", async () => {
        Database.getTable.mockResolvedValue(data.typesintervention)
        const reelOuput = await getList("Types d'intervention")
        const expectOutput = [{ id: 3, name: 'Coupe exportée' }, { id: 4, name: 'Coupe non exportée' } ]
        expect(reelOuput).toEqual(expectOutput)
    })

    test("getList for 'Unités' categorie", async () => {
        Database.getTable.mockResolvedValue(data.unites)
        const reelOuput = await getList("Unités")
        const expectOutput = [ { id: 1, name: 'mètre' }, { id: 2, name: 'centimètre' } ]
        expect(reelOuput).toEqual(expectOutput)
    })

    test("getList for not a categorie", async () => {
        Database.getTable.mockResolvedValue(data.parametres)
        const reelOuput = await getList("Bad Categorie")
        const expectOutput = []
        expect(reelOuput).toEqual(expectOutput)
    })

})

describe("getFirstElement", () => {
    beforeEach(() => {
        Database.mockClear()
    })

    test("getFirstElement with a list from database", async () => {
        Database.getElement.mockResolvedValue(data.animaux)
        const reelOuput = await getFirstElement('table','column','id')
        const expectOutput = {"anm_id": 1, "anm_code1": "2005658", "anm_code2": "5658", "anm_nom": "FARINE",  "anm_sex": "Femelle",
                            "anm_debut": "2005-04-10T22:00:00.000Z", "anm_fin": "2016-08-08T22:00:00.000Z", "anm_site": "Theix" }
        expect(reelOuput).toEqual(expectOutput)
    })

    test("getFirstElement with a empty list from database", async () => {
        Database.getElement.mockResolvedValue([])
        const reelOuput = await getFirstElement('table','column','id')
        const expectOutput = undefined
        expect(reelOuput).toEqual(expectOutput)
    })
})

describe("getElement", () => {
    beforeEach(() => {
        Database.mockClear()
    })

    test("getElement for 'Animaux' categorie", async () => {
        Database.getElement.mockResolvedValue(data.animaux)
        const reelOuput = await getElement('Animaux')
        const expectOutput = data.animaux[0]
        expect(reelOuput).toEqual(expectOutput)
    })

    test("getElement for 'Dispositifs' categorie", async () => {
        Database.getElement.mockResolvedValue(data.dispositifs)
        const reelOuput = await getElement('Dispositifs')
        const expectOutput = data.dispositifs[0]
        expect(reelOuput).toEqual(expectOutput)
    })

    test("getElement for 'Echantillons' categorie", async () => {
        Database.getElement.mockResolvedValue(data.echantillons)
        const reelOuput = await getElement('Echantillons')
        const expectOutput = data.echantillons[0]
        expect(reelOuput).toEqual(expectOutput)
    })

    test("getElement for 'Elément du paysage' categorie", async () => {
        Database.getElement.mockResolvedValue(data.elements)
        const reelOuput = await getElement('Elément du paysage')
        const expectOutput = data.elements[0]
        expect(reelOuput).toEqual(expectOutput)
    })

    test("getElement for 'Equipements' categorie", async () => {
        Database.getElement.mockResolvedValue(data.equipements)
        const reelOuput = await getElement('Equipements')
        const expectOutput = data.equipements[0]
        expect(reelOuput).toEqual(expectOutput)
    })

    test("getElement for 'Lots d'animaux' categorie", async () => {
        Database.getElement.mockResolvedValue(data.lotsanimaux)
        const reelOuput = await getElement("Lots d'animaux")
        const expectOutput = data.lotsanimaux[0]
        expect(reelOuput).toEqual(expectOutput)
    })

    test("getElement for 'Méthodes de mesure' categorie", async () => {
        Database.getElement.mockResolvedValue(data.methodes)
        const reelOuput = await getElement("Méthodes de mesure")
        const expectOutput = data.methodes[0]
        expect(reelOuput).toEqual(expectOutput)
    })

    test("getElement for 'Modes opératoires' categorie", async () => {
        Database.getElement.mockResolvedValue(data.modop)
        const reelOuput = await getElement("Modes opératoires")
        const expectOutput = data.modop[0]
        expect(reelOuput).toEqual(expectOutput)
    })

    test("getElement for 'Paramètres' categorie", async () => {
        Database.getElement.mockResolvedValue(data.parametres)
        const reelOuput = await getElement("Paramètres")
        const expectOutput = data.parametres[0]
        expect(reelOuput).toEqual(expectOutput)
    })

    test("getElement for 'Parcelles' categorie", async () => {
        Database.getElement.mockResolvedValue(data.parcelles)
        const reelOuput = await getElement("Parcelles")
        const expectOutput = data.parcelles[0]
        expect(reelOuput).toEqual(expectOutput)
    })

    test("getElement for 'Ressources' categorie", async () => {
        Database.getElement.mockResolvedValue(data.ressources)
        const reelOuput = await getElement("Ressources")
        const expectOutput = data.ressources[0]
        expect(reelOuput).toEqual(expectOutput)
    })

    test("getElement for 'Paramètres' categorie", async () => {
        Database.getElement.mockResolvedValue(data.parametres)
        const reelOuput = await getElement("Paramètres")
        const expectOutput =  data.parametres[0]
        expect(reelOuput).toEqual(expectOutput)
    })

    test("getElement for 'Types d'intervention' categorie", async () => {
        Database.getElement.mockResolvedValue(data.typesintervention)
        const reelOuput = await getElement("Types d'intervention")
        const expectOutput = data.typesintervention[0]
        expect(reelOuput).toEqual(expectOutput)
    })

    test("getElement for 'Unités' categorie", async () => {
        Database.getElement.mockResolvedValue(data.unites)
        const reelOuput = await getElement("Unités")
        const expectOutput = data.unites[0]
        expect(reelOuput).toEqual(expectOutput)
    })

    test("getElement for not a categorie", async () => {
        Database.getElement.mockResolvedValue(data.parametres)
        const reelOuput = await getElement("Bad Categorie")
        const expectOutput = undefined
        expect(reelOuput).toEqual(expectOutput)
    })

})