import AsyncStorage from '@react-native-community/async-storage';

export async function storeData (key, value) {
  try {
    await AsyncStorage.setItem(key, value)
  } catch (e) {
    console.log('Error save data')
  }
}

export async function getData(key) {
  try {
    const value = await AsyncStorage.getItem(key)
    return value
  } catch(e) {
      console.log('Error get data')
      return null
  }
}

export async function isSite () {
  const site = await getData('@site')
  const isSite = site !== null ? true : false
  return isSite
}

export function isEmpty(str) {
  return (!str || 0 === str.length);
}

export function isBlank(str) {
  return (!str || /^\s*$/.test(str));
}

export const requestLogin = "http://10.0.2.2:5253/api/v1/auth/signin"
export const requestGetSites = "http://10.0.2.2:5253/api/v1/sites"