import AsyncStorage from '@react-native-community/async-storage'

/**
 * Contains useful and general functions for async storage
 */

 /**
  * Save data in preference 
  * @param {string} key key where data save
  * @param {*} value value of data
  */
export async function storeData (key, value) {
  try {
    await AsyncStorage.setItem(key, value)
  } catch (e) {
    console.log('Error save data')
  }
}

 /**
  * get data that save in preference 
  * @param {string} key key where data save
  * @retrun {*} value value of data
  */
export async function getData(key) {
  try {
    const value = await AsyncStorage.getItem(key)
    return value
  } catch(e) {
      console.log('Error get data')
      return null
  }
}

/**
 * Check if site is selected in preference
 * @return {boolean} 
 */
export async function isSite () {
  const site = await getData('@site')
  const isSite = site !== null ? true : false
  return isSite
}