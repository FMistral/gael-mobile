import jwt from "react-native-pure-jwt"
import { SECRET } from 'react-native-dotenv'

/**
 * Contains useful and general functions
 */


/**
 * Check if string is empty
 * @param {string} str string to check
 * @return {boolean}
 */
export function isEmpty(str) {
  return (!str || 0 === str.length);
}

/**
 * Check if string is blank
 * @param {string} str string to check
 * @return {boolean}
 */
export function isBlank(str) {
  return (!str || /^\s*$/.test(str));
}

/**
 * Generate a token since email, id, first_name and last_nam
 * @param {string} email 
 * @param {*} id 
 * @param {string} first_name 
 * @param {string} last_name 
 * @return {string} token
 */
export function generateUserToken(email, id, first_name, last_name) {
    return  jwt.sign({
        email: email,
        user_id: id,
        first_name :first_name,
        last_name: last_name,
        exp: new Date().getTime() + 3600*1000,
    },
    SECRET,{algorithm: "HS256"});
}

/**
 * Sort a list by position
 * @param {Array} list an array of object with position
 * @return {Array} list sorted 
 */
export function sortByPosition(list){
    list.sort(function(a, b) {
        var compare = 0
        if (a.position < b.position) {
            compare = -1
        }
        if (a.position > b.position) {
            compare = 1
        }
        return compare
    })
    return list
}

/**
 * Convert a date in string dd/mm/aaaa
 * @param {Date} date date to convert
 * @return {String}
 */
export function dateToString(date){
  var dateString = ''
  if(date !== undefined && date !== null &&  date instanceof Date){
    const month = date.getMonth() +1
    dateString = date.getDate() +'/' + month  +'/' +date.getFullYear()
  }
  return dateString
}

export const requestLogin = "http://10.0.2.2:5253/api/v1/auth/signin"
export const requestGetSites = "http://10.0.2.2:5253/api/v1/sites"
export const requestGetFichesAndDependecy = "http://10.0.2.2:5253/api/v1/fiches/sites/"
export const requestPostSaisies = "http://10.0.2.2:5253/api/v1/saisies"