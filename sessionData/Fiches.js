import Database from '../Database'

/**
 * Class with basic information about sheet
 */
class Fiches {
    /** array of all sheets */
    static fiches = []

    /** Add sheet in list */
    static addFiche(newFiche){
        this.fiches.push(newFiche)
    }

    /** Delete all sheets */
    static clearFiches(){
        this.fiches.splice(0,this.fiches.length)
    }

    /** Load Data from database about sheets */
    // 1 = en cours, 2 = cloture
    static initFiches() {
        this.clearFiches()
        return new Promise((resolve) => {
            Database.getTable(' t_fiche_fic')
            .then(res => {
                for(var i = 0; i < res.length; i++ ){
                    const fiche = res[i]
                    this.fiches.push({id: fiche.fic_id, name: fiche.fic_nom, date: new Date(fiche.fic_date), state: 1})
                }
                Database.getTable('t_saisie_sas')
                .then(res => {
                    for(var j = 0; j < res.length; j++){
                        const indexFiche = this.fiches.findIndex(fiche => fiche.id === res[j].sas_fiche)
                        if (indexFiche >= 0){
                            this.fiches[indexFiche].state = res[j].sas_statut
                        }
                    } 
                    resolve('END')
                })
                
            })
            .catch( err => console.log(err))
        })
    }

    /**
     * Retrun sheets filter if sheet is in progress and/or finished
     * @param {boolean} isProgress if keep sheets in progrees
     * @param {boolean} isFinished if keep sheets finished
     * @return {Array} array of sheet sorted
     */
    static getFichesFilter(isProgress, isFinished) {
        var fiches = []
        if (isProgress){
            fiches = fiches.concat(this.fiches.filter(fiche => fiche.state === 1))
        }
        if (isFinished){
            fiches = fiches.concat(this.fiches.filter(fiche => fiche.state === 2))
        }
        fiches.sort(function(a, b) {
            var compare = 0
            if (a.date < b.date) {
                compare = -1
            }
            if (a.date > b.date) {
                compare = 1
            }
            return compare;
        })
        return fiches
    }
}

export default Fiches