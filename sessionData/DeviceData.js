import {getData} from '../Utils/UtilsAsyncStorage'

/**
 * Class with easy access of name and id of site selected, user's token and user's login
 */
class DeviceData {
    static currentSite = ''
    static currentIdSite= ''
    static userToken = ''
    static userName =''

    /** Init information about site from preference and user information are empty */
    static async init() {
        this.currentSite = await  getData('@site')
        this.currentIdSite = await  getData('@id_site')
        this.userToken = ''
        this.userName = ''
    }

    /** Set user token */
    static setUserToken(newToken){
        this.userToken = newToken
    }

    /** Set user login */
    static setUserName(newName){
        this.userName = newName
    }

    /** Lgout user, delete information about user */
    static logout() {
        this.userToken = ''
        this.userName = ''
    }

    /** Set data about site from preference */
    static async reloadSite() {
        this.currentSite = await  getData('@site')
        this.currentIdSite = await  getData('@id_site')
    }
}

export default DeviceData